﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MaxSdkAndroid::.cctor()
extern void MaxSdkAndroid__cctor_mA39D8643C599874263FB4027F08CD6B69AB406E7 (void);
// 0x00000002 System.Boolean MaxSdkAndroid::IsVerboseLoggingEnabled()
extern void MaxSdkAndroid_IsVerboseLoggingEnabled_m78D800807345F182F84C6F6140C04CF55622C99E (void);
// 0x00000003 System.Void MaxSdkAndroid/BackgroundCallbackProxy::.ctor()
extern void BackgroundCallbackProxy__ctor_m288C289C823B3752C1811B310E98EDE8635B836F (void);
// 0x00000004 System.Void MaxSdkAndroid/BackgroundCallbackProxy::onEvent(System.String)
extern void BackgroundCallbackProxy_onEvent_m83E3740AEDCE20DEEEFEB97C47F4DDCD8FDEE871 (void);
// 0x00000005 System.Void MaxSdkBase::InitCallbacks()
extern void MaxSdkBase_InitCallbacks_m7160D9CF5B123427FE4E367E7030A56A1E5700C0 (void);
// 0x00000006 System.Void MaxSdkBase::.cctor()
extern void MaxSdkBase__cctor_m39BEA40C3BF334AC7765B15DD5992E3AF562E36B (void);
// 0x00000007 MaxSdkBase/SdkConfiguration MaxSdkBase/SdkConfiguration::Create(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void SdkConfiguration_Create_m6C9ADF8FF1C42624EBAA14A7179EA649AB60BD9C (void);
// 0x00000008 System.Void MaxSdkBase/SdkConfiguration::.ctor()
extern void SdkConfiguration__ctor_m285C993D48E2588FD3819884EADD36B5ABAB8013 (void);
// 0x00000009 System.String MaxSdkBase/Reward::ToString()
extern void Reward_ToString_mDD01DDDFE65BE22FA87EDEFAD4DF04AEAFD2F1F9 (void);
// 0x0000000A System.String MaxSdkBase/AdInfo::get_AdUnitIdentifier()
extern void AdInfo_get_AdUnitIdentifier_m2B5D2E9519A7065BB495938D1060AAE774545C64 (void);
// 0x0000000B System.Void MaxSdkBase/AdInfo::set_AdUnitIdentifier(System.String)
extern void AdInfo_set_AdUnitIdentifier_m39336DC3488D6F278C69374428867F8DF59366FD (void);
// 0x0000000C System.String MaxSdkBase/AdInfo::get_AdFormat()
extern void AdInfo_get_AdFormat_m86B3AAE5864C3A17D09D3395EE6F568CD18E767D (void);
// 0x0000000D System.Void MaxSdkBase/AdInfo::set_AdFormat(System.String)
extern void AdInfo_set_AdFormat_m6EC22D9B4D130862F67882230BDF9D91A1510C91 (void);
// 0x0000000E System.String MaxSdkBase/AdInfo::get_NetworkName()
extern void AdInfo_get_NetworkName_m43BFD26676B8A3D849DB261270A682541158ECC3 (void);
// 0x0000000F System.Void MaxSdkBase/AdInfo::set_NetworkName(System.String)
extern void AdInfo_set_NetworkName_m797547E11F1490AA0DE67A0E9895746AD68451A3 (void);
// 0x00000010 System.String MaxSdkBase/AdInfo::get_NetworkPlacement()
extern void AdInfo_get_NetworkPlacement_m12DE80EE52D4E09696DBBFA551060EA8E4631A65 (void);
// 0x00000011 System.Void MaxSdkBase/AdInfo::set_NetworkPlacement(System.String)
extern void AdInfo_set_NetworkPlacement_m98F0442CCD02E0EE34C00A9D91ED2A18A1B6B9B1 (void);
// 0x00000012 System.String MaxSdkBase/AdInfo::get_Placement()
extern void AdInfo_get_Placement_mD030708483DA7D58A48A6FA513715CA235467EC1 (void);
// 0x00000013 System.Void MaxSdkBase/AdInfo::set_Placement(System.String)
extern void AdInfo_set_Placement_m9CF46C5FDA8E1F93A27A2B2C46B040BC7DEEA2D6 (void);
// 0x00000014 System.String MaxSdkBase/AdInfo::get_CreativeIdentifier()
extern void AdInfo_get_CreativeIdentifier_m3BBF3F0F2F3E117F7B2B9D8728D5E40BCFFE90AF (void);
// 0x00000015 System.Void MaxSdkBase/AdInfo::set_CreativeIdentifier(System.String)
extern void AdInfo_set_CreativeIdentifier_mF7D6AD6806B7840677BF7273A3A801BA2F0E6960 (void);
// 0x00000016 System.Double MaxSdkBase/AdInfo::get_Revenue()
extern void AdInfo_get_Revenue_mBDE49D64764F318D50E8D10E9DD473742AD601C8 (void);
// 0x00000017 System.Void MaxSdkBase/AdInfo::set_Revenue(System.Double)
extern void AdInfo_set_Revenue_mDCEE4277FE73E9B877B2F692551E02371F1B4FC9 (void);
// 0x00000018 System.String MaxSdkBase/AdInfo::get_RevenuePrecision()
extern void AdInfo_get_RevenuePrecision_m57B0FE53BCC0E9156ACEC5F8BDCA2126FA8E67C1 (void);
// 0x00000019 System.Void MaxSdkBase/AdInfo::set_RevenuePrecision(System.String)
extern void AdInfo_set_RevenuePrecision_m7404A391DA7A1D9DBB0860DA0D5AFDCDBE272785 (void);
// 0x0000001A System.Void MaxSdkBase/AdInfo::set_WaterfallInfo(MaxSdkBase/WaterfallInfo)
extern void AdInfo_set_WaterfallInfo_m3B38C423DC0AD9C03BA07BC32EE573544E513659 (void);
// 0x0000001B System.String MaxSdkBase/AdInfo::get_DspName()
extern void AdInfo_get_DspName_m308A85019CD64E49DA44A107B403775B50AC9F9D (void);
// 0x0000001C System.Void MaxSdkBase/AdInfo::set_DspName(System.String)
extern void AdInfo_set_DspName_m97142658B1E93D98D983FF1BCDAE36CB36DDBBD9 (void);
// 0x0000001D System.Void MaxSdkBase/AdInfo::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void AdInfo__ctor_m7310799516834E78C2B05719E3858AA995537145 (void);
// 0x0000001E System.String MaxSdkBase/AdInfo::ToString()
extern void AdInfo_ToString_mAE5842B21A673084ED6329FFFEF6240EABD8157F (void);
// 0x0000001F System.String MaxSdkBase/WaterfallInfo::get_Name()
extern void WaterfallInfo_get_Name_m0609BF6768FF716DA229E575C3B1661194756BBD (void);
// 0x00000020 System.Void MaxSdkBase/WaterfallInfo::set_Name(System.String)
extern void WaterfallInfo_set_Name_m8DD3C7BBBCB42A7826FB9B7D12BBA85AE7097606 (void);
// 0x00000021 System.String MaxSdkBase/WaterfallInfo::get_TestName()
extern void WaterfallInfo_get_TestName_m35CC0E2B46098D3A37CBB2C28011D3D421627C25 (void);
// 0x00000022 System.Void MaxSdkBase/WaterfallInfo::set_TestName(System.String)
extern void WaterfallInfo_set_TestName_m8FD58834864613AAD8824A3938589B12A2640C86 (void);
// 0x00000023 System.Collections.Generic.List`1<MaxSdkBase/NetworkResponseInfo> MaxSdkBase/WaterfallInfo::get_NetworkResponses()
extern void WaterfallInfo_get_NetworkResponses_m0088D51C6DC7D15CF8FC67866144D185247F3BA7 (void);
// 0x00000024 System.Void MaxSdkBase/WaterfallInfo::set_NetworkResponses(System.Collections.Generic.List`1<MaxSdkBase/NetworkResponseInfo>)
extern void WaterfallInfo_set_NetworkResponses_m5966DE1A168CF90D421B40C3228E78A84BC4E4CE (void);
// 0x00000025 System.Int64 MaxSdkBase/WaterfallInfo::get_LatencyMillis()
extern void WaterfallInfo_get_LatencyMillis_m99F499835D9B79E3AB5E1240918405C979899E1E (void);
// 0x00000026 System.Void MaxSdkBase/WaterfallInfo::set_LatencyMillis(System.Int64)
extern void WaterfallInfo_set_LatencyMillis_mA98A61C9B91D40DA1D7B362EAB6B1D6D32D21015 (void);
// 0x00000027 System.Void MaxSdkBase/WaterfallInfo::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void WaterfallInfo__ctor_m8E1638B1290E171E56BEA62F32BD8C11599F610F (void);
// 0x00000028 System.String MaxSdkBase/WaterfallInfo::ToString()
extern void WaterfallInfo_ToString_m119254B3F030AFFEF44445985B93A1C390CB44B0 (void);
// 0x00000029 MaxSdkBase/MaxAdLoadState MaxSdkBase/NetworkResponseInfo::get_AdLoadState()
extern void NetworkResponseInfo_get_AdLoadState_m6D743832E42F8E69173D760361934A234752D00B (void);
// 0x0000002A System.Void MaxSdkBase/NetworkResponseInfo::set_AdLoadState(MaxSdkBase/MaxAdLoadState)
extern void NetworkResponseInfo_set_AdLoadState_mFFC57225E8F5E8E0DCC03D15347D0A38FB01C80A (void);
// 0x0000002B MaxSdkBase/MediatedNetworkInfo MaxSdkBase/NetworkResponseInfo::get_MediatedNetwork()
extern void NetworkResponseInfo_get_MediatedNetwork_mE27666303CE844F91DE15F96E614BBC7F43FE7E7 (void);
// 0x0000002C System.Void MaxSdkBase/NetworkResponseInfo::set_MediatedNetwork(MaxSdkBase/MediatedNetworkInfo)
extern void NetworkResponseInfo_set_MediatedNetwork_m236FDAC8D11605891980A024F314384511DC1FCA (void);
// 0x0000002D System.Collections.Generic.Dictionary`2<System.String,System.Object> MaxSdkBase/NetworkResponseInfo::get_Credentials()
extern void NetworkResponseInfo_get_Credentials_m947FD13510DC327DD2E74A72DF3B4F055BA58C19 (void);
// 0x0000002E System.Void MaxSdkBase/NetworkResponseInfo::set_Credentials(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NetworkResponseInfo_set_Credentials_mB53976D9535251171C4D195C1B179DC8029B7F3B (void);
// 0x0000002F System.Int64 MaxSdkBase/NetworkResponseInfo::get_LatencyMillis()
extern void NetworkResponseInfo_get_LatencyMillis_m89EA9B706FE019BA53A8032115452B9BC559D207 (void);
// 0x00000030 System.Void MaxSdkBase/NetworkResponseInfo::set_LatencyMillis(System.Int64)
extern void NetworkResponseInfo_set_LatencyMillis_mF439C231124A202A17DD30EA7D9F93985BA05A89 (void);
// 0x00000031 MaxSdkBase/ErrorInfo MaxSdkBase/NetworkResponseInfo::get_Error()
extern void NetworkResponseInfo_get_Error_mB08DF50DD56D1BB2DDA5963E32CBD5B0E88B923B (void);
// 0x00000032 System.Void MaxSdkBase/NetworkResponseInfo::set_Error(MaxSdkBase/ErrorInfo)
extern void NetworkResponseInfo_set_Error_m5595A38D6F673369E784D774BD6CEF004278DCA5 (void);
// 0x00000033 System.Void MaxSdkBase/NetworkResponseInfo::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void NetworkResponseInfo__ctor_m5BD4E3212E7FCF5713BE2990B940119FFF52F422 (void);
// 0x00000034 System.String MaxSdkBase/NetworkResponseInfo::ToString()
extern void NetworkResponseInfo_ToString_m3A25A6E77CC6B76F3E7B7F16B6D6D26088E9E597 (void);
// 0x00000035 System.String MaxSdkBase/MediatedNetworkInfo::get_Name()
extern void MediatedNetworkInfo_get_Name_m175DEBBDA11DEB7F9151BEC5C0A2FF1274B3D130 (void);
// 0x00000036 System.Void MaxSdkBase/MediatedNetworkInfo::set_Name(System.String)
extern void MediatedNetworkInfo_set_Name_m6B9CB5A4FE03BC31A4C59D551971794B389B5542 (void);
// 0x00000037 System.String MaxSdkBase/MediatedNetworkInfo::get_AdapterClassName()
extern void MediatedNetworkInfo_get_AdapterClassName_m5964D7978630FD0610453FBCD1092348D9D0ED3C (void);
// 0x00000038 System.Void MaxSdkBase/MediatedNetworkInfo::set_AdapterClassName(System.String)
extern void MediatedNetworkInfo_set_AdapterClassName_m7F429D641CEFB02728A0E25E25EEC07F7ABDBEC0 (void);
// 0x00000039 System.String MaxSdkBase/MediatedNetworkInfo::get_AdapterVersion()
extern void MediatedNetworkInfo_get_AdapterVersion_m41F585237A1BBD85B3614B6A0E800B6C454EEFBD (void);
// 0x0000003A System.Void MaxSdkBase/MediatedNetworkInfo::set_AdapterVersion(System.String)
extern void MediatedNetworkInfo_set_AdapterVersion_mAF60A45B9100971C8936968EDBB09CF228093F17 (void);
// 0x0000003B System.String MaxSdkBase/MediatedNetworkInfo::get_SdkVersion()
extern void MediatedNetworkInfo_get_SdkVersion_m316AADB1D839E793B833F8C71C0DA8A97EAD040C (void);
// 0x0000003C System.Void MaxSdkBase/MediatedNetworkInfo::set_SdkVersion(System.String)
extern void MediatedNetworkInfo_set_SdkVersion_m46A2BA2B7682024C4B0E9AE37CBB7EF8EBFBA70C (void);
// 0x0000003D System.Void MaxSdkBase/MediatedNetworkInfo::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MediatedNetworkInfo__ctor_mD747DBCB32B4AFD4813FD4E6E8FBCF4C56CB6433 (void);
// 0x0000003E System.String MaxSdkBase/MediatedNetworkInfo::ToString()
extern void MediatedNetworkInfo_ToString_m3A61E39187C12702298C848D9634200738B162BF (void);
// 0x0000003F MaxSdkBase/ErrorCode MaxSdkBase/ErrorInfo::get_Code()
extern void ErrorInfo_get_Code_mFCCCD97437ED286864EF29D2427AE3CA5C111088 (void);
// 0x00000040 System.Void MaxSdkBase/ErrorInfo::set_Code(MaxSdkBase/ErrorCode)
extern void ErrorInfo_set_Code_m78B86EA7E36244C3BED2F727A879534182AB797A (void);
// 0x00000041 System.String MaxSdkBase/ErrorInfo::get_Message()
extern void ErrorInfo_get_Message_mBE0013996CFF29951E5E751F4924128F5CC0B103 (void);
// 0x00000042 System.Void MaxSdkBase/ErrorInfo::set_Message(System.String)
extern void ErrorInfo_set_Message_m55F1DD6A0D3CAA36DAFF3E487A2A4158CF807309 (void);
// 0x00000043 System.Int32 MaxSdkBase/ErrorInfo::get_MediatedNetworkErrorCode()
extern void ErrorInfo_get_MediatedNetworkErrorCode_mC71E94A118BE27EBADC89A9A8B3B3CFAAC29061A (void);
// 0x00000044 System.Void MaxSdkBase/ErrorInfo::set_MediatedNetworkErrorCode(System.Int32)
extern void ErrorInfo_set_MediatedNetworkErrorCode_m24DB03C54CC83AC70214375A115C3049596C7A1F (void);
// 0x00000045 System.String MaxSdkBase/ErrorInfo::get_MediatedNetworkErrorMessage()
extern void ErrorInfo_get_MediatedNetworkErrorMessage_mB4C10226DEAB4363AEFCA3DD12213CCE3850BF58 (void);
// 0x00000046 System.Void MaxSdkBase/ErrorInfo::set_MediatedNetworkErrorMessage(System.String)
extern void ErrorInfo_set_MediatedNetworkErrorMessage_m9812B3A351B58D05ACC8B31054685DAEF6FF2ED1 (void);
// 0x00000047 System.String MaxSdkBase/ErrorInfo::get_AdLoadFailureInfo()
extern void ErrorInfo_get_AdLoadFailureInfo_m8B19FDB4A053FBA8D3C86A4AD05462FE717215B0 (void);
// 0x00000048 System.Void MaxSdkBase/ErrorInfo::set_AdLoadFailureInfo(System.String)
extern void ErrorInfo_set_AdLoadFailureInfo_m577225495FA06A2A58AFA12DAD957D000F2FBFB4 (void);
// 0x00000049 System.Void MaxSdkBase/ErrorInfo::set_WaterfallInfo(MaxSdkBase/WaterfallInfo)
extern void ErrorInfo_set_WaterfallInfo_m218E40A6BB571D0759673943BD0D2922C5780223 (void);
// 0x0000004A System.Void MaxSdkBase/ErrorInfo::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ErrorInfo__ctor_m6E2A619A1C64C53E3EEAC00208A1A58DA6DF9B28 (void);
// 0x0000004B System.String MaxSdkBase/ErrorInfo::ToString()
extern void ErrorInfo_ToString_m848B4507C47E1B1BF9925A82A48904CB70B7B9C3 (void);
// 0x0000004C MaxSdkCallbacks MaxSdkCallbacks::get_Instance()
extern void MaxSdkCallbacks_get_Instance_m653B23A956A3A113C0719EC85971BB6DEC697226 (void);
// 0x0000004D System.Void MaxSdkCallbacks::set_Instance(MaxSdkCallbacks)
extern void MaxSdkCallbacks_set_Instance_m5B3D2A99B3792971BB1D71C38E49269C06A6F568 (void);
// 0x0000004E System.Void MaxSdkCallbacks::add_OnSdkInitializedEvent(System.Action`1<MaxSdkBase/SdkConfiguration>)
extern void MaxSdkCallbacks_add_OnSdkInitializedEvent_m87E692212C4B9B5F6EDCE3F3FF598C342988BC94 (void);
// 0x0000004F System.Void MaxSdkCallbacks::remove_OnSdkInitializedEvent(System.Action`1<MaxSdkBase/SdkConfiguration>)
extern void MaxSdkCallbacks_remove_OnSdkInitializedEvent_mA2D9E6437A74836C29C9ABA3250323C458D63837 (void);
// 0x00000050 System.Void MaxSdkCallbacks::add_OnVariablesUpdatedEvent(System.Action)
extern void MaxSdkCallbacks_add_OnVariablesUpdatedEvent_m6DFDAAF608769FE0EB32DC651E4EAC68A0D6A168 (void);
// 0x00000051 System.Void MaxSdkCallbacks::remove_OnVariablesUpdatedEvent(System.Action)
extern void MaxSdkCallbacks_remove_OnVariablesUpdatedEvent_m71EB99AD35FEF9E4C17B9935FE40FE9AC42F9367 (void);
// 0x00000052 System.Void MaxSdkCallbacks::add_OnSdkConsentDialogDismissedEvent(System.Action)
extern void MaxSdkCallbacks_add_OnSdkConsentDialogDismissedEvent_m76320D5AB87C33926E392BA52141751F2BAD74B4 (void);
// 0x00000053 System.Void MaxSdkCallbacks::remove_OnSdkConsentDialogDismissedEvent(System.Action)
extern void MaxSdkCallbacks_remove_OnSdkConsentDialogDismissedEvent_mB33AB9E1DD89407943057D74B2CA93EF5F76082B (void);
// 0x00000054 System.Void MaxSdkCallbacks::add_OnBannerAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnBannerAdLoadedEvent_m2E67A4153E503ECF11F63125CE2380FE67D4DD65 (void);
// 0x00000055 System.Void MaxSdkCallbacks::remove_OnBannerAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnBannerAdLoadedEvent_mE7A6ACFEC31DF6CC63F4F1F8A92330671CBA195C (void);
// 0x00000056 System.Void MaxSdkCallbacks::add_OnBannerAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnBannerAdLoadFailedEvent_mD315E2A5F0C3CC66F6432D5E5DD5D049D0E09BC1 (void);
// 0x00000057 System.Void MaxSdkCallbacks::remove_OnBannerAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnBannerAdLoadFailedEvent_m3C3EEB6C5594701B76FB948C3E580DF384CA78FB (void);
// 0x00000058 System.Void MaxSdkCallbacks::add_OnBannerAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnBannerAdClickedEvent_mAB96F321E1DF532A9EF46E4DEA1EAE499840E625 (void);
// 0x00000059 System.Void MaxSdkCallbacks::remove_OnBannerAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnBannerAdClickedEvent_m884EE812274A7EF093AEDA20C9B7042D51B2E65C (void);
// 0x0000005A System.Void MaxSdkCallbacks::add_OnBannerAdExpandedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnBannerAdExpandedEvent_m30B75B616E3CF258179273D37E34028A78691580 (void);
// 0x0000005B System.Void MaxSdkCallbacks::remove_OnBannerAdExpandedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnBannerAdExpandedEvent_m58D60DBC686C556A60D1E1549FFEEA47F3BFA8B4 (void);
// 0x0000005C System.Void MaxSdkCallbacks::add_OnBannerAdCollapsedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnBannerAdCollapsedEvent_m0674992FB30B46017B573C2CDF5ADB77D02598C4 (void);
// 0x0000005D System.Void MaxSdkCallbacks::remove_OnBannerAdCollapsedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnBannerAdCollapsedEvent_m452BCBDFDE226CA1D44B4F66EDB56B8CD2CB4199 (void);
// 0x0000005E System.Void MaxSdkCallbacks::add_OnMRecAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnMRecAdLoadedEvent_mB68ED701969F1282F3FA1F5AFE6082BFC721726B (void);
// 0x0000005F System.Void MaxSdkCallbacks::remove_OnMRecAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnMRecAdLoadedEvent_mE664823B5803E1D38B75147AD33DDADD345FD742 (void);
// 0x00000060 System.Void MaxSdkCallbacks::add_OnMRecAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnMRecAdLoadFailedEvent_mEAC18EDE3B042B72FD0B2D0129784F89127E5303 (void);
// 0x00000061 System.Void MaxSdkCallbacks::remove_OnMRecAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnMRecAdLoadFailedEvent_m19A49701D4E391FC3CAF47480F50B14E065D5BB7 (void);
// 0x00000062 System.Void MaxSdkCallbacks::add_OnMRecAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnMRecAdClickedEvent_m54D9B756202E41C4170C91DC451B1FFC5FA57D71 (void);
// 0x00000063 System.Void MaxSdkCallbacks::remove_OnMRecAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnMRecAdClickedEvent_mE28B92011C77DB85A143EA2BC21C7C56F8281EF7 (void);
// 0x00000064 System.Void MaxSdkCallbacks::add_OnMRecAdExpandedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnMRecAdExpandedEvent_m00C4CD9860CAB1AD7094114C6AF9E04FDF01F463 (void);
// 0x00000065 System.Void MaxSdkCallbacks::remove_OnMRecAdExpandedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnMRecAdExpandedEvent_m2F1DC8B441BFD3FA33F2DD9E96A81F5FCF38F581 (void);
// 0x00000066 System.Void MaxSdkCallbacks::add_OnMRecAdCollapsedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnMRecAdCollapsedEvent_m96ED8BEEBCB9F8AAF1CC3F62F3836151921FF90D (void);
// 0x00000067 System.Void MaxSdkCallbacks::remove_OnMRecAdCollapsedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnMRecAdCollapsedEvent_m0A38E5E49555AB5BE929DF48D33455FEF74973DC (void);
// 0x00000068 System.Void MaxSdkCallbacks::add_OnInterstitialLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnInterstitialLoadedEvent_mB252C93F45EFEF00AB54F939416A10ABCACC3A14 (void);
// 0x00000069 System.Void MaxSdkCallbacks::remove_OnInterstitialLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnInterstitialLoadedEvent_mEC2A9EF4F044E17073211AFECEBDCD7C79F34FDD (void);
// 0x0000006A System.Void MaxSdkCallbacks::add_OnInterstitialLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnInterstitialLoadFailedEvent_m939C2AEF7F080E123FB3549F77BC147100A1C1CA (void);
// 0x0000006B System.Void MaxSdkCallbacks::remove_OnInterstitialLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnInterstitialLoadFailedEvent_m59DD3C4496997191C19C777312794172348764AC (void);
// 0x0000006C System.Void MaxSdkCallbacks::add_OnInterstitialHiddenEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnInterstitialHiddenEvent_m86144390CB58A2F58AF841192FD2AB9E81AC3AE8 (void);
// 0x0000006D System.Void MaxSdkCallbacks::remove_OnInterstitialHiddenEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnInterstitialHiddenEvent_m5B2BBBE34656FAA29DDCD6C4EB7A29A8695938A7 (void);
// 0x0000006E System.Void MaxSdkCallbacks::add_OnInterstitialDisplayedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnInterstitialDisplayedEvent_mF2EE4706B5955CE043D702A05F22E423597DB6D2 (void);
// 0x0000006F System.Void MaxSdkCallbacks::remove_OnInterstitialDisplayedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnInterstitialDisplayedEvent_m1B8B4DF20BE20AB85009A318D5D05A8B6C6E4FF5 (void);
// 0x00000070 System.Void MaxSdkCallbacks::add_OnInterstitialAdFailedToDisplayEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnInterstitialAdFailedToDisplayEvent_m34D116DE0804A605CD0A520083A48809C554E478 (void);
// 0x00000071 System.Void MaxSdkCallbacks::remove_OnInterstitialAdFailedToDisplayEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnInterstitialAdFailedToDisplayEvent_mC4DDDE43CAB6A8C75019464B4C01E1C191C4E4EA (void);
// 0x00000072 System.Void MaxSdkCallbacks::add_OnInterstitialClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnInterstitialClickedEvent_m7107FA4547177B325A4294014B74C29D43A65655 (void);
// 0x00000073 System.Void MaxSdkCallbacks::remove_OnInterstitialClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnInterstitialClickedEvent_mB4C92EA455AF761356ACF3D0995B3617BD9EBBF1 (void);
// 0x00000074 System.Void MaxSdkCallbacks::add_OnRewardedAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnRewardedAdLoadedEvent_mBBFC616284A40832885B59B5B774E981C42EF7C0 (void);
// 0x00000075 System.Void MaxSdkCallbacks::remove_OnRewardedAdLoadedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnRewardedAdLoadedEvent_m1DE48B708934536E85F012436999724FAA45D25E (void);
// 0x00000076 System.Void MaxSdkCallbacks::add_OnRewardedAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnRewardedAdLoadFailedEvent_mD89C87B0B2955D6DD71235715CCD6E412897B9E3 (void);
// 0x00000077 System.Void MaxSdkCallbacks::remove_OnRewardedAdLoadFailedEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnRewardedAdLoadFailedEvent_m87601E74A08A58875853556C7FA19B857F77793E (void);
// 0x00000078 System.Void MaxSdkCallbacks::add_OnRewardedAdDisplayedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnRewardedAdDisplayedEvent_m4381FC70230842DFEA50579481945A83C22C682C (void);
// 0x00000079 System.Void MaxSdkCallbacks::remove_OnRewardedAdDisplayedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnRewardedAdDisplayedEvent_m8D44E231E1CED9937F7E614F60D745D71680C28F (void);
// 0x0000007A System.Void MaxSdkCallbacks::add_OnRewardedAdHiddenEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnRewardedAdHiddenEvent_m9A9278E9451072584909626D4B66F3023D0F6CC3 (void);
// 0x0000007B System.Void MaxSdkCallbacks::remove_OnRewardedAdHiddenEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnRewardedAdHiddenEvent_m2CC5821DBDE808F2A9960685EBBA5EEABC751092 (void);
// 0x0000007C System.Void MaxSdkCallbacks::add_OnRewardedAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_add_OnRewardedAdClickedEvent_m565115253D8659BCFAEF285263592CD99F6FC2D0 (void);
// 0x0000007D System.Void MaxSdkCallbacks::remove_OnRewardedAdClickedEvent(System.Action`1<System.String>)
extern void MaxSdkCallbacks_remove_OnRewardedAdClickedEvent_m810D5340B974EEF2E72ECD4708F606E642119125 (void);
// 0x0000007E System.Void MaxSdkCallbacks::add_OnRewardedAdFailedToDisplayEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_add_OnRewardedAdFailedToDisplayEvent_mCE80DBF55EAE09FEBDEBEA9A49DD2BCB1222B581 (void);
// 0x0000007F System.Void MaxSdkCallbacks::remove_OnRewardedAdFailedToDisplayEvent(System.Action`2<System.String,System.Int32>)
extern void MaxSdkCallbacks_remove_OnRewardedAdFailedToDisplayEvent_m0E159970D90BC669AD2423E0221AEF6B6084DD96 (void);
// 0x00000080 System.Void MaxSdkCallbacks::add_OnRewardedAdReceivedRewardEvent(System.Action`2<System.String,MaxSdkBase/Reward>)
extern void MaxSdkCallbacks_add_OnRewardedAdReceivedRewardEvent_mFE69E24433304C7A7803310F19CB9555F3A2F755 (void);
// 0x00000081 System.Void MaxSdkCallbacks::remove_OnRewardedAdReceivedRewardEvent(System.Action`2<System.String,MaxSdkBase/Reward>)
extern void MaxSdkCallbacks_remove_OnRewardedAdReceivedRewardEvent_mFD55D438E54B3BD7CF7145DD5EB988B1EF5AC94F (void);
// 0x00000082 System.Void MaxSdkCallbacks::Awake()
extern void MaxSdkCallbacks_Awake_m256650B2892F96CD1306F2D85B66F97C6F93A61A (void);
// 0x00000083 System.Void MaxSdkCallbacks::ForwardEvent(System.String)
extern void MaxSdkCallbacks_ForwardEvent_m857EAE295B23AA455B35DDBEA1BF9D5DDA2B6B21 (void);
// 0x00000084 System.Void MaxSdkCallbacks::InvokeEvent(System.Action)
extern void MaxSdkCallbacks_InvokeEvent_mD4EDE900385AF6C072955F31F4A5967732CBE802 (void);
// 0x00000085 System.Void MaxSdkCallbacks::InvokeEvent(System.Action`1<T>,T)
// 0x00000086 System.Void MaxSdkCallbacks::InvokeEvent(System.Action`2<T1,T2>,T1,T2)
// 0x00000087 System.Void MaxSdkCallbacks::InvokeEvent(System.Action`3<T1,T2,T3>,T1,T2,T3)
// 0x00000088 System.Boolean MaxSdkCallbacks::CanInvokeEvent(System.Delegate)
extern void MaxSdkCallbacks_CanInvokeEvent_m9B4F53F43EAA8E5E4E9DD18F05917FC823FE182C (void);
// 0x00000089 System.Void MaxSdkCallbacks::LogSubscribedToEvent(System.String)
extern void MaxSdkCallbacks_LogSubscribedToEvent_m6D1B22F27EC24B3EF55DFFA251DF1FF61407554E (void);
// 0x0000008A System.Void MaxSdkCallbacks::LogUnsubscribedToEvent(System.String)
extern void MaxSdkCallbacks_LogUnsubscribedToEvent_m62365E451AE56BECF9E13B74335C4AC22C2C9662 (void);
// 0x0000008B System.Void MaxSdkCallbacks::.ctor()
extern void MaxSdkCallbacks__ctor_mB5988163F81D838A730F892B05743E41F8DDD0A9 (void);
// 0x0000008C System.Void MaxSdkLogger::UserDebug(System.String)
extern void MaxSdkLogger_UserDebug_m70993A8AF89F1EA31CE54B33517626170184BE57 (void);
// 0x0000008D System.Void MaxSdkLogger::D(System.String)
extern void MaxSdkLogger_D_m7A116E26180E5E1745D6B26A84F6C2D58EF4F4E8 (void);
// 0x0000008E System.Void MaxSdkLogger::UserWarning(System.String)
extern void MaxSdkLogger_UserWarning_m910EBCADCFF977AA2082EE89265571334AD65BD5 (void);
// 0x0000008F System.Void MaxSdkLogger::E(System.String)
extern void MaxSdkLogger_E_m808029613D9BD3F194778F50121A0F03069ED407 (void);
// 0x00000090 System.Collections.Generic.Dictionary`2<System.String,System.Object> MaxSdkUtils::GetDictionaryFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void MaxSdkUtils_GetDictionaryFromDictionary_m46859B4FAB74994DBFACA09B2210EA26AE0C3A4D (void);
// 0x00000091 System.Collections.Generic.List`1<System.Object> MaxSdkUtils::GetListFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Collections.Generic.List`1<System.Object>)
extern void MaxSdkUtils_GetListFromDictionary_mFEF1CF9AE1BD5BE63CD6DE841FB9183FFCB5F5D3 (void);
// 0x00000092 System.String MaxSdkUtils::GetStringFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.String)
extern void MaxSdkUtils_GetStringFromDictionary_m931890AE7EB5DB734B341469CC364DB49AB27FD3 (void);
// 0x00000093 System.Boolean MaxSdkUtils::GetBoolFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
extern void MaxSdkUtils_GetBoolFromDictionary_mAD638B087A2AB2222C1D508E35B5439D7932CA51 (void);
// 0x00000094 System.Int32 MaxSdkUtils::GetIntFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Int32)
extern void MaxSdkUtils_GetIntFromDictionary_mE4E97924EAB3AA2D358162BE1A6CE125DE522A1A (void);
// 0x00000095 System.Int64 MaxSdkUtils::GetLongFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Int64)
extern void MaxSdkUtils_GetLongFromDictionary_mB365230259E91A1878B4F530B58DD353578F133F (void);
// 0x00000096 System.Double MaxSdkUtils::GetDoubleFromDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Int32)
extern void MaxSdkUtils_GetDoubleFromDictionary_m634D5E818A9A8F4AD63FD04F98B12D6D5963661F (void);
// 0x00000097 System.String MaxSdkUtils::InvariantCultureToString(System.Object)
extern void MaxSdkUtils_InvariantCultureToString_m29C76851E4260B2C3B95652E92E4675245CC3719 (void);
// 0x00000098 System.Void MaxSdkUtils::.cctor()
extern void MaxSdkUtils__cctor_m69955B4AC126B6834269407FFD1EAADCCA022ACC (void);
// 0x00000099 System.Void MaxTargetingData::.ctor()
extern void MaxTargetingData__ctor_mF667862720271611C279068C755FC455C9ECA388 (void);
// 0x0000009A System.String MaxUserSegment::get_Name()
extern void MaxUserSegment_get_Name_mF4AC6403330AABE5F70E179AED50316A359B3A97 (void);
// 0x0000009B System.String MaxUserSegment::ToString()
extern void MaxUserSegment_ToString_mD28A9465169655A94A5BE575238528C60970FA80 (void);
// 0x0000009C System.Void MaxUserSegment::.ctor()
extern void MaxUserSegment__ctor_m5C43828E434E88B8BFAF91251F90D77F89DAF8A2 (void);
// 0x0000009D System.Object AppLovinMax.ThirdParty.MiniJson.Json::Deserialize(System.String)
extern void Json_Deserialize_m66288AB2DCFC64857FEB154C7046340E9C6CFFA2 (void);
// 0x0000009E System.Boolean AppLovinMax.ThirdParty.MiniJson.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m94CE3F54545527042D01C86E9A8A7A19DB6EB7E1 (void);
// 0x0000009F System.Void AppLovinMax.ThirdParty.MiniJson.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m664C5F54F214C5DB08B7F5C7E24D7EEC2DE7AC40 (void);
// 0x000000A0 System.Object AppLovinMax.ThirdParty.MiniJson.Json/Parser::Parse(System.String)
extern void Parser_Parse_m71C0F446F69BC2B35F19079DA790638F10511A3B (void);
// 0x000000A1 System.Void AppLovinMax.ThirdParty.MiniJson.Json/Parser::Dispose()
extern void Parser_Dispose_m772C169C33878DBF01644111B7789226344F0205 (void);
// 0x000000A2 System.Collections.Generic.Dictionary`2<System.String,System.Object> AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseObject()
extern void Parser_ParseObject_m7E0BC55E9BBFEFF0A7E57FEC20A17321B8C8B5F8 (void);
// 0x000000A3 System.Collections.Generic.List`1<System.Object> AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseArray()
extern void Parser_ParseArray_m459DCC16388D6A4E60A6E58F69C74892DEA4E46B (void);
// 0x000000A4 System.Object AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseValue()
extern void Parser_ParseValue_mF1EBA3F395E4DAA895F83836FAC51F852041DEEC (void);
// 0x000000A5 System.Object AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseByToken(AppLovinMax.ThirdParty.MiniJson.Json/Parser/TOKEN)
extern void Parser_ParseByToken_m54CA84B7917342C3B6E7A474644EB932D2A54BF3 (void);
// 0x000000A6 System.String AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseString()
extern void Parser_ParseString_m54ED8D3907627C626FCCA4A6177FD23A371F866B (void);
// 0x000000A7 System.Object AppLovinMax.ThirdParty.MiniJson.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mC628A0B9F58F4403FC580D6978AF10A54C740E4E (void);
// 0x000000A8 System.Void AppLovinMax.ThirdParty.MiniJson.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mC42AE1D157D7EDC642B0CB05A5AA109C0A3AD4F7 (void);
// 0x000000A9 System.Char AppLovinMax.ThirdParty.MiniJson.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m0154B63F8EF92D0D33D6BE64266EA47539BA23BC (void);
// 0x000000AA System.Char AppLovinMax.ThirdParty.MiniJson.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_mD8D0B0E2C1AB8977057A31B919930EBC4167F70C (void);
// 0x000000AB System.String AppLovinMax.ThirdParty.MiniJson.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m555E6B20E0D8A3A5D54FBBF215DE324FBF678A68 (void);
// 0x000000AC AppLovinMax.ThirdParty.MiniJson.Json/Parser/TOKEN AppLovinMax.ThirdParty.MiniJson.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m3A746A225E258AEE59133239F96EDED2CAA39D81 (void);
static Il2CppMethodPointer s_methodPointers[172] = 
{
	MaxSdkAndroid__cctor_mA39D8643C599874263FB4027F08CD6B69AB406E7,
	MaxSdkAndroid_IsVerboseLoggingEnabled_m78D800807345F182F84C6F6140C04CF55622C99E,
	BackgroundCallbackProxy__ctor_m288C289C823B3752C1811B310E98EDE8635B836F,
	BackgroundCallbackProxy_onEvent_m83E3740AEDCE20DEEEFEB97C47F4DDCD8FDEE871,
	MaxSdkBase_InitCallbacks_m7160D9CF5B123427FE4E367E7030A56A1E5700C0,
	MaxSdkBase__cctor_m39BEA40C3BF334AC7765B15DD5992E3AF562E36B,
	SdkConfiguration_Create_m6C9ADF8FF1C42624EBAA14A7179EA649AB60BD9C,
	SdkConfiguration__ctor_m285C993D48E2588FD3819884EADD36B5ABAB8013,
	Reward_ToString_mDD01DDDFE65BE22FA87EDEFAD4DF04AEAFD2F1F9,
	AdInfo_get_AdUnitIdentifier_m2B5D2E9519A7065BB495938D1060AAE774545C64,
	AdInfo_set_AdUnitIdentifier_m39336DC3488D6F278C69374428867F8DF59366FD,
	AdInfo_get_AdFormat_m86B3AAE5864C3A17D09D3395EE6F568CD18E767D,
	AdInfo_set_AdFormat_m6EC22D9B4D130862F67882230BDF9D91A1510C91,
	AdInfo_get_NetworkName_m43BFD26676B8A3D849DB261270A682541158ECC3,
	AdInfo_set_NetworkName_m797547E11F1490AA0DE67A0E9895746AD68451A3,
	AdInfo_get_NetworkPlacement_m12DE80EE52D4E09696DBBFA551060EA8E4631A65,
	AdInfo_set_NetworkPlacement_m98F0442CCD02E0EE34C00A9D91ED2A18A1B6B9B1,
	AdInfo_get_Placement_mD030708483DA7D58A48A6FA513715CA235467EC1,
	AdInfo_set_Placement_m9CF46C5FDA8E1F93A27A2B2C46B040BC7DEEA2D6,
	AdInfo_get_CreativeIdentifier_m3BBF3F0F2F3E117F7B2B9D8728D5E40BCFFE90AF,
	AdInfo_set_CreativeIdentifier_mF7D6AD6806B7840677BF7273A3A801BA2F0E6960,
	AdInfo_get_Revenue_mBDE49D64764F318D50E8D10E9DD473742AD601C8,
	AdInfo_set_Revenue_mDCEE4277FE73E9B877B2F692551E02371F1B4FC9,
	AdInfo_get_RevenuePrecision_m57B0FE53BCC0E9156ACEC5F8BDCA2126FA8E67C1,
	AdInfo_set_RevenuePrecision_m7404A391DA7A1D9DBB0860DA0D5AFDCDBE272785,
	AdInfo_set_WaterfallInfo_m3B38C423DC0AD9C03BA07BC32EE573544E513659,
	AdInfo_get_DspName_m308A85019CD64E49DA44A107B403775B50AC9F9D,
	AdInfo_set_DspName_m97142658B1E93D98D983FF1BCDAE36CB36DDBBD9,
	AdInfo__ctor_m7310799516834E78C2B05719E3858AA995537145,
	AdInfo_ToString_mAE5842B21A673084ED6329FFFEF6240EABD8157F,
	WaterfallInfo_get_Name_m0609BF6768FF716DA229E575C3B1661194756BBD,
	WaterfallInfo_set_Name_m8DD3C7BBBCB42A7826FB9B7D12BBA85AE7097606,
	WaterfallInfo_get_TestName_m35CC0E2B46098D3A37CBB2C28011D3D421627C25,
	WaterfallInfo_set_TestName_m8FD58834864613AAD8824A3938589B12A2640C86,
	WaterfallInfo_get_NetworkResponses_m0088D51C6DC7D15CF8FC67866144D185247F3BA7,
	WaterfallInfo_set_NetworkResponses_m5966DE1A168CF90D421B40C3228E78A84BC4E4CE,
	WaterfallInfo_get_LatencyMillis_m99F499835D9B79E3AB5E1240918405C979899E1E,
	WaterfallInfo_set_LatencyMillis_mA98A61C9B91D40DA1D7B362EAB6B1D6D32D21015,
	WaterfallInfo__ctor_m8E1638B1290E171E56BEA62F32BD8C11599F610F,
	WaterfallInfo_ToString_m119254B3F030AFFEF44445985B93A1C390CB44B0,
	NetworkResponseInfo_get_AdLoadState_m6D743832E42F8E69173D760361934A234752D00B,
	NetworkResponseInfo_set_AdLoadState_mFFC57225E8F5E8E0DCC03D15347D0A38FB01C80A,
	NetworkResponseInfo_get_MediatedNetwork_mE27666303CE844F91DE15F96E614BBC7F43FE7E7,
	NetworkResponseInfo_set_MediatedNetwork_m236FDAC8D11605891980A024F314384511DC1FCA,
	NetworkResponseInfo_get_Credentials_m947FD13510DC327DD2E74A72DF3B4F055BA58C19,
	NetworkResponseInfo_set_Credentials_mB53976D9535251171C4D195C1B179DC8029B7F3B,
	NetworkResponseInfo_get_LatencyMillis_m89EA9B706FE019BA53A8032115452B9BC559D207,
	NetworkResponseInfo_set_LatencyMillis_mF439C231124A202A17DD30EA7D9F93985BA05A89,
	NetworkResponseInfo_get_Error_mB08DF50DD56D1BB2DDA5963E32CBD5B0E88B923B,
	NetworkResponseInfo_set_Error_m5595A38D6F673369E784D774BD6CEF004278DCA5,
	NetworkResponseInfo__ctor_m5BD4E3212E7FCF5713BE2990B940119FFF52F422,
	NetworkResponseInfo_ToString_m3A25A6E77CC6B76F3E7B7F16B6D6D26088E9E597,
	MediatedNetworkInfo_get_Name_m175DEBBDA11DEB7F9151BEC5C0A2FF1274B3D130,
	MediatedNetworkInfo_set_Name_m6B9CB5A4FE03BC31A4C59D551971794B389B5542,
	MediatedNetworkInfo_get_AdapterClassName_m5964D7978630FD0610453FBCD1092348D9D0ED3C,
	MediatedNetworkInfo_set_AdapterClassName_m7F429D641CEFB02728A0E25E25EEC07F7ABDBEC0,
	MediatedNetworkInfo_get_AdapterVersion_m41F585237A1BBD85B3614B6A0E800B6C454EEFBD,
	MediatedNetworkInfo_set_AdapterVersion_mAF60A45B9100971C8936968EDBB09CF228093F17,
	MediatedNetworkInfo_get_SdkVersion_m316AADB1D839E793B833F8C71C0DA8A97EAD040C,
	MediatedNetworkInfo_set_SdkVersion_m46A2BA2B7682024C4B0E9AE37CBB7EF8EBFBA70C,
	MediatedNetworkInfo__ctor_mD747DBCB32B4AFD4813FD4E6E8FBCF4C56CB6433,
	MediatedNetworkInfo_ToString_m3A61E39187C12702298C848D9634200738B162BF,
	ErrorInfo_get_Code_mFCCCD97437ED286864EF29D2427AE3CA5C111088,
	ErrorInfo_set_Code_m78B86EA7E36244C3BED2F727A879534182AB797A,
	ErrorInfo_get_Message_mBE0013996CFF29951E5E751F4924128F5CC0B103,
	ErrorInfo_set_Message_m55F1DD6A0D3CAA36DAFF3E487A2A4158CF807309,
	ErrorInfo_get_MediatedNetworkErrorCode_mC71E94A118BE27EBADC89A9A8B3B3CFAAC29061A,
	ErrorInfo_set_MediatedNetworkErrorCode_m24DB03C54CC83AC70214375A115C3049596C7A1F,
	ErrorInfo_get_MediatedNetworkErrorMessage_mB4C10226DEAB4363AEFCA3DD12213CCE3850BF58,
	ErrorInfo_set_MediatedNetworkErrorMessage_m9812B3A351B58D05ACC8B31054685DAEF6FF2ED1,
	ErrorInfo_get_AdLoadFailureInfo_m8B19FDB4A053FBA8D3C86A4AD05462FE717215B0,
	ErrorInfo_set_AdLoadFailureInfo_m577225495FA06A2A58AFA12DAD957D000F2FBFB4,
	ErrorInfo_set_WaterfallInfo_m218E40A6BB571D0759673943BD0D2922C5780223,
	ErrorInfo__ctor_m6E2A619A1C64C53E3EEAC00208A1A58DA6DF9B28,
	ErrorInfo_ToString_m848B4507C47E1B1BF9925A82A48904CB70B7B9C3,
	MaxSdkCallbacks_get_Instance_m653B23A956A3A113C0719EC85971BB6DEC697226,
	MaxSdkCallbacks_set_Instance_m5B3D2A99B3792971BB1D71C38E49269C06A6F568,
	MaxSdkCallbacks_add_OnSdkInitializedEvent_m87E692212C4B9B5F6EDCE3F3FF598C342988BC94,
	MaxSdkCallbacks_remove_OnSdkInitializedEvent_mA2D9E6437A74836C29C9ABA3250323C458D63837,
	MaxSdkCallbacks_add_OnVariablesUpdatedEvent_m6DFDAAF608769FE0EB32DC651E4EAC68A0D6A168,
	MaxSdkCallbacks_remove_OnVariablesUpdatedEvent_m71EB99AD35FEF9E4C17B9935FE40FE9AC42F9367,
	MaxSdkCallbacks_add_OnSdkConsentDialogDismissedEvent_m76320D5AB87C33926E392BA52141751F2BAD74B4,
	MaxSdkCallbacks_remove_OnSdkConsentDialogDismissedEvent_mB33AB9E1DD89407943057D74B2CA93EF5F76082B,
	MaxSdkCallbacks_add_OnBannerAdLoadedEvent_m2E67A4153E503ECF11F63125CE2380FE67D4DD65,
	MaxSdkCallbacks_remove_OnBannerAdLoadedEvent_mE7A6ACFEC31DF6CC63F4F1F8A92330671CBA195C,
	MaxSdkCallbacks_add_OnBannerAdLoadFailedEvent_mD315E2A5F0C3CC66F6432D5E5DD5D049D0E09BC1,
	MaxSdkCallbacks_remove_OnBannerAdLoadFailedEvent_m3C3EEB6C5594701B76FB948C3E580DF384CA78FB,
	MaxSdkCallbacks_add_OnBannerAdClickedEvent_mAB96F321E1DF532A9EF46E4DEA1EAE499840E625,
	MaxSdkCallbacks_remove_OnBannerAdClickedEvent_m884EE812274A7EF093AEDA20C9B7042D51B2E65C,
	MaxSdkCallbacks_add_OnBannerAdExpandedEvent_m30B75B616E3CF258179273D37E34028A78691580,
	MaxSdkCallbacks_remove_OnBannerAdExpandedEvent_m58D60DBC686C556A60D1E1549FFEEA47F3BFA8B4,
	MaxSdkCallbacks_add_OnBannerAdCollapsedEvent_m0674992FB30B46017B573C2CDF5ADB77D02598C4,
	MaxSdkCallbacks_remove_OnBannerAdCollapsedEvent_m452BCBDFDE226CA1D44B4F66EDB56B8CD2CB4199,
	MaxSdkCallbacks_add_OnMRecAdLoadedEvent_mB68ED701969F1282F3FA1F5AFE6082BFC721726B,
	MaxSdkCallbacks_remove_OnMRecAdLoadedEvent_mE664823B5803E1D38B75147AD33DDADD345FD742,
	MaxSdkCallbacks_add_OnMRecAdLoadFailedEvent_mEAC18EDE3B042B72FD0B2D0129784F89127E5303,
	MaxSdkCallbacks_remove_OnMRecAdLoadFailedEvent_m19A49701D4E391FC3CAF47480F50B14E065D5BB7,
	MaxSdkCallbacks_add_OnMRecAdClickedEvent_m54D9B756202E41C4170C91DC451B1FFC5FA57D71,
	MaxSdkCallbacks_remove_OnMRecAdClickedEvent_mE28B92011C77DB85A143EA2BC21C7C56F8281EF7,
	MaxSdkCallbacks_add_OnMRecAdExpandedEvent_m00C4CD9860CAB1AD7094114C6AF9E04FDF01F463,
	MaxSdkCallbacks_remove_OnMRecAdExpandedEvent_m2F1DC8B441BFD3FA33F2DD9E96A81F5FCF38F581,
	MaxSdkCallbacks_add_OnMRecAdCollapsedEvent_m96ED8BEEBCB9F8AAF1CC3F62F3836151921FF90D,
	MaxSdkCallbacks_remove_OnMRecAdCollapsedEvent_m0A38E5E49555AB5BE929DF48D33455FEF74973DC,
	MaxSdkCallbacks_add_OnInterstitialLoadedEvent_mB252C93F45EFEF00AB54F939416A10ABCACC3A14,
	MaxSdkCallbacks_remove_OnInterstitialLoadedEvent_mEC2A9EF4F044E17073211AFECEBDCD7C79F34FDD,
	MaxSdkCallbacks_add_OnInterstitialLoadFailedEvent_m939C2AEF7F080E123FB3549F77BC147100A1C1CA,
	MaxSdkCallbacks_remove_OnInterstitialLoadFailedEvent_m59DD3C4496997191C19C777312794172348764AC,
	MaxSdkCallbacks_add_OnInterstitialHiddenEvent_m86144390CB58A2F58AF841192FD2AB9E81AC3AE8,
	MaxSdkCallbacks_remove_OnInterstitialHiddenEvent_m5B2BBBE34656FAA29DDCD6C4EB7A29A8695938A7,
	MaxSdkCallbacks_add_OnInterstitialDisplayedEvent_mF2EE4706B5955CE043D702A05F22E423597DB6D2,
	MaxSdkCallbacks_remove_OnInterstitialDisplayedEvent_m1B8B4DF20BE20AB85009A318D5D05A8B6C6E4FF5,
	MaxSdkCallbacks_add_OnInterstitialAdFailedToDisplayEvent_m34D116DE0804A605CD0A520083A48809C554E478,
	MaxSdkCallbacks_remove_OnInterstitialAdFailedToDisplayEvent_mC4DDDE43CAB6A8C75019464B4C01E1C191C4E4EA,
	MaxSdkCallbacks_add_OnInterstitialClickedEvent_m7107FA4547177B325A4294014B74C29D43A65655,
	MaxSdkCallbacks_remove_OnInterstitialClickedEvent_mB4C92EA455AF761356ACF3D0995B3617BD9EBBF1,
	MaxSdkCallbacks_add_OnRewardedAdLoadedEvent_mBBFC616284A40832885B59B5B774E981C42EF7C0,
	MaxSdkCallbacks_remove_OnRewardedAdLoadedEvent_m1DE48B708934536E85F012436999724FAA45D25E,
	MaxSdkCallbacks_add_OnRewardedAdLoadFailedEvent_mD89C87B0B2955D6DD71235715CCD6E412897B9E3,
	MaxSdkCallbacks_remove_OnRewardedAdLoadFailedEvent_m87601E74A08A58875853556C7FA19B857F77793E,
	MaxSdkCallbacks_add_OnRewardedAdDisplayedEvent_m4381FC70230842DFEA50579481945A83C22C682C,
	MaxSdkCallbacks_remove_OnRewardedAdDisplayedEvent_m8D44E231E1CED9937F7E614F60D745D71680C28F,
	MaxSdkCallbacks_add_OnRewardedAdHiddenEvent_m9A9278E9451072584909626D4B66F3023D0F6CC3,
	MaxSdkCallbacks_remove_OnRewardedAdHiddenEvent_m2CC5821DBDE808F2A9960685EBBA5EEABC751092,
	MaxSdkCallbacks_add_OnRewardedAdClickedEvent_m565115253D8659BCFAEF285263592CD99F6FC2D0,
	MaxSdkCallbacks_remove_OnRewardedAdClickedEvent_m810D5340B974EEF2E72ECD4708F606E642119125,
	MaxSdkCallbacks_add_OnRewardedAdFailedToDisplayEvent_mCE80DBF55EAE09FEBDEBEA9A49DD2BCB1222B581,
	MaxSdkCallbacks_remove_OnRewardedAdFailedToDisplayEvent_m0E159970D90BC669AD2423E0221AEF6B6084DD96,
	MaxSdkCallbacks_add_OnRewardedAdReceivedRewardEvent_mFE69E24433304C7A7803310F19CB9555F3A2F755,
	MaxSdkCallbacks_remove_OnRewardedAdReceivedRewardEvent_mFD55D438E54B3BD7CF7145DD5EB988B1EF5AC94F,
	MaxSdkCallbacks_Awake_m256650B2892F96CD1306F2D85B66F97C6F93A61A,
	MaxSdkCallbacks_ForwardEvent_m857EAE295B23AA455B35DDBEA1BF9D5DDA2B6B21,
	MaxSdkCallbacks_InvokeEvent_mD4EDE900385AF6C072955F31F4A5967732CBE802,
	NULL,
	NULL,
	NULL,
	MaxSdkCallbacks_CanInvokeEvent_m9B4F53F43EAA8E5E4E9DD18F05917FC823FE182C,
	MaxSdkCallbacks_LogSubscribedToEvent_m6D1B22F27EC24B3EF55DFFA251DF1FF61407554E,
	MaxSdkCallbacks_LogUnsubscribedToEvent_m62365E451AE56BECF9E13B74335C4AC22C2C9662,
	MaxSdkCallbacks__ctor_mB5988163F81D838A730F892B05743E41F8DDD0A9,
	MaxSdkLogger_UserDebug_m70993A8AF89F1EA31CE54B33517626170184BE57,
	MaxSdkLogger_D_m7A116E26180E5E1745D6B26A84F6C2D58EF4F4E8,
	MaxSdkLogger_UserWarning_m910EBCADCFF977AA2082EE89265571334AD65BD5,
	MaxSdkLogger_E_m808029613D9BD3F194778F50121A0F03069ED407,
	MaxSdkUtils_GetDictionaryFromDictionary_m46859B4FAB74994DBFACA09B2210EA26AE0C3A4D,
	MaxSdkUtils_GetListFromDictionary_mFEF1CF9AE1BD5BE63CD6DE841FB9183FFCB5F5D3,
	MaxSdkUtils_GetStringFromDictionary_m931890AE7EB5DB734B341469CC364DB49AB27FD3,
	MaxSdkUtils_GetBoolFromDictionary_mAD638B087A2AB2222C1D508E35B5439D7932CA51,
	MaxSdkUtils_GetIntFromDictionary_mE4E97924EAB3AA2D358162BE1A6CE125DE522A1A,
	MaxSdkUtils_GetLongFromDictionary_mB365230259E91A1878B4F530B58DD353578F133F,
	MaxSdkUtils_GetDoubleFromDictionary_m634D5E818A9A8F4AD63FD04F98B12D6D5963661F,
	MaxSdkUtils_InvariantCultureToString_m29C76851E4260B2C3B95652E92E4675245CC3719,
	MaxSdkUtils__cctor_m69955B4AC126B6834269407FFD1EAADCCA022ACC,
	MaxTargetingData__ctor_mF667862720271611C279068C755FC455C9ECA388,
	MaxUserSegment_get_Name_mF4AC6403330AABE5F70E179AED50316A359B3A97,
	MaxUserSegment_ToString_mD28A9465169655A94A5BE575238528C60970FA80,
	MaxUserSegment__ctor_m5C43828E434E88B8BFAF91251F90D77F89DAF8A2,
	Json_Deserialize_m66288AB2DCFC64857FEB154C7046340E9C6CFFA2,
	Parser_IsWordBreak_m94CE3F54545527042D01C86E9A8A7A19DB6EB7E1,
	Parser__ctor_m664C5F54F214C5DB08B7F5C7E24D7EEC2DE7AC40,
	Parser_Parse_m71C0F446F69BC2B35F19079DA790638F10511A3B,
	Parser_Dispose_m772C169C33878DBF01644111B7789226344F0205,
	Parser_ParseObject_m7E0BC55E9BBFEFF0A7E57FEC20A17321B8C8B5F8,
	Parser_ParseArray_m459DCC16388D6A4E60A6E58F69C74892DEA4E46B,
	Parser_ParseValue_mF1EBA3F395E4DAA895F83836FAC51F852041DEEC,
	Parser_ParseByToken_m54CA84B7917342C3B6E7A474644EB932D2A54BF3,
	Parser_ParseString_m54ED8D3907627C626FCCA4A6177FD23A371F866B,
	Parser_ParseNumber_mC628A0B9F58F4403FC580D6978AF10A54C740E4E,
	Parser_EatWhitespace_mC42AE1D157D7EDC642B0CB05A5AA109C0A3AD4F7,
	Parser_get_PeekChar_m0154B63F8EF92D0D33D6BE64266EA47539BA23BC,
	Parser_get_NextChar_mD8D0B0E2C1AB8977057A31B919930EBC4167F70C,
	Parser_get_NextWord_m555E6B20E0D8A3A5D54FBBF215DE324FBF678A68,
	Parser_get_NextToken_m3A746A225E258AEE59133239F96EDED2CAA39D81,
};
extern void Reward_ToString_mDD01DDDFE65BE22FA87EDEFAD4DF04AEAFD2F1F9_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000009, Reward_ToString_mDD01DDDFE65BE22FA87EDEFAD4DF04AEAFD2F1F9_AdjustorThunk },
};
static const int32_t s_InvokerIndices[172] = 
{
	4912,
	4868,
	3253,
	2638,
	4912,
	4912,
	4683,
	3253,
	3165,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3122,
	2598,
	3165,
	2638,
	2638,
	3165,
	2638,
	2638,
	3165,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3147,
	2622,
	2638,
	3165,
	3146,
	2621,
	3165,
	2638,
	3165,
	2638,
	3147,
	2622,
	3165,
	2638,
	2638,
	3165,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	3165,
	2638,
	2638,
	3165,
	3146,
	2621,
	3165,
	2638,
	3146,
	2621,
	3165,
	2638,
	3165,
	2638,
	2638,
	2638,
	3165,
	4888,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	4815,
	3253,
	2638,
	4815,
	0,
	0,
	0,
	4557,
	4815,
	4815,
	3253,
	4815,
	4815,
	4815,
	4815,
	3960,
	3960,
	3960,
	3847,
	3904,
	3918,
	3871,
	4683,
	4912,
	3253,
	3165,
	3165,
	3253,
	4683,
	4564,
	2638,
	4683,
	3253,
	3165,
	3165,
	3165,
	2316,
	3165,
	3165,
	3253,
	3240,
	3240,
	3165,
	3146,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x06000085, { 0, 4 } },
	{ 0x06000086, { 4, 6 } },
	{ 0x06000087, { 10, 8 } },
};
extern const uint32_t g_rgctx_T_tDB279991626110E5A1DCD27460C9EFE35B042C27;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tDB279991626110E5A1DCD27460C9EFE35B042C27_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_Action_1_t5E8D80E588AFD5D08287E2C416B66B6BE956B261;
extern const uint32_t g_rgctx_Action_1_Invoke_m6C4BCD3CE1D274D4FBC65578206D5AF9C89A4F9F;
extern const uint32_t g_rgctx_T1_tF80C83F99A7F4D2D528B64F57BE97BC4188161C2;
extern const Il2CppRGCTXConstrainedData g_rgctx_T1_tF80C83F99A7F4D2D528B64F57BE97BC4188161C2_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_T2_t0C6814D37A782D36AB9069EB5739DC73D7E68F5D;
extern const Il2CppRGCTXConstrainedData g_rgctx_T2_t0C6814D37A782D36AB9069EB5739DC73D7E68F5D_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_Action_2_t72C6FDAE974CEEB82ED68CA40648F2B276982EE8;
extern const uint32_t g_rgctx_Action_2_Invoke_m465FDE2DB62BAD1EDD86AA1BE213BF10C7D01CBC;
extern const uint32_t g_rgctx_T1_tF8CF9B4A0DA8E37CAFDA2F806967FEF329F20856;
extern const Il2CppRGCTXConstrainedData g_rgctx_T1_tF8CF9B4A0DA8E37CAFDA2F806967FEF329F20856_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_T2_t704C95CB6DC774411682267E13E19C60AE5011CD;
extern const Il2CppRGCTXConstrainedData g_rgctx_T2_t704C95CB6DC774411682267E13E19C60AE5011CD_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_T3_t66FB93B6F09030EB8A7306A931BDEA4F0D7039DE;
extern const Il2CppRGCTXConstrainedData g_rgctx_T3_t66FB93B6F09030EB8A7306A931BDEA4F0D7039DE_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_Action_3_t783E7A08FCD6291FE1B430A8BDC4BFB9DF2A33F1;
extern const uint32_t g_rgctx_Action_3_Invoke_m9748ED6A69327508CBD45ADAB129FED2B6B9BC13;
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tDB279991626110E5A1DCD27460C9EFE35B042C27 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tDB279991626110E5A1DCD27460C9EFE35B042C27_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t5E8D80E588AFD5D08287E2C416B66B6BE956B261 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m6C4BCD3CE1D274D4FBC65578206D5AF9C89A4F9F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T1_tF80C83F99A7F4D2D528B64F57BE97BC4188161C2 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T1_tF80C83F99A7F4D2D528B64F57BE97BC4188161C2_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T2_t0C6814D37A782D36AB9069EB5739DC73D7E68F5D },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T2_t0C6814D37A782D36AB9069EB5739DC73D7E68F5D_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t72C6FDAE974CEEB82ED68CA40648F2B276982EE8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m465FDE2DB62BAD1EDD86AA1BE213BF10C7D01CBC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T1_tF8CF9B4A0DA8E37CAFDA2F806967FEF329F20856 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T1_tF8CF9B4A0DA8E37CAFDA2F806967FEF329F20856_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T2_t704C95CB6DC774411682267E13E19C60AE5011CD },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T2_t704C95CB6DC774411682267E13E19C60AE5011CD_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T3_t66FB93B6F09030EB8A7306A931BDEA4F0D7039DE },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T3_t66FB93B6F09030EB8A7306A931BDEA4F0D7039DE_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t783E7A08FCD6291FE1B430A8BDC4BFB9DF2A33F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3_Invoke_m9748ED6A69327508CBD45ADAB129FED2B6B9BC13 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MaxSdk_Scripts_CodeGenModule;
const Il2CppCodeGenModule g_MaxSdk_Scripts_CodeGenModule = 
{
	"MaxSdk.Scripts.dll",
	172,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
