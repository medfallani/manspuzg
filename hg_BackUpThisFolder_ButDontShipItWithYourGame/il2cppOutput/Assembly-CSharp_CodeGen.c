﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void rectactivator::Start()
extern void rectactivator_Start_m22086C3FB7180C9B22365A5AC31B765C4FE252A9 (void);
// 0x00000002 System.Void rectactivator::.ctor()
extern void rectactivator__ctor_m1A10124088D84CC61EEDCCE1340CE6E499EB0CF5 (void);
// 0x00000003 InitializeAdmob InitializeAdmob::get_instance()
extern void InitializeAdmob_get_instance_m775F097969C7C96CD64A7835A5DF0F53D427EDB9 (void);
// 0x00000004 System.Void InitializeAdmob::Awake()
extern void InitializeAdmob_Awake_m76617573C8A496E3D35AE8371DE507B88121EDA4 (void);
// 0x00000005 System.Void InitializeAdmob::Start()
extern void InitializeAdmob_Start_mFFD8C1EAC7AA4F50BC5E43F3A085059B97A7D653 (void);
// 0x00000006 System.Void InitializeAdmob::cekiklan()
extern void InitializeAdmob_cekiklan_m7CF02EE8C7C4A167AB6295B9BDAAC2E5E1246F9A (void);
// 0x00000007 System.Void InitializeAdmob::requestInter()
extern void InitializeAdmob_requestInter_m40FE3639B5B423D8F69ED3109F6B3E9EE7820BB0 (void);
// 0x00000008 System.Void InitializeAdmob::Interstitial_OnAdClosed(System.Object,System.EventArgs)
extern void InitializeAdmob_Interstitial_OnAdClosed_m4B1C439154E7C410E25D47DE642F1AF57EE97355 (void);
// 0x00000009 System.Void InitializeAdmob::showfullads()
extern void InitializeAdmob_showfullads_mC70EE3972D8EE5D88C641AEFC41C9F9874A25739 (void);
// 0x0000000A System.Void InitializeAdmob::showbanner()
extern void InitializeAdmob_showbanner_m6819F116DF0C57B9D343E7EC145BC52F78BB3112 (void);
// 0x0000000B System.Void InitializeAdmob::Show_Ads_Full()
extern void InitializeAdmob_Show_Ads_Full_m8E104BA115306CFAD1F2961FF5181C87383F4AA3 (void);
// 0x0000000C System.Void InitializeAdmob::.ctor()
extern void InitializeAdmob__ctor_m583DE5BE708B6B75F5B6B34F5AF07A6D378C5B82 (void);
// 0x0000000D System.Void button_Script::tersentuh()
extern void button_Script_tersentuh_m19DF08D0EA67E81461E99AAD3D35879ED50565B9 (void);
// 0x0000000E System.Void button_Script::Update()
extern void button_Script_Update_mF498BE5DE7A985556E0768DC44C4901547E98AFC (void);
// 0x0000000F System.Void button_Script::.ctor()
extern void button_Script__ctor_mB645C943600628448E4B1B83720431240712728E (void);
// 0x00000010 System.Void canvas_force::Update()
extern void canvas_force_Update_m3055F27B205EF754B5D5B2AA3172525CE5CEF5CA (void);
// 0x00000011 System.Void canvas_force::.ctor()
extern void canvas_force__ctor_m31D5B33A208CA1C2208F58ED063A84A5AC4F1049 (void);
// 0x00000012 System.Void Data::.cctor()
extern void Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961 (void);
// 0x00000013 System.Void dondestroy::Awake()
extern void dondestroy_Awake_m076E25B2777843BF9152C493C714DE674F117FB8 (void);
// 0x00000014 System.Void dondestroy::.ctor()
extern void dondestroy__ctor_mF4F741B2DAB9832D3CAF8048C15FE7BCCE869B64 (void);
// 0x00000015 GameBG GameBG::get_Instance()
extern void GameBG_get_Instance_m2655FFB2044399BC79F94DD2417A6872ED6DA71B (void);
// 0x00000016 System.Void GameBG::Awake()
extern void GameBG_Awake_m7976188C2502858B1F8F2C78B77DAE1141F16BFF (void);
// 0x00000017 System.Void GameBG::Update()
extern void GameBG_Update_m2A691D6430B64415F8E93B1D9C11E401A9BE7E5B (void);
// 0x00000018 System.Void GameBG::.ctor()
extern void GameBG__ctor_m4631F9041032D5A73AB40B2DDDD0F009E3ECFCB2 (void);
// 0x00000019 System.Void GameSystem::Awake()
extern void GameSystem_Awake_m49B2786FF71ACC6EAAC99F840AC0F92E18ADB259 (void);
// 0x0000001A System.Void GameSystem::Start()
extern void GameSystem_Start_mF9E1A5B3DDCD3AF572B2C9251405804B47CF6241 (void);
// 0x0000001B System.Void GameSystem::scramble()
extern void GameSystem_scramble_mD8478B182BD3EB4B5F4CC8543047100E05432C90 (void);
// 0x0000001C System.Void GameSystem::scramblez()
extern void GameSystem_scramblez_m56BD4DDACBDED5113B07ED726EB73190D803F822 (void);
// 0x0000001D System.Void GameSystem::active()
extern void GameSystem_active_mF0A917D93990E582599397BA7911D17308D3B8BB (void);
// 0x0000001E System.Void GameSystem::Update()
extern void GameSystem_Update_m2AC59D669C6A807FF9D3DC70687D2EEB63777A5B (void);
// 0x0000001F System.Void GameSystem::winner()
extern void GameSystem_winner_mCC21ABFC353FABD4F183BDEC76DA7D319D287728 (void);
// 0x00000020 System.Void GameSystem::next()
extern void GameSystem_next_m2428ABC617C94E9DF27B79ED9E7ED5B723C1F290 (void);
// 0x00000021 System.Void GameSystem::changed()
extern void GameSystem_changed_mC94029FAE92D5294550947CD22FA7EEC11708ACF (void);
// 0x00000022 System.Void GameSystem::restart()
extern void GameSystem_restart_m78E3BD49C85D8A5482D1F2E5BC3C1754DDECCDE5 (void);
// 0x00000023 System.Void GameSystem::kemana(System.String)
extern void GameSystem_kemana_m1C9290A7AE40B1D42153BB1A51833B2A90AAB510 (void);
// 0x00000024 System.Void GameSystem::.ctor()
extern void GameSystem__ctor_mA7E4388F83B0D2F6973EA35D5187A08AC79053B7 (void);
// 0x00000025 System.Void ImagesAssets::.ctor()
extern void ImagesAssets__ctor_m52017AB37DCFF12A931040DF6210DF9210D53222 (void);
// 0x00000026 System.Void LevelSystem::Start()
extern void LevelSystem_Start_mDD518C2338EE9ADFCE9862687D5648E1B4122278 (void);
// 0x00000027 System.Void LevelSystem::menu()
extern void LevelSystem_menu_m5C920C1D8B9814D0359FFCDAB0B08126E19C9652 (void);
// 0x00000028 System.Void LevelSystem::WUI_Open()
extern void LevelSystem_WUI_Open_m749D627F39D02A38ED34D7944AD8A3455D5C3F94 (void);
// 0x00000029 System.Void LevelSystem::btn_No()
extern void LevelSystem_btn_No_m376F0B7F88D27A4207B3F5473F2E757BFB427B20 (void);
// 0x0000002A System.Void LevelSystem::btn_yes()
extern void LevelSystem_btn_yes_mB9D5CF1AF72317CA5E482F85C37B664A14BCC5C2 (void);
// 0x0000002B System.Void LevelSystem::.ctor()
extern void LevelSystem__ctor_mB3CC0410F3D6513C6E2800846967F0F7A8395C83 (void);
// 0x0000002C System.Void MainMenuSystem::btn_moregames(System.String)
extern void MainMenuSystem_btn_moregames_mC909FFD9B610999207940A8C9BA1886B517681A7 (void);
// 0x0000002D System.Void MainMenuSystem::btn_rateus(System.String)
extern void MainMenuSystem_btn_rateus_m09118A3F00D1F3D7FD06622D734FAA758F531F50 (void);
// 0x0000002E System.Void MainMenuSystem::btn_play()
extern void MainMenuSystem_btn_play_m9DE228CFF10F4FFEB17E2E97C12638614145F3BD (void);
// 0x0000002F System.Void MainMenuSystem::Update()
extern void MainMenuSystem_Update_m443454E09CC24225D38CA71EFA522341AE864547 (void);
// 0x00000030 System.Void MainMenuSystem::.ctor()
extern void MainMenuSystem__ctor_m60B4D90BECC677E592CE9E2F82E774C824AB74B5 (void);
// 0x00000031 Music_Singleton Music_Singleton::get_Instance()
extern void Music_Singleton_get_Instance_mA7B8E90EFA16F927E08E456E4E2840B7EC2CB498 (void);
// 0x00000032 System.Void Music_Singleton::Awake()
extern void Music_Singleton_Awake_m4A95CE9CBBAE86B8DC49215549D8201A571083D3 (void);
// 0x00000033 System.Void Music_Singleton::Start()
extern void Music_Singleton_Start_mD63A816193475735E6E538DF2D492F5EAC82B614 (void);
// 0x00000034 System.Void Music_Singleton::s_play(System.Int32)
extern void Music_Singleton_s_play_m43083697D517BA70ADAD1CF4E0A73FC5A71AB8F5 (void);
// 0x00000035 System.Void Music_Singleton::.ctor()
extern void Music_Singleton__ctor_m56D100D2237D50465DE47F70A925C0992F163E85 (void);
// 0x00000036 System.Void PemanggilBanner::Start()
extern void PemanggilBanner_Start_mDAB04547FE3835414B385AF7418952FC71CC3142 (void);
// 0x00000037 System.Void PemanggilBanner::.ctor()
extern void PemanggilBanner__ctor_mC8387338CC3AA92BFE7BDD0AA330CA04978771A1 (void);
// 0x00000038 System.Void SelectBtn::Start()
extern void SelectBtn_Start_m73C1F9FF526A924B249506516EBC2FD73276B2C1 (void);
// 0x00000039 System.Void SelectBtn::button_start()
extern void SelectBtn_button_start_m36ED2891D061BA9254A86A511FE97A3D2D5E3DCF (void);
// 0x0000003A System.Void SelectBtn::.ctor()
extern void SelectBtn__ctor_m6B0968D9099BC72920DDFFAC3E226658C56D80F8 (void);
// 0x0000003B System.Void UIz::restart()
extern void UIz_restart_m133DD60BC9689F1F2435995EE9E0EE9E2C26D083 (void);
// 0x0000003C System.Void UIz::iklan()
extern void UIz_iklan_mD337F1C7A495941BFF2F5DF3FB7C26185DF418E3 (void);
// 0x0000003D System.Void UIz::kemana()
extern void UIz_kemana_m0C3694033D2EB80F8568A85B5CA48FF6B572F0B9 (void);
// 0x0000003E System.Void UIz::gakaktif()
extern void UIz_gakaktif_m3C5244BF2C4067659704D7190EF3CCFAA590EF36 (void);
// 0x0000003F System.Void UIz::.ctor()
extern void UIz__ctor_mEE00A172DB5E578FE45A5737D399F0BCEB937B9F (void);
static Il2CppMethodPointer s_methodPointers[63] = 
{
	rectactivator_Start_m22086C3FB7180C9B22365A5AC31B765C4FE252A9,
	rectactivator__ctor_m1A10124088D84CC61EEDCCE1340CE6E499EB0CF5,
	InitializeAdmob_get_instance_m775F097969C7C96CD64A7835A5DF0F53D427EDB9,
	InitializeAdmob_Awake_m76617573C8A496E3D35AE8371DE507B88121EDA4,
	InitializeAdmob_Start_mFFD8C1EAC7AA4F50BC5E43F3A085059B97A7D653,
	InitializeAdmob_cekiklan_m7CF02EE8C7C4A167AB6295B9BDAAC2E5E1246F9A,
	InitializeAdmob_requestInter_m40FE3639B5B423D8F69ED3109F6B3E9EE7820BB0,
	InitializeAdmob_Interstitial_OnAdClosed_m4B1C439154E7C410E25D47DE642F1AF57EE97355,
	InitializeAdmob_showfullads_mC70EE3972D8EE5D88C641AEFC41C9F9874A25739,
	InitializeAdmob_showbanner_m6819F116DF0C57B9D343E7EC145BC52F78BB3112,
	InitializeAdmob_Show_Ads_Full_m8E104BA115306CFAD1F2961FF5181C87383F4AA3,
	InitializeAdmob__ctor_m583DE5BE708B6B75F5B6B34F5AF07A6D378C5B82,
	button_Script_tersentuh_m19DF08D0EA67E81461E99AAD3D35879ED50565B9,
	button_Script_Update_mF498BE5DE7A985556E0768DC44C4901547E98AFC,
	button_Script__ctor_mB645C943600628448E4B1B83720431240712728E,
	canvas_force_Update_m3055F27B205EF754B5D5B2AA3172525CE5CEF5CA,
	canvas_force__ctor_m31D5B33A208CA1C2208F58ED063A84A5AC4F1049,
	Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961,
	dondestroy_Awake_m076E25B2777843BF9152C493C714DE674F117FB8,
	dondestroy__ctor_mF4F741B2DAB9832D3CAF8048C15FE7BCCE869B64,
	GameBG_get_Instance_m2655FFB2044399BC79F94DD2417A6872ED6DA71B,
	GameBG_Awake_m7976188C2502858B1F8F2C78B77DAE1141F16BFF,
	GameBG_Update_m2A691D6430B64415F8E93B1D9C11E401A9BE7E5B,
	GameBG__ctor_m4631F9041032D5A73AB40B2DDDD0F009E3ECFCB2,
	GameSystem_Awake_m49B2786FF71ACC6EAAC99F840AC0F92E18ADB259,
	GameSystem_Start_mF9E1A5B3DDCD3AF572B2C9251405804B47CF6241,
	GameSystem_scramble_mD8478B182BD3EB4B5F4CC8543047100E05432C90,
	GameSystem_scramblez_m56BD4DDACBDED5113B07ED726EB73190D803F822,
	GameSystem_active_mF0A917D93990E582599397BA7911D17308D3B8BB,
	GameSystem_Update_m2AC59D669C6A807FF9D3DC70687D2EEB63777A5B,
	GameSystem_winner_mCC21ABFC353FABD4F183BDEC76DA7D319D287728,
	GameSystem_next_m2428ABC617C94E9DF27B79ED9E7ED5B723C1F290,
	GameSystem_changed_mC94029FAE92D5294550947CD22FA7EEC11708ACF,
	GameSystem_restart_m78E3BD49C85D8A5482D1F2E5BC3C1754DDECCDE5,
	GameSystem_kemana_m1C9290A7AE40B1D42153BB1A51833B2A90AAB510,
	GameSystem__ctor_mA7E4388F83B0D2F6973EA35D5187A08AC79053B7,
	ImagesAssets__ctor_m52017AB37DCFF12A931040DF6210DF9210D53222,
	LevelSystem_Start_mDD518C2338EE9ADFCE9862687D5648E1B4122278,
	LevelSystem_menu_m5C920C1D8B9814D0359FFCDAB0B08126E19C9652,
	LevelSystem_WUI_Open_m749D627F39D02A38ED34D7944AD8A3455D5C3F94,
	LevelSystem_btn_No_m376F0B7F88D27A4207B3F5473F2E757BFB427B20,
	LevelSystem_btn_yes_mB9D5CF1AF72317CA5E482F85C37B664A14BCC5C2,
	LevelSystem__ctor_mB3CC0410F3D6513C6E2800846967F0F7A8395C83,
	MainMenuSystem_btn_moregames_mC909FFD9B610999207940A8C9BA1886B517681A7,
	MainMenuSystem_btn_rateus_m09118A3F00D1F3D7FD06622D734FAA758F531F50,
	MainMenuSystem_btn_play_m9DE228CFF10F4FFEB17E2E97C12638614145F3BD,
	MainMenuSystem_Update_m443454E09CC24225D38CA71EFA522341AE864547,
	MainMenuSystem__ctor_m60B4D90BECC677E592CE9E2F82E774C824AB74B5,
	Music_Singleton_get_Instance_mA7B8E90EFA16F927E08E456E4E2840B7EC2CB498,
	Music_Singleton_Awake_m4A95CE9CBBAE86B8DC49215549D8201A571083D3,
	Music_Singleton_Start_mD63A816193475735E6E538DF2D492F5EAC82B614,
	Music_Singleton_s_play_m43083697D517BA70ADAD1CF4E0A73FC5A71AB8F5,
	Music_Singleton__ctor_m56D100D2237D50465DE47F70A925C0992F163E85,
	PemanggilBanner_Start_mDAB04547FE3835414B385AF7418952FC71CC3142,
	PemanggilBanner__ctor_mC8387338CC3AA92BFE7BDD0AA330CA04978771A1,
	SelectBtn_Start_m73C1F9FF526A924B249506516EBC2FD73276B2C1,
	SelectBtn_button_start_m36ED2891D061BA9254A86A511FE97A3D2D5E3DCF,
	SelectBtn__ctor_m6B0968D9099BC72920DDFFAC3E226658C56D80F8,
	UIz_restart_m133DD60BC9689F1F2435995EE9E0EE9E2C26D083,
	UIz_iklan_mD337F1C7A495941BFF2F5DF3FB7C26185DF418E3,
	UIz_kemana_m0C3694033D2EB80F8568A85B5CA48FF6B572F0B9,
	UIz_gakaktif_m3C5244BF2C4067659704D7190EF3CCFAA590EF36,
	UIz__ctor_mEE00A172DB5E578FE45A5737D399F0BCEB937B9F,
};
static const int32_t s_InvokerIndices[63] = 
{
	3253,
	3253,
	4888,
	3253,
	3253,
	3253,
	3253,
	1434,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	4912,
	3253,
	3253,
	4888,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	2638,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	2638,
	2638,
	3253,
	3253,
	3253,
	4888,
	3253,
	3253,
	2621,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	63,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
