﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void haroldmurray::Start()
extern void haroldmurray_Start_m68A41B66D3A668E5D05B4800EAF17283AE4E8C0E (void);
// 0x00000002 System.Void haroldmurray::.ctor()
extern void haroldmurray__ctor_m446BC6A2461D445E956057F69A9DFDA5A4268964 (void);
// 0x00000003 augustbenavides augustbenavides::get_Instance()
extern void augustbenavides_get_Instance_m6AC03F1C95ADE417BB58FF2DD2E956D9173E347D (void);
// 0x00000004 System.Void augustbenavides::CheckAds()
extern void augustbenavides_CheckAds_m625B93D98990C6B13CE2C391F282D29A10C346E1 (void);
// 0x00000005 System.Void augustbenavides::Awake()
extern void augustbenavides_Awake_m082133401B7551CB56D68A827B2E698FC6B0034B (void);
// 0x00000006 System.Void augustbenavides::LoadApplovin()
extern void augustbenavides_LoadApplovin_mF0D67409B6000A2D47B75D9382D2EE947A0F9613 (void);
// 0x00000007 System.Void augustbenavides::Update()
extern void augustbenavides_Update_m0E686B8F1CFF4005AB889559409180C2BBE680B3 (void);
// 0x00000008 System.Collections.IEnumerator augustbenavides::LoadScene()
extern void augustbenavides_LoadScene_m648ECFCEAA2FDFEFC52D3D00BD53BD4CBE758DCB (void);
// 0x00000009 System.Void augustbenavides::LoadInterstitial()
extern void augustbenavides_LoadInterstitial_m9BCE641FDD845FA4A179811D09E4FF22145F8D8E (void);
// 0x0000000A System.Void augustbenavides::OnInterstitialLoadedEvent(System.String,MaxSdkBase/AdInfo)
extern void augustbenavides_OnInterstitialLoadedEvent_m59067413AA02EA3A5A29D114AA9E896149D59664 (void);
// 0x0000000B System.Void augustbenavides::OnInterstitialLoadFailedEvent(System.String,MaxSdkBase/ErrorInfo)
extern void augustbenavides_OnInterstitialLoadFailedEvent_m71544314129AC500374FEA868A9262F229B94F9C (void);
// 0x0000000C System.Void augustbenavides::OnInterstitialDisplayedEvent(System.String,MaxSdkBase/AdInfo)
extern void augustbenavides_OnInterstitialDisplayedEvent_m128A233FD2151633EFB6F58BA0CC140678AD0E76 (void);
// 0x0000000D System.Void augustbenavides::OnInterstitialAdFailedToDisplayEvent(System.String,MaxSdkBase/ErrorInfo,MaxSdkBase/AdInfo)
extern void augustbenavides_OnInterstitialAdFailedToDisplayEvent_m9CBFE7D389489F15D3BBF8DCCD037F3DB0C1AA9F (void);
// 0x0000000E System.Void augustbenavides::OnInterstitialClickedEvent(System.String,MaxSdkBase/AdInfo)
extern void augustbenavides_OnInterstitialClickedEvent_m129DACA997B6D9251D6F210646B3AF2C9F805ADC (void);
// 0x0000000F System.Void augustbenavides::OnInterstitialHiddenEvent(System.String,MaxSdkBase/AdInfo)
extern void augustbenavides_OnInterstitialHiddenEvent_m312712D7381B89786DD7D330FF94E7E40EBC153A (void);
// 0x00000010 System.Void augustbenavides::ShowInterstitial()
extern void augustbenavides_ShowInterstitial_m5C4510ABBE584C85306AE5FF19B4783D45D0D61E (void);
// 0x00000011 System.Collections.IEnumerator augustbenavides::chasityhatch()
extern void augustbenavides_chasityhatch_m14F138D30D7B07F20F38074BD7045751A567B553 (void);
// 0x00000012 System.Void augustbenavides::IsVideoRewardAvailable()
extern void augustbenavides_IsVideoRewardAvailable_m01D2FB13AB09539F6E9885066122A04CB557CD77 (void);
// 0x00000013 System.Boolean augustbenavides::isVideoAvaiable()
extern void augustbenavides_isVideoAvaiable_m6BCE7D253B8F585002D18676EA4FE6B9884756AF (void);
// 0x00000014 System.Void augustbenavides::ShowApplovin()
extern void augustbenavides_ShowApplovin_m0E9E03A133231AE2D9D3E53A2CD701C31ADAC08B (void);
// 0x00000015 System.Void augustbenavides::ShowAdMob()
extern void augustbenavides_ShowAdMob_mEAB836211CEF769871ED5ABCA8103AA343431F07 (void);
// 0x00000016 System.Void augustbenavides::HandleOnAdLoaded(System.Object,System.EventArgs)
extern void augustbenavides_HandleOnAdLoaded_m57F4F41B608A5D70950135964EB55DA3509D37F4 (void);
// 0x00000017 System.Void augustbenavides::HandleOnAdOpened(System.Object,System.EventArgs)
extern void augustbenavides_HandleOnAdOpened_mB39EF69A3100F1C7616A91B06F858B0157D4DBFA (void);
// 0x00000018 System.Void augustbenavides::HandleOnAdLeavingApplication(System.Object,System.EventArgs)
extern void augustbenavides_HandleOnAdLeavingApplication_m9056C8D3771FFEE85D2E9566E5CDE14E6D58CABB (void);
// 0x00000019 System.Void augustbenavides::HandleRewardBasedVideoLoadedAdMob(System.Object,System.EventArgs)
extern void augustbenavides_HandleRewardBasedVideoLoadedAdMob_m1583F41993976B6CBC7CDE946D837D4307F38ED4 (void);
// 0x0000001A System.Void augustbenavides::HandleRewardBasedVideoOpenedAdMob(System.Object,System.EventArgs)
extern void augustbenavides_HandleRewardBasedVideoOpenedAdMob_m4DD5DD557E79CC33B343E7B06555CA39E936EE64 (void);
// 0x0000001B System.Void augustbenavides::HandleRewardBasedVideoStartedAdMob(System.Object,System.EventArgs)
extern void augustbenavides_HandleRewardBasedVideoStartedAdMob_mBBA04578061165A412B3D89352BD58C9EAAF3CCB (void);
// 0x0000001C System.Void augustbenavides::HandleRewardBasedVideoClosedAdMob(System.Object,System.EventArgs)
extern void augustbenavides_HandleRewardBasedVideoClosedAdMob_m7E9B35EE0A7D497C6E27CA558D8E935BA5A34382 (void);
// 0x0000001D System.Void augustbenavides::HandleRewardBasedVideoLeftApplicationAdMob(System.Object,System.EventArgs)
extern void augustbenavides_HandleRewardBasedVideoLeftApplicationAdMob_mF19E3B324C76F46D69D4CECA9119717AEC214502 (void);
// 0x0000001E System.Void augustbenavides::ShowInterstitialfb()
extern void augustbenavides_ShowInterstitialfb_mE2D08174A9117CD9563A50F97141E7EBCA7E0B1D (void);
// 0x0000001F System.Void augustbenavides::OnInitializationComplete()
extern void augustbenavides_OnInitializationComplete_m8621DCF4EA6E4B635646091895BA8312793FB21F (void);
// 0x00000020 System.Void augustbenavides::ShowAdUnity()
extern void augustbenavides_ShowAdUnity_m7C5E622105BA6AA1C401E9815712579A463001F8 (void);
// 0x00000021 System.Void augustbenavides::.ctor()
extern void augustbenavides__ctor_m4116F41EF398A6773362B586EC8D72DCACD4C914 (void);
// 0x00000022 System.Void augustbenavides::.cctor()
extern void augustbenavides__cctor_m6CF30187E81CACA7288FC74F64608CBF45F976A9 (void);
// 0x00000023 System.Void augustbenavides::<CheckAds>b__23_0(System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>)
extern void augustbenavides_U3CCheckAdsU3Eb__23_0_mD7EEE878801CE68A404006706ED02CA1837F9A8E (void);
// 0x00000024 System.Void augustbenavides/<>c::.cctor()
extern void U3CU3Ec__cctor_mB991A48C428951EE5D2A158DBC2168C0DC078317 (void);
// 0x00000025 System.Void augustbenavides/<>c::.ctor()
extern void U3CU3Ec__ctor_m802B7BD538898D177E9DB23735BBCB2D5D5FF327 (void);
// 0x00000026 System.Void augustbenavides/<>c::<Awake>b__24_0(MaxSdkBase/SdkConfiguration)
extern void U3CU3Ec_U3CAwakeU3Eb__24_0_mFF12207FDCE407AAB3626B371A53A0D68797E058 (void);
// 0x00000027 System.Void augustbenavides/<LoadScene>d__31::.ctor(System.Int32)
extern void U3CLoadSceneU3Ed__31__ctor_mE734CB56206A023CCB3014F8303677EE39B31F0F (void);
// 0x00000028 System.Void augustbenavides/<LoadScene>d__31::System.IDisposable.Dispose()
extern void U3CLoadSceneU3Ed__31_System_IDisposable_Dispose_m9956ACC369AF8C72E0ADC64F686B3A183D94B89B (void);
// 0x00000029 System.Boolean augustbenavides/<LoadScene>d__31::MoveNext()
extern void U3CLoadSceneU3Ed__31_MoveNext_mC0D6E77D0031739A455DCA5A0743E77BD6BA1027 (void);
// 0x0000002A System.Object augustbenavides/<LoadScene>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E1068BC7F4A0B4403C4C048BE95D132754BFD25 (void);
// 0x0000002B System.Object augustbenavides/<LoadScene>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneU3Ed__31_System_Collections_IEnumerator_get_Current_m398E14AE7A0C554FFF2453014B037B5B922F54A6 (void);
// 0x0000002C System.Void augustbenavides/<chasityhatch>d__40::.ctor(System.Int32)
extern void U3CchasityhatchU3Ed__40__ctor_m7B84BA860E07A2E20FEADDCBD6A596330D24E598 (void);
// 0x0000002D System.Void augustbenavides/<chasityhatch>d__40::System.IDisposable.Dispose()
extern void U3CchasityhatchU3Ed__40_System_IDisposable_Dispose_mF9E9F620517A1DF26AF165FA738D47F91155D1D9 (void);
// 0x0000002E System.Boolean augustbenavides/<chasityhatch>d__40::MoveNext()
extern void U3CchasityhatchU3Ed__40_MoveNext_m2C99E4D9E9DA1D92A2374B3E580434CE89D1C404 (void);
// 0x0000002F System.Object augustbenavides/<chasityhatch>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CchasityhatchU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891CEA7E6ECE9895E9D7621D7E9E6A0C770E6190 (void);
// 0x00000030 System.Object augustbenavides/<chasityhatch>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CchasityhatchU3Ed__40_System_Collections_IEnumerator_get_Current_m1F2D3B9F561BEF0C9221A4B818C0BF38BF4828EC (void);
// 0x00000031 System.Void brentkearney::Start()
extern void brentkearney_Start_m00E92AB566D90C96C867CFB2342469995CA369A3 (void);
// 0x00000032 System.Void brentkearney::OnTokenReceived(System.Object,Firebase.Messaging.TokenReceivedEventArgs)
extern void brentkearney_OnTokenReceived_m1ED02B2A08B4DDD140F7B5F71840B07E25E0C319 (void);
// 0x00000033 System.Void brentkearney::OnMessageReceived(System.Object,Firebase.Messaging.MessageReceivedEventArgs)
extern void brentkearney_OnMessageReceived_mFE2D1F8FA3A9829CAA887A86490DD3069CBD438F (void);
// 0x00000034 System.Void brentkearney::.ctor()
extern void brentkearney__ctor_m327D83CF140168278F8628B1C66499801B8D77B8 (void);
// 0x00000035 System.Void deweysanchez::Start()
extern void deweysanchez_Start_m35E31D6FA6EA6D309C1B9229246A86B51AD2E13B (void);
// 0x00000036 System.Void deweysanchez::Update()
extern void deweysanchez_Update_m47F3071AF0235E2E5F98698BEFD547B5C51FD3B9 (void);
// 0x00000037 System.Void deweysanchez::chelseysamuel()
extern void deweysanchez_chelseysamuel_m8D2C7DC7F0BEEE60F043DD3BDDECAF8DDC17B961 (void);
// 0x00000038 System.Void deweysanchez::robertvela()
extern void deweysanchez_robertvela_m320C7DDC06BF9802BD68F1053CD50BB9894ED70B (void);
// 0x00000039 System.Collections.IEnumerator deweysanchez::flossiekeene(System.String,UnityEngine.UI.Image)
extern void deweysanchez_flossiekeene_m7B80699BB54D3E0F655DBE2E13A84891BDB8EAE5 (void);
// 0x0000003A System.Void deweysanchez::.ctor()
extern void deweysanchez__ctor_m26AAF900AA84201C89000A7548CE8CC5901A2056 (void);
// 0x0000003B System.Void deweysanchez::<chelseysamuel>b__9_0(System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>)
extern void deweysanchez_U3CchelseysamuelU3Eb__9_0_m9F78ED7995CD106EF81E6319341B5E547BB36602 (void);
// 0x0000003C System.Void deweysanchez/<flossiekeene>d__12::.ctor(System.Int32)
extern void U3CflossiekeeneU3Ed__12__ctor_m0FE3B29F6234F1D71E03376D9C37B6C0D43A79AA (void);
// 0x0000003D System.Void deweysanchez/<flossiekeene>d__12::System.IDisposable.Dispose()
extern void U3CflossiekeeneU3Ed__12_System_IDisposable_Dispose_m90EC4A351F1CDA5385953D37DB0F820208F1F9F8 (void);
// 0x0000003E System.Boolean deweysanchez/<flossiekeene>d__12::MoveNext()
extern void U3CflossiekeeneU3Ed__12_MoveNext_m37275F3D3FCB6A3CB8F3C498076DC0F47754E2F0 (void);
// 0x0000003F System.Void deweysanchez/<flossiekeene>d__12::<>m__Finally1()
extern void U3CflossiekeeneU3Ed__12_U3CU3Em__Finally1_m014BF44C05B15470C43A1E1F3E6875D98CFDAC68 (void);
// 0x00000040 System.Object deweysanchez/<flossiekeene>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CflossiekeeneU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5662D07A0330943B10EF02DDB15343C4BA9C77AE (void);
// 0x00000041 System.Object deweysanchez/<flossiekeene>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CflossiekeeneU3Ed__12_System_Collections_IEnumerator_get_Current_m90F5066A8EC72A418E2C1B1D8A10704FCAD47F29 (void);
// 0x00000042 davidsaucedo davidsaucedo::get_instance()
extern void davidsaucedo_get_instance_m6D4197140CB7DA9AA56AA3C8FCF76C6B935CE18C (void);
// 0x00000043 System.Void davidsaucedo::Awake()
extern void davidsaucedo_Awake_m9BD091B210B7B53FCD94FFAA25FEB3F959D5F9C0 (void);
// 0x00000044 System.Void davidsaucedo::Start()
extern void davidsaucedo_Start_m7971EFB44A8AC1C80D429EA1926D6697BA942E07 (void);
// 0x00000045 System.Void davidsaucedo::deloresluna()
extern void davidsaucedo_deloresluna_m5481D868F30E75B8CE89C2E759838A17B89049E7 (void);
// 0x00000046 System.Void davidsaucedo::loreneelias()
extern void davidsaucedo_loreneelias_m2ADA3456ADE3E8ABECC83A6B5A80885805C80D0F (void);
// 0x00000047 System.Void davidsaucedo::shanacamacho(System.Object,System.EventArgs)
extern void davidsaucedo_shanacamacho_m31705F909D0B6D1E5D8EA3FD07CA8670659D39E0 (void);
// 0x00000048 System.Void davidsaucedo::glendacrockett()
extern void davidsaucedo_glendacrockett_m466FD61C41AB3CEDBAA54F7DD6877F6ED0FA03B7 (void);
// 0x00000049 System.Void davidsaucedo::ruthiefair()
extern void davidsaucedo_ruthiefair_m7C7FD97FA520975B233FCAF7C2196F4DDD2F83BF (void);
// 0x0000004A System.Void davidsaucedo::chasitypittman()
extern void davidsaucedo_chasitypittman_m4E485651A1A4765419E6721F16CAC5B403E2AEF5 (void);
// 0x0000004B System.Void davidsaucedo::.ctor()
extern void davidsaucedo__ctor_mCD032D8CAEA77CF1858B1396F79029B750D0A703 (void);
// 0x0000004C System.Void brandenyoung::Update()
extern void brandenyoung_Update_mA4E5A6439C54C118E4E76AB29DDC808093158170 (void);
// 0x0000004D System.Void brandenyoung::.ctor()
extern void brandenyoung__ctor_mA689A85958B9664F1B0E4C661FC64ED3BE929879 (void);
// 0x0000004E System.Void calebfloyd::.ctor()
extern void calebfloyd__ctor_mC2AD0B7C1BA71DF81EA6A95E3FA26A23A03376E7 (void);
// 0x0000004F System.Void carlodahl::Awake()
extern void carlodahl_Awake_m30BEBC0820F20C1395A2245D9323DBFA789A0BCE (void);
// 0x00000050 System.Void carlodahl::.ctor()
extern void carlodahl__ctor_mE51FE2972E64CF291000B6EC49624C9B14E9AE9F (void);
// 0x00000051 System.Void clinttripp::almameeks(System.String)
extern void clinttripp_almameeks_m22AEF30A8B30162C9CB8DB093A847049F0056C5F (void);
// 0x00000052 System.Void clinttripp::keishanewman(System.String)
extern void clinttripp_keishanewman_m51D481BE635BFB4E7A1C609D456FE1442B72E1E3 (void);
// 0x00000053 System.Void clinttripp::luannratliff()
extern void clinttripp_luannratliff_m7E9FCE40573CF806886A2E7837EB8FA89C4C556C (void);
// 0x00000054 System.Void clinttripp::Update()
extern void clinttripp_Update_mFFC5886C0930B93F29BB2EC3D346CC92B2F2F4C0 (void);
// 0x00000055 System.Void clinttripp::.ctor()
extern void clinttripp__ctor_m24ED97F19E302E2B31D4239FD361C0209EDEA6BA (void);
// 0x00000056 System.Void craighanna::margerysilva()
extern void craighanna_margerysilva_m015C2BE580FA6B49BD4989E004BD3A19534FAEE8 (void);
// 0x00000057 System.Void craighanna::iklan()
extern void craighanna_iklan_m9E329478C2EDDD78FC9B4610BADB59C7BD152D5A (void);
// 0x00000058 System.Void craighanna::elaineparsons()
extern void craighanna_elaineparsons_m7BBCFC909F5662B2C36882711E6FA8F716C544AC (void);
// 0x00000059 System.Void craighanna::gakaktif()
extern void craighanna_gakaktif_m3DED0FE54AEC02DB2CDE1625ED2BA78F909A7C8F (void);
// 0x0000005A System.Void craighanna::.ctor()
extern void craighanna__ctor_mAAE621F7FC189C41B14F96871A6E99EAC5DCB37E (void);
// 0x0000005B System.Void Data::.cctor()
extern void Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961 (void);
// 0x0000005C System.Void genaropace::.ctor()
extern void genaropace__ctor_m3A0572510AB94DB67B739C8752E6FD0D64000D02 (void);
// 0x0000005D System.Void guschurch::rachellehopkins()
extern void guschurch_rachellehopkins_m096BF0F64DB207CE17076156AADB30C164BA63D8 (void);
// 0x0000005E System.Void guschurch::.ctor()
extern void guschurch__ctor_mA327D6B4DDEF006F19D4D68241901D9590AC2E22 (void);
// 0x0000005F System.Void hansring::Awake()
extern void hansring_Awake_m2AEC201543EFE4A5BE5EBA0107C50DE6D61C7C2B (void);
// 0x00000060 System.Void hansring::Start()
extern void hansring_Start_m18120B3AAFAFF226EB10062C470FCA7DDC246A5A (void);
// 0x00000061 System.Void hansring::kristinegarza()
extern void hansring_kristinegarza_mAE9D3024457B45E6119AB0BD3108B17955C1D625 (void);
// 0x00000062 System.Void hansring::harrietgrady()
extern void hansring_harrietgrady_m47B35ABC629FD7C4B97265F11B1DD7D4AA08B568 (void);
// 0x00000063 System.Void hansring::jilllam()
extern void hansring_jilllam_m33D2D67FDB771E8F979BFFEFCCF5B0106057BC15 (void);
// 0x00000064 System.Void hansring::Update()
extern void hansring_Update_m57971E9CD10659903E0FCA9204E94384A12F91F9 (void);
// 0x00000065 System.Void hansring::carolinamadrigal()
extern void hansring_carolinamadrigal_m5F57D72B346F0F239C9E1F145424FD7F3A0D60B1 (void);
// 0x00000066 System.Void hansring::brittneyholley()
extern void hansring_brittneyholley_m758B89F68323B4293FF276C4DC541EFBB6B5DB20 (void);
// 0x00000067 System.Void hansring::violetgarrison()
extern void hansring_violetgarrison_m7BFA7BC4F127F03EE2CC19C2FD6376D84284CAA2 (void);
// 0x00000068 System.Void hansring::margerysilva()
extern void hansring_margerysilva_m751FE3931C153757695E696634FFC9450E349994 (void);
// 0x00000069 System.Void hansring::elaineparsons(System.String)
extern void hansring_elaineparsons_m5302A9A1A6DAF72F37ACB42E3ACB98C1525D017E (void);
// 0x0000006A System.Void hansring::.ctor()
extern void hansring__ctor_m60FC91DE53FAFA624FB65AC2F70B02819A3901E0 (void);
// 0x0000006B System.Void jackiehunter::Start()
extern void jackiehunter_Start_mF84A17BA6027CCC6F2424DCB944FE356F47C1ADA (void);
// 0x0000006C System.Void jackiehunter::dorisdodge()
extern void jackiehunter_dorisdodge_mDF8786C6581B3BC0346BE947F82AA9C3B61855FC (void);
// 0x0000006D System.Void jackiehunter::Update()
extern void jackiehunter_Update_m44CE6B33015AE488A73E957777FE95704702FD3B (void);
// 0x0000006E System.Void jackiehunter::.ctor()
extern void jackiehunter__ctor_m0FBFEA85B54147EFCB5B782A9B78FED24CE2DF48 (void);
// 0x0000006F System.Void kennethpage::Start()
extern void kennethpage_Start_m2EF00BD152D9CAF50B5D98E22AE290D09FC79D11 (void);
// 0x00000070 System.Void kennethpage::menu()
extern void kennethpage_menu_m959A93F78DB4D2A1728AAA8A5F0D449FA6CC7A87 (void);
// 0x00000071 System.Void kennethpage::WUI_Open()
extern void kennethpage_WUI_Open_m700BC9869126B420623180A91807CFAF71E0E15A (void);
// 0x00000072 System.Void kennethpage::btn_No()
extern void kennethpage_btn_No_mA836CEF9D2DAA0BF0E25DF9B2B8E02215930D975 (void);
// 0x00000073 System.Void kennethpage::btn_yes()
extern void kennethpage_btn_yes_mD76DB70BFDB3B3A7ED8F01131008753BFD29126A (void);
// 0x00000074 System.Void kennethpage::.ctor()
extern void kennethpage__ctor_m144DAFF5B73C3AD6AE36875755AC1956864C0B56 (void);
// 0x00000075 sergiodouglas sergiodouglas::get_Instance()
extern void sergiodouglas_get_Instance_mACDB5DCD1AF59939C3713383014906E374C268D5 (void);
// 0x00000076 System.Void sergiodouglas::Awake()
extern void sergiodouglas_Awake_m9AF49EF593FCB4DFC45E426264DEF656300852F1 (void);
// 0x00000077 System.Void sergiodouglas::Start()
extern void sergiodouglas_Start_m5EE9C3D99CBB458FB1687A77A3C515C9937C29D8 (void);
// 0x00000078 System.Void sergiodouglas::sabrinatodd(System.Int32)
extern void sergiodouglas_sabrinatodd_m4CE1BB5BD308DC9580235A0A83B42AEFF95FD54E (void);
// 0x00000079 System.Void sergiodouglas::.ctor()
extern void sergiodouglas__ctor_mF1846BE7DDDB16D50B026ADF2C68ABBEF0752B86 (void);
// 0x0000007A winfredcohen winfredcohen::get_Instance()
extern void winfredcohen_get_Instance_m9C4C9B23282AE4ED0B018B62DF852E8D0AC2B56D (void);
// 0x0000007B System.Void winfredcohen::Awake()
extern void winfredcohen_Awake_mFBA394BC9AD8F88F2C32B03756C788A228B11E68 (void);
// 0x0000007C System.Void winfredcohen::Update()
extern void winfredcohen_Update_m8121F1A411B745B5F4D7699A579B6EE9CB92F463 (void);
// 0x0000007D System.Void winfredcohen::.ctor()
extern void winfredcohen__ctor_mA2FE06F889AF36AABA1520603B5DF6C73719B0D3 (void);
// 0x0000007E System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x0000007F System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000080 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x00000081 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x00000082 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x00000083 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x00000084 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x00000085 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x00000086 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x00000087 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x00000088 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x00000089 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x0000008A System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x0000008B System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x0000008C System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x0000008D System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x0000008E System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x0000008F System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x00000090 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x00000091 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x00000092 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x00000093 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x00000094 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x00000095 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x00000096 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x00000097 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x00000098 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x00000099 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x0000009A System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x0000009B System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x0000009C System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x0000009D System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x0000009E System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x0000009F System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x000000A0 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x000000A1 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x000000A2 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x000000A3 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x000000A4 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x000000A5 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x000000A6 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x000000A7 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x000000A8 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x000000A9 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x000000AA System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x000000AB System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x000000AC System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x000000AD System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x000000AE System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x000000AF System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x000000B0 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x000000B1 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x000000B2 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x000000B3 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x000000B4 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x000000B5 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x000000B6 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x000000B7 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x000000B8 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x000000B9 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x000000BA System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x000000BB System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x000000BC System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x000000BD System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x000000BE System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x000000BF System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x000000C0 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x000000C1 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x000000C2 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x000000C3 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x000000C4 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x000000C5 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x000000C6 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x000000C7 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x000000C8 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x000000C9 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x000000CA System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x000000CB System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x000000CC System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x000000CD System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x000000CE System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000000CF System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000000D0 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000000D1 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000000D2 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000000D3 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000000D4 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000000D5 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000000D6 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000000D7 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000000D8 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000000D9 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000000DA System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000000DB System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000000DC System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000000DD System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000000DE System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000000DF System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000000E0 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000000E1 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000000E2 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000000E3 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000000E4 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000000E5 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000000E6 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000000E7 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000000E8 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000000E9 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000000EA System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000000EB System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000000EC System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000000ED System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000000EE System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000000EF System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000000F0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000000F1 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000000F2 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000000F3 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x000000F4 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x000000F5 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x000000F6 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x000000F7 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x000000F8 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x000000F9 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x000000FA System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557 (void);
// 0x000000FB System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x000000FC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x000000FD System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x000000FE System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x000000FF System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x00000100 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x00000101 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x00000102 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x00000103 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x00000104 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x00000105 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x00000106 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000107 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x00000108 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x00000109 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x0000010A System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x0000010B System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x0000010C System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x0000010D System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x0000010E System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x0000010F System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x00000110 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x00000111 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x00000112 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x00000113 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x00000114 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x00000115 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x00000116 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x00000117 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x00000118 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x00000119 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x0000011A System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x0000011B System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x0000011C System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x0000011D System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x0000011E System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x0000011F System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x00000120 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x00000121 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x00000122 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x00000123 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x00000124 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x00000125 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x00000126 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000127 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x00000128 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x00000129 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x0000012A System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x0000012B System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x0000012C System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x0000012D System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x0000012E System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x0000012F System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x00000130 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x00000131 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x00000132 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x00000133 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x00000134 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x00000135 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x00000136 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x00000137 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x00000138 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x00000139 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x0000013A System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x0000013B System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x0000013C System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x0000013D System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x0000013E System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x0000013F System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x00000140 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x00000141 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x00000142 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x00000143 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x00000144 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x00000145 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x00000146 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x00000147 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x00000148 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x00000149 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x0000014A System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x0000014B System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x0000014C System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x0000014D System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x0000014E System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x0000014F System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x00000150 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x00000151 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x00000152 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x00000155 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x00000156 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x00000157 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x00000158 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x00000159 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x0000015A System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x0000015B System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x0000015C System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x0000015D System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x0000015E System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x0000015F System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x00000160 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x00000161 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x00000162 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x00000163 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x00000164 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x00000165 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x00000166 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x00000167 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x00000168 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x00000169 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x0000016A System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x0000016B System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x0000016C System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x0000016D UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x0000016E System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x0000016F System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000170 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x00000171 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000172 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x00000173 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x00000174 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
static Il2CppMethodPointer s_methodPointers[372] = 
{
	haroldmurray_Start_m68A41B66D3A668E5D05B4800EAF17283AE4E8C0E,
	haroldmurray__ctor_m446BC6A2461D445E956057F69A9DFDA5A4268964,
	augustbenavides_get_Instance_m6AC03F1C95ADE417BB58FF2DD2E956D9173E347D,
	augustbenavides_CheckAds_m625B93D98990C6B13CE2C391F282D29A10C346E1,
	augustbenavides_Awake_m082133401B7551CB56D68A827B2E698FC6B0034B,
	augustbenavides_LoadApplovin_mF0D67409B6000A2D47B75D9382D2EE947A0F9613,
	augustbenavides_Update_m0E686B8F1CFF4005AB889559409180C2BBE680B3,
	augustbenavides_LoadScene_m648ECFCEAA2FDFEFC52D3D00BD53BD4CBE758DCB,
	augustbenavides_LoadInterstitial_m9BCE641FDD845FA4A179811D09E4FF22145F8D8E,
	augustbenavides_OnInterstitialLoadedEvent_m59067413AA02EA3A5A29D114AA9E896149D59664,
	augustbenavides_OnInterstitialLoadFailedEvent_m71544314129AC500374FEA868A9262F229B94F9C,
	augustbenavides_OnInterstitialDisplayedEvent_m128A233FD2151633EFB6F58BA0CC140678AD0E76,
	augustbenavides_OnInterstitialAdFailedToDisplayEvent_m9CBFE7D389489F15D3BBF8DCCD037F3DB0C1AA9F,
	augustbenavides_OnInterstitialClickedEvent_m129DACA997B6D9251D6F210646B3AF2C9F805ADC,
	augustbenavides_OnInterstitialHiddenEvent_m312712D7381B89786DD7D330FF94E7E40EBC153A,
	augustbenavides_ShowInterstitial_m5C4510ABBE584C85306AE5FF19B4783D45D0D61E,
	augustbenavides_chasityhatch_m14F138D30D7B07F20F38074BD7045751A567B553,
	augustbenavides_IsVideoRewardAvailable_m01D2FB13AB09539F6E9885066122A04CB557CD77,
	augustbenavides_isVideoAvaiable_m6BCE7D253B8F585002D18676EA4FE6B9884756AF,
	augustbenavides_ShowApplovin_m0E9E03A133231AE2D9D3E53A2CD701C31ADAC08B,
	augustbenavides_ShowAdMob_mEAB836211CEF769871ED5ABCA8103AA343431F07,
	augustbenavides_HandleOnAdLoaded_m57F4F41B608A5D70950135964EB55DA3509D37F4,
	augustbenavides_HandleOnAdOpened_mB39EF69A3100F1C7616A91B06F858B0157D4DBFA,
	augustbenavides_HandleOnAdLeavingApplication_m9056C8D3771FFEE85D2E9566E5CDE14E6D58CABB,
	augustbenavides_HandleRewardBasedVideoLoadedAdMob_m1583F41993976B6CBC7CDE946D837D4307F38ED4,
	augustbenavides_HandleRewardBasedVideoOpenedAdMob_m4DD5DD557E79CC33B343E7B06555CA39E936EE64,
	augustbenavides_HandleRewardBasedVideoStartedAdMob_mBBA04578061165A412B3D89352BD58C9EAAF3CCB,
	augustbenavides_HandleRewardBasedVideoClosedAdMob_m7E9B35EE0A7D497C6E27CA558D8E935BA5A34382,
	augustbenavides_HandleRewardBasedVideoLeftApplicationAdMob_mF19E3B324C76F46D69D4CECA9119717AEC214502,
	augustbenavides_ShowInterstitialfb_mE2D08174A9117CD9563A50F97141E7EBCA7E0B1D,
	augustbenavides_OnInitializationComplete_m8621DCF4EA6E4B635646091895BA8312793FB21F,
	augustbenavides_ShowAdUnity_m7C5E622105BA6AA1C401E9815712579A463001F8,
	augustbenavides__ctor_m4116F41EF398A6773362B586EC8D72DCACD4C914,
	augustbenavides__cctor_m6CF30187E81CACA7288FC74F64608CBF45F976A9,
	augustbenavides_U3CCheckAdsU3Eb__23_0_mD7EEE878801CE68A404006706ED02CA1837F9A8E,
	U3CU3Ec__cctor_mB991A48C428951EE5D2A158DBC2168C0DC078317,
	U3CU3Ec__ctor_m802B7BD538898D177E9DB23735BBCB2D5D5FF327,
	U3CU3Ec_U3CAwakeU3Eb__24_0_mFF12207FDCE407AAB3626B371A53A0D68797E058,
	U3CLoadSceneU3Ed__31__ctor_mE734CB56206A023CCB3014F8303677EE39B31F0F,
	U3CLoadSceneU3Ed__31_System_IDisposable_Dispose_m9956ACC369AF8C72E0ADC64F686B3A183D94B89B,
	U3CLoadSceneU3Ed__31_MoveNext_mC0D6E77D0031739A455DCA5A0743E77BD6BA1027,
	U3CLoadSceneU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E1068BC7F4A0B4403C4C048BE95D132754BFD25,
	U3CLoadSceneU3Ed__31_System_Collections_IEnumerator_get_Current_m398E14AE7A0C554FFF2453014B037B5B922F54A6,
	U3CchasityhatchU3Ed__40__ctor_m7B84BA860E07A2E20FEADDCBD6A596330D24E598,
	U3CchasityhatchU3Ed__40_System_IDisposable_Dispose_mF9E9F620517A1DF26AF165FA738D47F91155D1D9,
	U3CchasityhatchU3Ed__40_MoveNext_m2C99E4D9E9DA1D92A2374B3E580434CE89D1C404,
	U3CchasityhatchU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891CEA7E6ECE9895E9D7621D7E9E6A0C770E6190,
	U3CchasityhatchU3Ed__40_System_Collections_IEnumerator_get_Current_m1F2D3B9F561BEF0C9221A4B818C0BF38BF4828EC,
	brentkearney_Start_m00E92AB566D90C96C867CFB2342469995CA369A3,
	brentkearney_OnTokenReceived_m1ED02B2A08B4DDD140F7B5F71840B07E25E0C319,
	brentkearney_OnMessageReceived_mFE2D1F8FA3A9829CAA887A86490DD3069CBD438F,
	brentkearney__ctor_m327D83CF140168278F8628B1C66499801B8D77B8,
	deweysanchez_Start_m35E31D6FA6EA6D309C1B9229246A86B51AD2E13B,
	deweysanchez_Update_m47F3071AF0235E2E5F98698BEFD547B5C51FD3B9,
	deweysanchez_chelseysamuel_m8D2C7DC7F0BEEE60F043DD3BDDECAF8DDC17B961,
	deweysanchez_robertvela_m320C7DDC06BF9802BD68F1053CD50BB9894ED70B,
	deweysanchez_flossiekeene_m7B80699BB54D3E0F655DBE2E13A84891BDB8EAE5,
	deweysanchez__ctor_m26AAF900AA84201C89000A7548CE8CC5901A2056,
	deweysanchez_U3CchelseysamuelU3Eb__9_0_m9F78ED7995CD106EF81E6319341B5E547BB36602,
	U3CflossiekeeneU3Ed__12__ctor_m0FE3B29F6234F1D71E03376D9C37B6C0D43A79AA,
	U3CflossiekeeneU3Ed__12_System_IDisposable_Dispose_m90EC4A351F1CDA5385953D37DB0F820208F1F9F8,
	U3CflossiekeeneU3Ed__12_MoveNext_m37275F3D3FCB6A3CB8F3C498076DC0F47754E2F0,
	U3CflossiekeeneU3Ed__12_U3CU3Em__Finally1_m014BF44C05B15470C43A1E1F3E6875D98CFDAC68,
	U3CflossiekeeneU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5662D07A0330943B10EF02DDB15343C4BA9C77AE,
	U3CflossiekeeneU3Ed__12_System_Collections_IEnumerator_get_Current_m90F5066A8EC72A418E2C1B1D8A10704FCAD47F29,
	davidsaucedo_get_instance_m6D4197140CB7DA9AA56AA3C8FCF76C6B935CE18C,
	davidsaucedo_Awake_m9BD091B210B7B53FCD94FFAA25FEB3F959D5F9C0,
	davidsaucedo_Start_m7971EFB44A8AC1C80D429EA1926D6697BA942E07,
	davidsaucedo_deloresluna_m5481D868F30E75B8CE89C2E759838A17B89049E7,
	davidsaucedo_loreneelias_m2ADA3456ADE3E8ABECC83A6B5A80885805C80D0F,
	davidsaucedo_shanacamacho_m31705F909D0B6D1E5D8EA3FD07CA8670659D39E0,
	davidsaucedo_glendacrockett_m466FD61C41AB3CEDBAA54F7DD6877F6ED0FA03B7,
	davidsaucedo_ruthiefair_m7C7FD97FA520975B233FCAF7C2196F4DDD2F83BF,
	davidsaucedo_chasitypittman_m4E485651A1A4765419E6721F16CAC5B403E2AEF5,
	davidsaucedo__ctor_mCD032D8CAEA77CF1858B1396F79029B750D0A703,
	brandenyoung_Update_mA4E5A6439C54C118E4E76AB29DDC808093158170,
	brandenyoung__ctor_mA689A85958B9664F1B0E4C661FC64ED3BE929879,
	calebfloyd__ctor_mC2AD0B7C1BA71DF81EA6A95E3FA26A23A03376E7,
	carlodahl_Awake_m30BEBC0820F20C1395A2245D9323DBFA789A0BCE,
	carlodahl__ctor_mE51FE2972E64CF291000B6EC49624C9B14E9AE9F,
	clinttripp_almameeks_m22AEF30A8B30162C9CB8DB093A847049F0056C5F,
	clinttripp_keishanewman_m51D481BE635BFB4E7A1C609D456FE1442B72E1E3,
	clinttripp_luannratliff_m7E9FCE40573CF806886A2E7837EB8FA89C4C556C,
	clinttripp_Update_mFFC5886C0930B93F29BB2EC3D346CC92B2F2F4C0,
	clinttripp__ctor_m24ED97F19E302E2B31D4239FD361C0209EDEA6BA,
	craighanna_margerysilva_m015C2BE580FA6B49BD4989E004BD3A19534FAEE8,
	craighanna_iklan_m9E329478C2EDDD78FC9B4610BADB59C7BD152D5A,
	craighanna_elaineparsons_m7BBCFC909F5662B2C36882711E6FA8F716C544AC,
	craighanna_gakaktif_m3DED0FE54AEC02DB2CDE1625ED2BA78F909A7C8F,
	craighanna__ctor_mAAE621F7FC189C41B14F96871A6E99EAC5DCB37E,
	Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961,
	genaropace__ctor_m3A0572510AB94DB67B739C8752E6FD0D64000D02,
	guschurch_rachellehopkins_m096BF0F64DB207CE17076156AADB30C164BA63D8,
	guschurch__ctor_mA327D6B4DDEF006F19D4D68241901D9590AC2E22,
	hansring_Awake_m2AEC201543EFE4A5BE5EBA0107C50DE6D61C7C2B,
	hansring_Start_m18120B3AAFAFF226EB10062C470FCA7DDC246A5A,
	hansring_kristinegarza_mAE9D3024457B45E6119AB0BD3108B17955C1D625,
	hansring_harrietgrady_m47B35ABC629FD7C4B97265F11B1DD7D4AA08B568,
	hansring_jilllam_m33D2D67FDB771E8F979BFFEFCCF5B0106057BC15,
	hansring_Update_m57971E9CD10659903E0FCA9204E94384A12F91F9,
	hansring_carolinamadrigal_m5F57D72B346F0F239C9E1F145424FD7F3A0D60B1,
	hansring_brittneyholley_m758B89F68323B4293FF276C4DC541EFBB6B5DB20,
	hansring_violetgarrison_m7BFA7BC4F127F03EE2CC19C2FD6376D84284CAA2,
	hansring_margerysilva_m751FE3931C153757695E696634FFC9450E349994,
	hansring_elaineparsons_m5302A9A1A6DAF72F37ACB42E3ACB98C1525D017E,
	hansring__ctor_m60FC91DE53FAFA624FB65AC2F70B02819A3901E0,
	jackiehunter_Start_mF84A17BA6027CCC6F2424DCB944FE356F47C1ADA,
	jackiehunter_dorisdodge_mDF8786C6581B3BC0346BE947F82AA9C3B61855FC,
	jackiehunter_Update_m44CE6B33015AE488A73E957777FE95704702FD3B,
	jackiehunter__ctor_m0FBFEA85B54147EFCB5B782A9B78FED24CE2DF48,
	kennethpage_Start_m2EF00BD152D9CAF50B5D98E22AE290D09FC79D11,
	kennethpage_menu_m959A93F78DB4D2A1728AAA8A5F0D449FA6CC7A87,
	kennethpage_WUI_Open_m700BC9869126B420623180A91807CFAF71E0E15A,
	kennethpage_btn_No_mA836CEF9D2DAA0BF0E25DF9B2B8E02215930D975,
	kennethpage_btn_yes_mD76DB70BFDB3B3A7ED8F01131008753BFD29126A,
	kennethpage__ctor_m144DAFF5B73C3AD6AE36875755AC1956864C0B56,
	sergiodouglas_get_Instance_mACDB5DCD1AF59939C3713383014906E374C268D5,
	sergiodouglas_Awake_m9AF49EF593FCB4DFC45E426264DEF656300852F1,
	sergiodouglas_Start_m5EE9C3D99CBB458FB1687A77A3C515C9937C29D8,
	sergiodouglas_sabrinatodd_m4CE1BB5BD308DC9580235A0A83B42AEFF95FD54E,
	sergiodouglas__ctor_mF1846BE7DDDB16D50B026ADF2C68ABBEF0752B86,
	winfredcohen_get_Instance_m9C4C9B23282AE4ED0B018B62DF852E8D0AC2B56D,
	winfredcohen_Awake_mFBA394BC9AD8F88F2C32B03756C788A228B11E68,
	winfredcohen_Update_m8121F1A411B745B5F4D7699A579B6EE9CB92F463,
	winfredcohen__ctor_mA2FE06F889AF36AABA1520603B5DF6C73719B0D3,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
};
static const int32_t s_InvokerIndices[372] = 
{
	3316,
	3316,
	5082,
	3316,
	3316,
	3316,
	3316,
	3228,
	3316,
	1458,
	1458,
	1458,
	788,
	1458,
	1458,
	3316,
	3228,
	3316,
	3168,
	3316,
	3316,
	1458,
	1458,
	1458,
	1458,
	1458,
	1458,
	1458,
	1458,
	3316,
	3316,
	3316,
	3316,
	5107,
	2688,
	5107,
	3316,
	2688,
	2670,
	3316,
	3168,
	3228,
	3228,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	1458,
	1458,
	3316,
	3316,
	3316,
	3316,
	3316,
	1112,
	3316,
	2688,
	2670,
	3316,
	3168,
	3316,
	3228,
	3228,
	5082,
	3316,
	3316,
	3316,
	3316,
	1458,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	2688,
	2688,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	5107,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	2688,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	5082,
	3316,
	3316,
	2670,
	3316,
	5082,
	3316,
	3316,
	3316,
	3316,
	3316,
	2688,
	3316,
	3316,
	3316,
	3316,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	677,
	3316,
	677,
	3316,
	3228,
	2688,
	3228,
	2688,
	3228,
	2688,
	3228,
	2688,
	3228,
	2688,
	3316,
	3316,
	2688,
	2688,
	1494,
	1494,
	771,
	771,
	787,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	3316,
	2362,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	2688,
	2362,
	2362,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3228,
	3228,
	3316,
	5107,
	2670,
	3316,
	3168,
	3228,
	3228,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	2670,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	3316,
	2670,
	3316,
	3316,
	3316,
	1494,
	1494,
	771,
	771,
	787,
	3316,
	3316,
	3316,
	3316,
	2688,
	2688,
	3316,
	3316,
	3316,
	3316,
	2688,
	3316,
	2688,
	2688,
	2688,
	2688,
	2670,
	3316,
	3316,
	3316,
	3316,
	2670,
	3316,
	3316,
	3316,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	2688,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	2688,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	2688,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	3316,
	3316,
	2688,
	3228,
	3316,
	3316,
	992,
	2670,
	3316,
	3168,
	3228,
	3228,
	3316,
	3316,
	2362,
	3228,
	3316,
	2670,
	3316,
	3168,
	3228,
	3228,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	372,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
