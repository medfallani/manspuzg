﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* LogUtil_LogMessageFromCallback_m3EA336850B4BE115C393BA3AD71981D1AA654307_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m7FE3B7ADC198F4ED5A180BC5ECD18CC371444591_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m82CC529F5355DF173784D29CDB197BC3AAA353BC_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m1A239C193A01B3E73BD763718FB528ED933847A0_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1F079CDB1AC454648BEFF38716F88AFE6FA8F926_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m5DA562871B81FA3E688FD12D78E82882F5ADC315_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_m7AE515E72B8E23D18919432B5B7BF0F06CCD18E7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mA50448F1AA4CA664C39B8AB78EF912F18E0DDF50_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mCD203C03B85ADB38206622594E5DEECA14C1CA7E_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m64057305E28A3122C79BFF5A8C441D72B04C6E5B_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m4CE89FA918E3D9CA7C6391147792F8226CF6BA07_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m0CE8D326228371436AB3BBCE9AA7464619030A35_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mB4209DD263A50C83F1E9CE39A85ADDAE18F51759_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m366DEFCF657EFE4CBABD2ADCD7D09BD6144E25B0_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mE317DA81F256CF3BD75CAC264E25961D7A536191_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m929C4B0922610C0571E685F27F79757BF669C77F_RuntimeMethod_var;



// 0x00000001 System.Void Firebase.AppUtilPINVOKE::.cctor()
extern void AppUtilPINVOKE__cctor_m8F7249FF9A16A52D144B2223D3CB9EA2195CE6DF (void);
// 0x00000002 System.Void Firebase.AppUtilPINVOKE::delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_FutureBase_mBA1AC9D7BE7977A080996ED289972DF8449F7BC5 (void);
// 0x00000003 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_status(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_status_m3056FA61BBB6B002A5BEE4F34F75DCD0858BC27C (void);
// 0x00000004 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_error(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_mDB0C901628E9EE247400F57E7E697743B69906E3 (void);
// 0x00000005 System.String Firebase.AppUtilPINVOKE::FutureBase_error_message(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_message_mDB4089A47D35BF6D3457750C5355B1908C4BE46B (void);
// 0x00000006 System.String Firebase.AppUtilPINVOKE::FirebaseApp_NameInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_NameInternal_get_mE4931CA287567E2C7F91442E07F30AE96AE91172 (void);
// 0x00000007 System.IntPtr Firebase.AppUtilPINVOKE::FirebaseApp_CreateInternal__SWIG_0()
extern void AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_m8C61ECBE5407D4A591827E10595AE28EFE8C6CE5 (void);
// 0x00000008 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_m5A184BBB9E09539CDDB771120995526D0328439B (void);
// 0x00000009 System.Int32 Firebase.AppUtilPINVOKE::FirebaseApp_GetLogLevelInternal()
extern void AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_mAF74BAD43730ACCFFB25EFBDCB0F40CA30CE1940 (void);
// 0x0000000A System.Void Firebase.AppUtilPINVOKE::FirebaseApp_RegisterLibraryInternal(System.String,System.String)
extern void AppUtilPINVOKE_FirebaseApp_RegisterLibraryInternal_mB4C1184BF2D1A163A2B3E91C6B91471CB7BB3E1F (void);
// 0x0000000B System.Void Firebase.AppUtilPINVOKE::FirebaseApp_AppSetDefaultConfigPath(System.String)
extern void AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_m461EBC1DCEA9353F3C927FAEEB1590BA130E69DB (void);
// 0x0000000C System.String Firebase.AppUtilPINVOKE::FirebaseApp_DefaultName_get()
extern void AppUtilPINVOKE_FirebaseApp_DefaultName_get_m7561CF63339BB772F6A9690B9E07D9E21BA70D2A (void);
// 0x0000000D System.Void Firebase.AppUtilPINVOKE::PollCallbacks()
extern void AppUtilPINVOKE_PollCallbacks_mD2FF7C60A52AF22E9AC028564A06E0F3974B6D56 (void);
// 0x0000000E System.Void Firebase.AppUtilPINVOKE::AppEnableLogCallback(System.Boolean)
extern void AppUtilPINVOKE_AppEnableLogCallback_m5BB69B725FD3DF1FE26C20DE516F14E02E82BDB3 (void);
// 0x0000000F System.Void Firebase.AppUtilPINVOKE::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAllAppCallbacks_m6A273BFF682F24C5D1F66273B0AA3AF975B29019 (void);
// 0x00000010 System.Void Firebase.AppUtilPINVOKE::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAppCallbackByName_m4705ADB8109C59533C8D2117C87E6336EEB54A05 (void);
// 0x00000011 System.Boolean Firebase.AppUtilPINVOKE::GetEnabledAppCallbackByName(System.String)
extern void AppUtilPINVOKE_GetEnabledAppCallbackByName_m97E56E8BCA68A8391B677BC814B1E3584FD985C5 (void);
// 0x00000012 System.Void Firebase.AppUtilPINVOKE::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtilPINVOKE_SetLogFunction_m76775D9FA055C83D4C65B6E6E7192E941A433EAE (void);
// 0x00000013 System.UInt32 Firebase.AppUtilPINVOKE::VariantVariantMap_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_size_m47D217D462E3AA2C0479998CEEC5F0AF5FB9A6AF (void);
// 0x00000014 System.Void Firebase.AppUtilPINVOKE::VariantVariantMap_Clear(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_Clear_mC15F99B817AE7002EB8B6847B9FC4262AF47ED95 (void);
// 0x00000015 System.IntPtr Firebase.AppUtilPINVOKE::VariantVariantMap_getitem(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_getitem_m8012D3A44F32A4B4A12B3B7C9A1F9804FA889E2C (void);
// 0x00000016 System.Void Firebase.AppUtilPINVOKE::VariantVariantMap_setitem(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_setitem_m59D72239FEC0A32146ED5A9F50A5EEA2FA9CED0D (void);
// 0x00000017 System.Boolean Firebase.AppUtilPINVOKE::VariantVariantMap_ContainsKey(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_ContainsKey_m8E9F83BAA16786DEA9AA2A23B18BCB67570C2A62 (void);
// 0x00000018 System.Void Firebase.AppUtilPINVOKE::VariantVariantMap_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_Add_m7693EF89F3612F62D27F1B7BFFAE93C0D9895FAA (void);
// 0x00000019 System.Boolean Firebase.AppUtilPINVOKE::VariantVariantMap_Remove(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_Remove_mBA53C94FFC339ADCA616B99BC9B3DE9C7184D08B (void);
// 0x0000001A System.IntPtr Firebase.AppUtilPINVOKE::VariantVariantMap_create_iterator_begin(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantVariantMap_create_iterator_begin_m7A904CC444AF4397B7038AD0AB0C14B3ECD21E1B (void);
// 0x0000001B System.IntPtr Firebase.AppUtilPINVOKE::VariantVariantMap_get_next_key(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_VariantVariantMap_get_next_key_mF61D7A043695C1BB6C22D37C0C45821647E91411 (void);
// 0x0000001C System.Void Firebase.AppUtilPINVOKE::VariantVariantMap_destroy_iterator(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_VariantVariantMap_destroy_iterator_m73C0BA71E2300B2C6A255129175E8C2B37EA85C4 (void);
// 0x0000001D System.Void Firebase.AppUtilPINVOKE::delete_VariantVariantMap(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_VariantVariantMap_mFE3E435232BF15367FA092A1098ED3FDEC0ABA0B (void);
// 0x0000001E System.UInt32 Firebase.AppUtilPINVOKE::VariantList_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_VariantList_size_m3A48821A74BE9435C6B2EF9632FA19F2D77C81EF (void);
// 0x0000001F System.IntPtr Firebase.AppUtilPINVOKE::VariantList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AppUtilPINVOKE_VariantList_getitem_mDBA8806675B9D11F3074AE72C844D01EF9F6A3FF (void);
// 0x00000020 System.Void Firebase.AppUtilPINVOKE::delete_VariantList(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_VariantList_mFCC6CA449E1F1B85B7D2EFB7A9735C2CFD7F1BC3 (void);
// 0x00000021 System.Void Firebase.AppUtilPINVOKE::delete_Variant(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_Variant_mA45ED3EFF2C315AFC59E0FF4A3950E6CA06EE363 (void);
// 0x00000022 System.Int32 Firebase.AppUtilPINVOKE::Variant_type(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_type_m0575207999A99C1679D7E9ABF70BF42B2DBA13AA (void);
// 0x00000023 System.Boolean Firebase.AppUtilPINVOKE::Variant_is_string(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_is_string_m0F75C01B34DB5651C00B60D0F477BF7BDD22DF3F (void);
// 0x00000024 System.Boolean Firebase.AppUtilPINVOKE::Variant_is_fundamental_type(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_is_fundamental_type_m8B68412DA67C92C82E313F5F7CE9CFF9C70F40FC (void);
// 0x00000025 System.IntPtr Firebase.AppUtilPINVOKE::Variant_AsString(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_AsString_m14A8B144EA7FAF0396478ACF233863E9CFB982AA (void);
// 0x00000026 System.UInt32 Firebase.AppUtilPINVOKE::Variant_blob_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_blob_size_m848887376DB001FD4066EFB4DAC5F13D3A7E4712 (void);
// 0x00000027 System.IntPtr Firebase.AppUtilPINVOKE::Variant_vector__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_vector__SWIG_0_m2EEE0E67CF5880090850B48A7D84B644AC1849AA (void);
// 0x00000028 System.IntPtr Firebase.AppUtilPINVOKE::Variant_map__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_map__SWIG_0_mE008492E043F7AE7554961B5382C1D6BFCCCEDD9 (void);
// 0x00000029 System.Int64 Firebase.AppUtilPINVOKE::Variant_int64_value(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_int64_value_m7215A286D3C0BDD480ED52735607F63039BDB947 (void);
// 0x0000002A System.Double Firebase.AppUtilPINVOKE::Variant_double_value(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_double_value_m3F37AD68D935C0EE9CED1063E55D2D47AE89FE7C (void);
// 0x0000002B System.Boolean Firebase.AppUtilPINVOKE::Variant_bool_value(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_bool_value_m9A7C00507E31A2EAA81D670B132BFE78D23ACDFB (void);
// 0x0000002C System.String Firebase.AppUtilPINVOKE::Variant_string_value(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_string_value_mCFC1313C97830F6EC780F3A3E37A2C8E898E45EB (void);
// 0x0000002D System.IntPtr Firebase.AppUtilPINVOKE::Variant_untyped_mutable_blob_data(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Variant_untyped_mutable_blob_data_mA898DA666F9663143AED17659F774773F2D6AC18 (void);
// 0x0000002E System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m2E9064FCCB3E37EAE10ED204AF7A72C0F1F78F8B (void);
// 0x0000002F System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_m4DB6794D8CB5F1A9740C37B0C257B69982C013B9 (void);
// 0x00000030 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m37C58735D4F8200BA3896F46FD46006F467A604D (void);
// 0x00000031 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m20CAE98AF0AFEDCADBBEAA94148843C8C671F8D5 (void);
// 0x00000032 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m7FE3B7ADC198F4ED5A180BC5ECD18CC371444591 (void);
// 0x00000033 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m5DA562871B81FA3E688FD12D78E82882F5ADC315 (void);
// 0x00000034 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_m7AE515E72B8E23D18919432B5B7BF0F06CCD18E7 (void);
// 0x00000035 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mCD203C03B85ADB38206622594E5DEECA14C1CA7E (void);
// 0x00000036 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m64057305E28A3122C79BFF5A8C441D72B04C6E5B (void);
// 0x00000037 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m4CE89FA918E3D9CA7C6391147792F8226CF6BA07 (void);
// 0x00000038 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_mA50448F1AA4CA664C39B8AB78EF912F18E0DDF50 (void);
// 0x00000039 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m0CE8D326228371436AB3BBCE9AA7464619030A35 (void);
// 0x0000003A System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mB4209DD263A50C83F1E9CE39A85ADDAE18F51759 (void);
// 0x0000003B System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m366DEFCF657EFE4CBABD2ADCD7D09BD6144E25B0 (void);
// 0x0000003C System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_mE317DA81F256CF3BD75CAC264E25961D7A536191 (void);
// 0x0000003D System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m82CC529F5355DF173784D29CDB197BC3AAA353BC (void);
// 0x0000003E System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_m1A239C193A01B3E73BD763718FB528ED933847A0 (void);
// 0x0000003F System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1F079CDB1AC454648BEFF38716F88AFE6FA8F926 (void);
// 0x00000040 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m49AB94CEC8E6544CE0D7B1E2300735728EE336D8 (void);
// 0x00000041 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_mE04E9A1D96F5AE159E3D7878E87706B91A149B25 (void);
// 0x00000042 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m45E4EFAE5F14FFEC5843A00ABEF4D0E1F0854629 (void);
// 0x00000043 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m46BEB8B17B239BFB498C54B89EB06352BD1948F8 (void);
// 0x00000044 System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_mFF0B94BEAD2C48E3CD3BB7EFB5EB23A0B6CF55EE (void);
// 0x00000045 System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m7EB577A3129E190D2FBAF8CB9C9CAB7F87DA2642 (void);
// 0x00000046 System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m5AF2B72B0C255B87F0044137B357F55F50101D51 (void);
// 0x00000047 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_mAEBC8A14986139AE4856DA02EC5F3748CACE1971 (void);
// 0x00000048 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_mF5EBABDC102D937A919B6A6CCA3690E2244ECE85 (void);
// 0x00000049 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m6DC4FCA4611C8A5521BE0E65F5CCE26D000E4DD1 (void);
// 0x0000004A System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m929C4B0922610C0571E685F27F79757BF669C77F (void);
// 0x0000004B System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_m4D5B167B33345B58192AD3B50D1F8901A18F4F4D (void);
// 0x0000004C System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_m9831BD87E0EBFECFF48B4CA3FFEB118280C2ABCB (void);
// 0x0000004D System.Void Firebase.AppUtil::PollCallbacks()
extern void AppUtil_PollCallbacks_mB25BC1FD5126974F80860A05910301AAF7589BFC (void);
// 0x0000004E System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern void AppUtil_AppEnableLogCallback_m9FE8159D116019E4E918F4B7CEC39687DD64B2EA (void);
// 0x0000004F System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtil_SetEnabledAllAppCallbacks_m0C41A4271764464915ACA461AE8A309AEDAFA6AC (void);
// 0x00000050 System.Void Firebase.AppUtil::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtil_SetEnabledAppCallbackByName_m59C6CB27D92D72FD0841031C207D86C2F684F366 (void);
// 0x00000051 System.Boolean Firebase.AppUtil::GetEnabledAppCallbackByName(System.String)
extern void AppUtil_GetEnabledAppCallbackByName_m979A86ABDBC2257B697ABDE5015AA76DE5D04F70 (void);
// 0x00000052 System.Void Firebase.AppUtil::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtil_SetLogFunction_m1C535B70AA8069AC3CE81CB05882946F841DACCB (void);
// 0x00000053 System.Void Firebase.Variant::.ctor(System.IntPtr,System.Boolean)
extern void Variant__ctor_mCBCDC0D6E1290E1A8412B59A4A8956BC32DFA163 (void);
// 0x00000054 System.Runtime.InteropServices.HandleRef Firebase.Variant::getCPtr(Firebase.Variant)
extern void Variant_getCPtr_m02745459FA3350FFDC29D01587C8D797BFC10067 (void);
// 0x00000055 System.Void Firebase.Variant::Finalize()
extern void Variant_Finalize_mAB5A4CC29AC40AF0FD7FFF427E671CA7796E9E0C (void);
// 0x00000056 System.Void Firebase.Variant::Dispose()
extern void Variant_Dispose_m68085851C3EE3DDDB413CAE4B1B4E237D0A87FBE (void);
// 0x00000057 System.Byte[] Firebase.Variant::blob_as_bytes()
extern void Variant_blob_as_bytes_mCA950A447F7928E4E3A303D09C0003EFB2A491EF (void);
// 0x00000058 Firebase.Variant/Type Firebase.Variant::type()
extern void Variant_type_m03A0A0BFC610CF0047576B5CB15DC7E58E00D0B0 (void);
// 0x00000059 System.Boolean Firebase.Variant::is_string()
extern void Variant_is_string_m2CD01E3123A174A575C240FD125F3E43CEAB73A8 (void);
// 0x0000005A System.Boolean Firebase.Variant::is_fundamental_type()
extern void Variant_is_fundamental_type_mB173361527A94BAEEBBF8747805DD81AD1BD9E2C (void);
// 0x0000005B Firebase.Variant Firebase.Variant::AsString()
extern void Variant_AsString_m88B77BD34C54D8FF8596675C201FAA43945EA758 (void);
// 0x0000005C System.UInt32 Firebase.Variant::blob_size()
extern void Variant_blob_size_mCF0A2FFB1862051AAFE295694E62A87F03228563 (void);
// 0x0000005D Firebase.VariantList Firebase.Variant::vector()
extern void Variant_vector_m605D7667ABCF391DD582F236746338AFEDD03262 (void);
// 0x0000005E Firebase.VariantVariantMap Firebase.Variant::map()
extern void Variant_map_mB7447AAFBC3421A6EEAE48E5A6FA13F08FA66597 (void);
// 0x0000005F System.Int64 Firebase.Variant::int64_value()
extern void Variant_int64_value_m11AE869002EB87F8958F3AF7122C33C466557C55 (void);
// 0x00000060 System.Double Firebase.Variant::double_value()
extern void Variant_double_value_m66B4F265644B4F4B4DA1C5CDC23D220C62E07DAA (void);
// 0x00000061 System.Boolean Firebase.Variant::bool_value()
extern void Variant_bool_value_m2AC1B3119BC6AD8B1BE18DA36B721B0CCBEE9CAC (void);
// 0x00000062 System.String Firebase.Variant::string_value()
extern void Variant_string_value_mFC641893647B7A1023754C2A28C8FC3DCB39B0C7 (void);
// 0x00000063 System.IntPtr Firebase.Variant::untyped_mutable_blob_data()
extern void Variant_untyped_mutable_blob_data_m69D26F368BADF4C3D26A201EFED448CE0A16169E (void);
// 0x00000064 System.Void Firebase.VariantList::.ctor(System.IntPtr,System.Boolean)
extern void VariantList__ctor_m61AA2F0090993B8A446B5F5F4605044D353F34DF (void);
// 0x00000065 System.Void Firebase.VariantList::Finalize()
extern void VariantList_Finalize_mE6E6EEDD2892B26B954A226E29E88147937C31DD (void);
// 0x00000066 System.Void Firebase.VariantList::Dispose()
extern void VariantList_Dispose_mCCC4F2FC0519A530EEAE62AD612EA962E14092C6 (void);
// 0x00000067 Firebase.Variant Firebase.VariantList::get_Item(System.Int32)
extern void VariantList_get_Item_m6DB080671B53261F20EC0BEFE80E72CB0F1945C2 (void);
// 0x00000068 System.Int32 Firebase.VariantList::get_Count()
extern void VariantList_get_Count_m912730C365BD33F01CC1665B3D099BA5D8B98F7D (void);
// 0x00000069 System.Collections.Generic.IEnumerator`1<Firebase.Variant> Firebase.VariantList::System.Collections.Generic.IEnumerable<Firebase.Variant>.GetEnumerator()
extern void VariantList_System_Collections_Generic_IEnumerableU3CFirebase_VariantU3E_GetEnumerator_m3E9FA3584B4B641FA48272FA89902A3E4C4B103A (void);
// 0x0000006A System.Collections.IEnumerator Firebase.VariantList::System.Collections.IEnumerable.GetEnumerator()
extern void VariantList_System_Collections_IEnumerable_GetEnumerator_mCF9510C458E396BA0926E077FB9C6CC9C2BF451C (void);
// 0x0000006B Firebase.VariantList/VariantListEnumerator Firebase.VariantList::GetEnumerator()
extern void VariantList_GetEnumerator_m7F02DAF1A3C06A5CE8F316DC8ABB81E39F209AAD (void);
// 0x0000006C System.UInt32 Firebase.VariantList::size()
extern void VariantList_size_m328AD0C3BE60BA20EC49B39067F51317241D58B1 (void);
// 0x0000006D Firebase.Variant Firebase.VariantList::getitem(System.Int32)
extern void VariantList_getitem_mC418034502AA2DDC2B866F7175BA912D506A0DF3 (void);
// 0x0000006E System.Void Firebase.VariantList/VariantListEnumerator::.ctor(Firebase.VariantList)
extern void VariantListEnumerator__ctor_m55CB33FB76E96E989600F557E45AFFBF440C04C7 (void);
// 0x0000006F Firebase.Variant Firebase.VariantList/VariantListEnumerator::get_Current()
extern void VariantListEnumerator_get_Current_mB5DE10FA7F3EF3D40024B568B035DDA9188000DD (void);
// 0x00000070 System.Object Firebase.VariantList/VariantListEnumerator::System.Collections.IEnumerator.get_Current()
extern void VariantListEnumerator_System_Collections_IEnumerator_get_Current_m305D2B35CB662715F6268DEA743BE4BD18E352A5 (void);
// 0x00000071 System.Boolean Firebase.VariantList/VariantListEnumerator::MoveNext()
extern void VariantListEnumerator_MoveNext_mB3356E9DA1734FE027E0F0F63CA8D0859C864117 (void);
// 0x00000072 System.Void Firebase.VariantList/VariantListEnumerator::Dispose()
extern void VariantListEnumerator_Dispose_mA490806D774FDDEA003913DD68EA5290E6B1B3ED (void);
// 0x00000073 System.Void Firebase.VariantVariantMap::.ctor(System.IntPtr,System.Boolean)
extern void VariantVariantMap__ctor_m41C3F31D743DEF93EBA676BE557F1A70E4BE7334 (void);
// 0x00000074 System.Void Firebase.VariantVariantMap::Finalize()
extern void VariantVariantMap_Finalize_mDAF681083D55595180D8A687C342C8B75265DDB2 (void);
// 0x00000075 System.Void Firebase.VariantVariantMap::Dispose()
extern void VariantVariantMap_Dispose_m2EF5C536EF12EF2311C8107618F38C6DEC115D17 (void);
// 0x00000076 Firebase.Variant Firebase.VariantVariantMap::get_Item(Firebase.Variant)
extern void VariantVariantMap_get_Item_mEE387DB235EC361518878AB36DAD8E9D508375FB (void);
// 0x00000077 System.Void Firebase.VariantVariantMap::set_Item(Firebase.Variant,Firebase.Variant)
extern void VariantVariantMap_set_Item_m3400A43959480801333DE6EF2C6678B07B0992BA (void);
// 0x00000078 System.Boolean Firebase.VariantVariantMap::TryGetValue(Firebase.Variant,Firebase.Variant&)
extern void VariantVariantMap_TryGetValue_mCEE65FA7D0E6BF754F3D250D2830FA38FA39786B (void);
// 0x00000079 System.Int32 Firebase.VariantVariantMap::get_Count()
extern void VariantVariantMap_get_Count_m2BEE44655BF6BD9D696D0D59C23BC6BBFB033A98 (void);
// 0x0000007A System.Boolean Firebase.VariantVariantMap::get_IsReadOnly()
extern void VariantVariantMap_get_IsReadOnly_m27A6D9FE6FDA1F97EE831FF81A567FC2F61EEB0A (void);
// 0x0000007B System.Collections.Generic.ICollection`1<Firebase.Variant> Firebase.VariantVariantMap::get_Keys()
extern void VariantVariantMap_get_Keys_m2C2F2F500CDE16BCD958453ECD0DD537C18D511C (void);
// 0x0000007C System.Void Firebase.VariantVariantMap::Add(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern void VariantVariantMap_Add_m290B443C10658813255555A1DD6A13A2E4260ED1 (void);
// 0x0000007D System.Boolean Firebase.VariantVariantMap::Remove(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern void VariantVariantMap_Remove_m38A98E00BA4B24B7B9A73B5A93F83D4A73565E83 (void);
// 0x0000007E System.Boolean Firebase.VariantVariantMap::Contains(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern void VariantVariantMap_Contains_m92DA6A5CCE3AB281CE9E729CD7FECBD2790BD602 (void);
// 0x0000007F System.Void Firebase.VariantVariantMap::CopyTo(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>[],System.Int32)
extern void VariantVariantMap_CopyTo_mF75ECD5C29F30F9F482126DBEA0E99D5A26436BE (void);
// 0x00000080 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>> Firebase.VariantVariantMap::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<Firebase.Variant,Firebase.Variant>>.GetEnumerator()
extern void VariantVariantMap_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CFirebase_VariantU2CFirebase_VariantU3EU3E_GetEnumerator_m801B90F2D5B346799F4D77E27865CEA05B94BE9E (void);
// 0x00000081 System.Collections.IEnumerator Firebase.VariantVariantMap::System.Collections.IEnumerable.GetEnumerator()
extern void VariantVariantMap_System_Collections_IEnumerable_GetEnumerator_mA754DFF662C1032AA3379FDA3056FB9581AEF8DE (void);
// 0x00000082 Firebase.VariantVariantMap/VariantVariantMapEnumerator Firebase.VariantVariantMap::GetEnumerator()
extern void VariantVariantMap_GetEnumerator_m360952FBCF0F2F6AE30CA92612D50F9043894567 (void);
// 0x00000083 System.UInt32 Firebase.VariantVariantMap::size()
extern void VariantVariantMap_size_m2770CDA09C92D8766E4A857F1A73E2A52565B1FD (void);
// 0x00000084 System.Void Firebase.VariantVariantMap::Clear()
extern void VariantVariantMap_Clear_m5BDCCEC4D9C6FD388B5AFE22808BA04693BEDBFC (void);
// 0x00000085 Firebase.Variant Firebase.VariantVariantMap::getitem(Firebase.Variant)
extern void VariantVariantMap_getitem_mB7316DB5AA86AF857DF7CFD03949E8A8290C6C1A (void);
// 0x00000086 System.Void Firebase.VariantVariantMap::setitem(Firebase.Variant,Firebase.Variant)
extern void VariantVariantMap_setitem_mDDA297409628E8F3E024CE64A2FECD629E7579B8 (void);
// 0x00000087 System.Boolean Firebase.VariantVariantMap::ContainsKey(Firebase.Variant)
extern void VariantVariantMap_ContainsKey_mFC7BDCB3103730A4FA0D5B94107F71CCC85E3955 (void);
// 0x00000088 System.Void Firebase.VariantVariantMap::Add(Firebase.Variant,Firebase.Variant)
extern void VariantVariantMap_Add_m0487DEEFE55FF79967F7A7583F7563408261BBF1 (void);
// 0x00000089 System.Boolean Firebase.VariantVariantMap::Remove(Firebase.Variant)
extern void VariantVariantMap_Remove_m6916F106939189219FBC8DB48CFB407F0CC7885A (void);
// 0x0000008A System.IntPtr Firebase.VariantVariantMap::create_iterator_begin()
extern void VariantVariantMap_create_iterator_begin_m131671A481692CB8BA53945C4D5731B034CDEE12 (void);
// 0x0000008B Firebase.Variant Firebase.VariantVariantMap::get_next_key(System.IntPtr)
extern void VariantVariantMap_get_next_key_m088DEF53A9746BEB15DD8B344C2AFB232B4A6DD3 (void);
// 0x0000008C System.Void Firebase.VariantVariantMap::destroy_iterator(System.IntPtr)
extern void VariantVariantMap_destroy_iterator_m754B78CB27395071ABE1E283241CCFEECA0ABD38 (void);
// 0x0000008D System.Void Firebase.VariantVariantMap/VariantVariantMapEnumerator::.ctor(Firebase.VariantVariantMap)
extern void VariantVariantMapEnumerator__ctor_m19EBFEA767C0CD94BAAC0028E8878CC804AD68E2 (void);
// 0x0000008E System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant> Firebase.VariantVariantMap/VariantVariantMapEnumerator::get_Current()
extern void VariantVariantMapEnumerator_get_Current_m2DDA678E632DBC094DBD1C613CB572AECCC6FC2A (void);
// 0x0000008F System.Object Firebase.VariantVariantMap/VariantVariantMapEnumerator::System.Collections.IEnumerator.get_Current()
extern void VariantVariantMapEnumerator_System_Collections_IEnumerator_get_Current_mA7F4DDCBABA52036A1BCC6524BAC28E9CE10C1E0 (void);
// 0x00000090 System.Boolean Firebase.VariantVariantMap/VariantVariantMapEnumerator::MoveNext()
extern void VariantVariantMapEnumerator_MoveNext_mBD4ECB27F072218FD3DE27476AA880BB530A0F06 (void);
// 0x00000091 System.Void Firebase.VariantVariantMap/VariantVariantMapEnumerator::Dispose()
extern void VariantVariantMapEnumerator_Dispose_m991451F9BDD4DC411399D475FFDEEACC3A7A4BC3 (void);
// 0x00000092 System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C (void);
// 0x00000093 System.Void Firebase.FirebaseApp::.cctor()
extern void FirebaseApp__cctor_m91B5E844644438D93858FE54C4DF15D53358F31B (void);
// 0x00000094 System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern void FirebaseApp_getCPtr_m54B5DAC73BA43E79771E0181BEFD846BBE06C84F (void);
// 0x00000095 System.Void Firebase.FirebaseApp::Finalize()
extern void FirebaseApp_Finalize_mF8DA91BE30AF031A390E068301053AEF3D6B5A98 (void);
// 0x00000096 System.Void Firebase.FirebaseApp::Dispose()
extern void FirebaseApp_Dispose_mC1965A7AE8BAB834DB652BF0BACF377F3D45192B (void);
// 0x00000097 System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
extern void FirebaseApp_TranslateDllNotFoundException_m8A53BF93797E69E0A396E5D387C8BE2FAC5A887E (void);
// 0x00000098 Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern void FirebaseApp_get_DefaultInstance_m2387909BEFA7CA8F51D87B62700EAE8DA6FC13A0 (void);
// 0x00000099 Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern void FirebaseApp_GetInstance_m9BAC597B32771401771C8915446DA531E7B66EC5 (void);
// 0x0000009A Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern void FirebaseApp_Create_mB737A2508FF5A06F35D01D4A8CD4AEF1F8944512 (void);
// 0x0000009B System.String Firebase.FirebaseApp::get_Name()
extern void FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E (void);
// 0x0000009C Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
extern void FirebaseApp_get_LogLevel_m64B54EED8CF1B5F8EA074612CF09E58026D23603 (void);
// 0x0000009D System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
extern void FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85 (void);
// 0x0000009E System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
extern void FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289 (void);
// 0x0000009F System.Void Firebase.FirebaseApp::AddReference()
extern void FirebaseApp_AddReference_m562BA6DFE00568AC30B15C36D8BB848F14EDED95 (void);
// 0x000000A0 System.Void Firebase.FirebaseApp::RemoveReference()
extern void FirebaseApp_RemoveReference_m3C28724EDB5D9F20A2A4924E517A8FF79C7E3425 (void);
// 0x000000A1 System.Void Firebase.FirebaseApp::ThrowIfNull()
extern void FirebaseApp_ThrowIfNull_mEBB4A7F4A0E30B8F6969C68C340AF30D44491B20 (void);
// 0x000000A2 System.Void Firebase.FirebaseApp::InitializeAppUtilCallbacks()
extern void FirebaseApp_InitializeAppUtilCallbacks_m69A50FD352AE820F31C0DBA793A462BC774F4B20 (void);
// 0x000000A3 System.Void Firebase.FirebaseApp::OnAllAppsDestroyed()
extern void FirebaseApp_OnAllAppsDestroyed_m19CF36FB1A2439786994BBB55F1E405B7B43CAAB (void);
// 0x000000A4 System.Boolean Firebase.FirebaseApp::InitializeCrashlyticsIfPresent()
extern void FirebaseApp_InitializeCrashlyticsIfPresent_m543A1327364F796F96120C0CD4D7805B412529B6 (void);
// 0x000000A5 Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate,Firebase.FirebaseApp)
extern void FirebaseApp_CreateAndTrack_m3EFC2B1BFE4FF3BE069B04DE63E93A257CD65B3E (void);
// 0x000000A6 System.Void Firebase.FirebaseApp::ThrowIfCheckDependenciesRunning()
extern void FirebaseApp_ThrowIfCheckDependenciesRunning_mCC374EEFD25964DB6A38DA14EC987792DE119D9B (void);
// 0x000000A7 System.Boolean Firebase.FirebaseApp::IsCheckDependenciesRunning()
extern void FirebaseApp_IsCheckDependenciesRunning_m14D0D3293F91F52A79382CB3B8B2C93EBE3FDB59 (void);
// 0x000000A8 System.String Firebase.FirebaseApp::get_NameInternal()
extern void FirebaseApp_get_NameInternal_m493D9AEC87709D1197A1997C7560AFEBB107FBCE (void);
// 0x000000A9 Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern void FirebaseApp_CreateInternal_m63EB3F64189DA8E6C5B2E1C95B11D63EF7F7BC4B (void);
// 0x000000AA System.Void Firebase.FirebaseApp::ReleaseReferenceInternal(Firebase.FirebaseApp)
extern void FirebaseApp_ReleaseReferenceInternal_mA281FCA13BCF136D9D9B4311C40925B99614D63D (void);
// 0x000000AB System.Void Firebase.FirebaseApp::RegisterLibraryInternal(System.String,System.String)
extern void FirebaseApp_RegisterLibraryInternal_mA76889CEB6554AF46D5A371236028E7BC32F13C4 (void);
// 0x000000AC System.Void Firebase.FirebaseApp::AppSetDefaultConfigPath(System.String)
extern void FirebaseApp_AppSetDefaultConfigPath_m9712BA055777DA0F98524BD85A6C97FBFC5C6192 (void);
// 0x000000AD System.String Firebase.FirebaseApp::get_DefaultName()
extern void FirebaseApp_get_DefaultName_mE170961E3E149AB409453866F8FBEDE07E9C3714 (void);
// 0x000000AE Firebase.FirebaseApp Firebase.FirebaseApp::<Create>m__0()
extern void FirebaseApp_U3CCreateU3Em__0_mA4818C0300E040537AA50D14D45568B60135FD59 (void);
// 0x000000AF System.Boolean Firebase.FirebaseApp::<CreateAndTrack>m__1()
extern void FirebaseApp_U3CCreateAndTrackU3Em__1_mAFA14C14DA9E90AE9A5F0C646F75AAD04D616A11 (void);
// 0x000000B0 System.Void Firebase.FirebaseApp/EnableModuleParams::.ctor(System.String,System.String,System.Boolean)
extern void EnableModuleParams__ctor_m448B394AF46BBC2CE9C3301F732850625F6B37EF (void);
// 0x000000B1 System.String Firebase.FirebaseApp/EnableModuleParams::get_CppModuleName()
extern void EnableModuleParams_get_CppModuleName_mB91981F21F3F94D82CD64DD7BD810741CBB04E3A (void);
// 0x000000B2 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CppModuleName(System.String)
extern void EnableModuleParams_set_CppModuleName_mF1C3FE3BBE44DEDB23AF2879630075AEAC7106DF (void);
// 0x000000B3 System.String Firebase.FirebaseApp/EnableModuleParams::get_CSharpClassName()
extern void EnableModuleParams_get_CSharpClassName_m04AD392AA82FCE1E6636F812672C77F294AC16EC (void);
// 0x000000B4 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CSharpClassName(System.String)
extern void EnableModuleParams_set_CSharpClassName_m9152635BDD8F608352C12F3447962C10F7DF4F43 (void);
// 0x000000B5 System.Boolean Firebase.FirebaseApp/EnableModuleParams::get_AlwaysEnable()
extern void EnableModuleParams_get_AlwaysEnable_mC44F8EA7A9EDCD493C6B8E04E3B3CF00D09FDEA6 (void);
// 0x000000B6 System.Void Firebase.FirebaseApp/EnableModuleParams::set_AlwaysEnable(System.Boolean)
extern void EnableModuleParams_set_AlwaysEnable_m3F7638041BDA0CC3669AD7119C68ABD2B6F7C482 (void);
// 0x000000B7 System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateDelegate__ctor_m966C39812E422F82DD3AACF101F012749B1F9E12 (void);
// 0x000000B8 Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern void CreateDelegate_Invoke_m3C05F10053C0FD938376079571835049ADDD6186 (void);
// 0x000000B9 System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern void FutureBase__ctor_m98C8AE4F030730C1CEE7E0B4A1816C623F2B9BE0 (void);
// 0x000000BA System.Void Firebase.FutureBase::Finalize()
extern void FutureBase_Finalize_m9CD99D25C0199A337732E16288ABCE051A4D5CB7 (void);
// 0x000000BB System.Void Firebase.FutureBase::Dispose()
extern void FutureBase_Dispose_m32193D02DE4608C6C3EDF42F3D0495707DA4D15E (void);
// 0x000000BC Firebase.FutureStatus Firebase.FutureBase::status()
extern void FutureBase_status_mC75FD35438B176F95462D3A5D7D9194629211902 (void);
// 0x000000BD System.Int32 Firebase.FutureBase::error()
extern void FutureBase_error_m47E3B5E0A43B4C19510A77B3658EE5D7D10B6030 (void);
// 0x000000BE System.String Firebase.FutureBase::error_message()
extern void FutureBase_error_message_m5CC18319253B1ECC3C8AC675B213A08B1755D527 (void);
// 0x000000BF System.Void Firebase.Platform.FirebaseAppUtils::.ctor()
extern void FirebaseAppUtils__ctor_m77E9C2ADF611B1553A685AC953C5508DFD636CD4 (void);
// 0x000000C0 Firebase.Platform.FirebaseAppUtils Firebase.Platform.FirebaseAppUtils::get_Instance()
extern void FirebaseAppUtils_get_Instance_m844818D4028B3E8866E21AD1CB6E559038CEEC41 (void);
// 0x000000C1 System.Void Firebase.Platform.FirebaseAppUtils::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtils_TranslateDllNotFoundException_m8D9620D2F9B093C4DBF14AD9803923F0763955B8 (void);
// 0x000000C2 System.Void Firebase.Platform.FirebaseAppUtils::PollCallbacks()
extern void FirebaseAppUtils_PollCallbacks_m94AC1FCAA3602F030E6AA26C1FD6CB03E0F7155C (void);
// 0x000000C3 Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtils::GetLogLevel()
extern void FirebaseAppUtils_GetLogLevel_m420F7E6140E65C5494538339E1322E33F3661105 (void);
// 0x000000C4 System.Void Firebase.Platform.FirebaseAppUtils::.cctor()
extern void FirebaseAppUtils__cctor_m5444BBFA10C503F9659FF92D02A028B94DBB2204 (void);
// 0x000000C5 System.String Firebase.VersionInfo::get_SdkVersion()
extern void VersionInfo_get_SdkVersion_mC32BFBE632414898F8525BD5AED76B512BA0E266 (void);
// 0x000000C6 System.Object Firebase.VariantExtension::ToObject(Firebase.Variant,Firebase.VariantExtension/KeyOptions)
extern void VariantExtension_ToObject_m48E049BEBDB87869AC67AE16FB2F31E7981CA594 (void);
// 0x000000C7 System.Object Firebase.VariantExtension::ToObjectInternal(Firebase.Variant,Firebase.VariantExtension/KeyOptions)
extern void VariantExtension_ToObjectInternal_m8AA939C2150F64216049D0FE9C713732AA4DBCF0 (void);
// 0x000000C8 System.Collections.Generic.IDictionary`2<System.String,System.Object> Firebase.VariantExtension::ToStringVariantMap(Firebase.VariantVariantMap,Firebase.VariantExtension/KeyOptions)
extern void VariantExtension_ToStringVariantMap_m616B0FA2BD08664FBC67A9A74BFC66A7BBA04646 (void);
// 0x000000C9 System.Void Firebase.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_m4AE84268E5E69C1E4E1E8CD7AF145EF3C73DDA02 (void);
// 0x000000CA System.Void Firebase.LogUtil::.cctor()
extern void LogUtil__cctor_m65D0A76AA61474FFF64D462091D3620818923C9E (void);
// 0x000000CB System.Void Firebase.LogUtil::.ctor()
extern void LogUtil__ctor_mFE64F3E0CAE4C8D317093D419552825F2187F3EA (void);
// 0x000000CC System.Void Firebase.LogUtil::InitializeLogging()
extern void LogUtil_InitializeLogging_mC8B6DCC4B1E24F42B676EA58E1AD2EBCDF2967CE (void);
// 0x000000CD Firebase.Platform.PlatformLogLevel Firebase.LogUtil::ConvertLogLevel(Firebase.LogLevel)
extern void LogUtil_ConvertLogLevel_mE58CCE065A1D6EBEDDDDA2CDE76AFEA71E474216 (void);
// 0x000000CE System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessage_mA96CEACFEBC0F9B08D7F282A4E55685F6E803A49 (void);
// 0x000000CF System.Void Firebase.LogUtil::LogMessageFromCallback(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessageFromCallback_m3EA336850B4BE115C393BA3AD71981D1AA654307 (void);
// 0x000000D0 System.Void Firebase.LogUtil::Finalize()
extern void LogUtil_Finalize_mA58D6095B47CD414CEED5AB924C2D53F34FF9D55 (void);
// 0x000000D1 System.Void Firebase.LogUtil::Dispose()
extern void LogUtil_Dispose_m69B36B965145091F6023543E577B1D882AAD3F31 (void);
// 0x000000D2 System.Void Firebase.LogUtil::Dispose(System.Boolean)
extern void LogUtil_Dispose_m97EA8C366043F8F98301F73F488901880DA431CB (void);
// 0x000000D3 System.Void Firebase.LogUtil::<LogUtil>m__0(System.Object,System.EventArgs)
extern void LogUtil_U3CLogUtilU3Em__0_mBF4E9852385CD398896D319A5ECB080F71E3324A (void);
// 0x000000D4 System.Void Firebase.LogUtil/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void LogMessageDelegate__ctor_mB6AACCCEAE43E818C4B0DFCF6388FF4CC7200F10 (void);
// 0x000000D5 System.Void Firebase.LogUtil/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern void LogMessageDelegate_Invoke_m93848481738EC2A03FD8F5600C132464290BDAC8 (void);
// 0x000000D6 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern void InitializationException__ctor_mC48C74EE90B137CDEA82068C2E1695D81974C5BF (void);
// 0x000000D7 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String,System.Exception)
extern void InitializationException__ctor_m1384021A3E1B7B0E372257380559D926BD6200BF (void);
// 0x000000D8 System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern void InitializationException_set_InitResult_m94032AD57F63718F6F20625FDB98958766C9D764 (void);
// 0x000000D9 System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern void FirebaseException__ctor_m18D67DA955D2B4EA2BC58BCE0E96AC0A177DD70F (void);
// 0x000000DA System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern void FirebaseException_set_ErrorCode_m65B2880424E85063D56405A009DAA13E3B106465 (void);
// 0x000000DB System.String Firebase.ErrorMessages::get_DependencyNotFoundErrorMessage()
extern void ErrorMessages_get_DependencyNotFoundErrorMessage_mA71EBFCD6E5CC0C61BD0E3624738175EADBCC0F7 (void);
// 0x000000DC System.String Firebase.ErrorMessages::get_DllNotFoundExceptionErrorMessage()
extern void ErrorMessages_get_DllNotFoundExceptionErrorMessage_m0B273BB2A0E048AACEA44918DBBBBACB38B579F3 (void);
// 0x000000DD System.Void Firebase.ErrorMessages::.cctor()
extern void ErrorMessages__cctor_m15AA44253303AB0779074729761A927C52A9DD82 (void);
static Il2CppMethodPointer s_methodPointers[221] = 
{
	AppUtilPINVOKE__cctor_m8F7249FF9A16A52D144B2223D3CB9EA2195CE6DF,
	AppUtilPINVOKE_delete_FutureBase_mBA1AC9D7BE7977A080996ED289972DF8449F7BC5,
	AppUtilPINVOKE_FutureBase_status_m3056FA61BBB6B002A5BEE4F34F75DCD0858BC27C,
	AppUtilPINVOKE_FutureBase_error_mDB0C901628E9EE247400F57E7E697743B69906E3,
	AppUtilPINVOKE_FutureBase_error_message_mDB4089A47D35BF6D3457750C5355B1908C4BE46B,
	AppUtilPINVOKE_FirebaseApp_NameInternal_get_mE4931CA287567E2C7F91442E07F30AE96AE91172,
	AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_m8C61ECBE5407D4A591827E10595AE28EFE8C6CE5,
	AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_m5A184BBB9E09539CDDB771120995526D0328439B,
	AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_mAF74BAD43730ACCFFB25EFBDCB0F40CA30CE1940,
	AppUtilPINVOKE_FirebaseApp_RegisterLibraryInternal_mB4C1184BF2D1A163A2B3E91C6B91471CB7BB3E1F,
	AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_m461EBC1DCEA9353F3C927FAEEB1590BA130E69DB,
	AppUtilPINVOKE_FirebaseApp_DefaultName_get_m7561CF63339BB772F6A9690B9E07D9E21BA70D2A,
	AppUtilPINVOKE_PollCallbacks_mD2FF7C60A52AF22E9AC028564A06E0F3974B6D56,
	AppUtilPINVOKE_AppEnableLogCallback_m5BB69B725FD3DF1FE26C20DE516F14E02E82BDB3,
	AppUtilPINVOKE_SetEnabledAllAppCallbacks_m6A273BFF682F24C5D1F66273B0AA3AF975B29019,
	AppUtilPINVOKE_SetEnabledAppCallbackByName_m4705ADB8109C59533C8D2117C87E6336EEB54A05,
	AppUtilPINVOKE_GetEnabledAppCallbackByName_m97E56E8BCA68A8391B677BC814B1E3584FD985C5,
	AppUtilPINVOKE_SetLogFunction_m76775D9FA055C83D4C65B6E6E7192E941A433EAE,
	AppUtilPINVOKE_VariantVariantMap_size_m47D217D462E3AA2C0479998CEEC5F0AF5FB9A6AF,
	AppUtilPINVOKE_VariantVariantMap_Clear_mC15F99B817AE7002EB8B6847B9FC4262AF47ED95,
	AppUtilPINVOKE_VariantVariantMap_getitem_m8012D3A44F32A4B4A12B3B7C9A1F9804FA889E2C,
	AppUtilPINVOKE_VariantVariantMap_setitem_m59D72239FEC0A32146ED5A9F50A5EEA2FA9CED0D,
	AppUtilPINVOKE_VariantVariantMap_ContainsKey_m8E9F83BAA16786DEA9AA2A23B18BCB67570C2A62,
	AppUtilPINVOKE_VariantVariantMap_Add_m7693EF89F3612F62D27F1B7BFFAE93C0D9895FAA,
	AppUtilPINVOKE_VariantVariantMap_Remove_mBA53C94FFC339ADCA616B99BC9B3DE9C7184D08B,
	AppUtilPINVOKE_VariantVariantMap_create_iterator_begin_m7A904CC444AF4397B7038AD0AB0C14B3ECD21E1B,
	AppUtilPINVOKE_VariantVariantMap_get_next_key_mF61D7A043695C1BB6C22D37C0C45821647E91411,
	AppUtilPINVOKE_VariantVariantMap_destroy_iterator_m73C0BA71E2300B2C6A255129175E8C2B37EA85C4,
	AppUtilPINVOKE_delete_VariantVariantMap_mFE3E435232BF15367FA092A1098ED3FDEC0ABA0B,
	AppUtilPINVOKE_VariantList_size_m3A48821A74BE9435C6B2EF9632FA19F2D77C81EF,
	AppUtilPINVOKE_VariantList_getitem_mDBA8806675B9D11F3074AE72C844D01EF9F6A3FF,
	AppUtilPINVOKE_delete_VariantList_mFCC6CA449E1F1B85B7D2EFB7A9735C2CFD7F1BC3,
	AppUtilPINVOKE_delete_Variant_mA45ED3EFF2C315AFC59E0FF4A3950E6CA06EE363,
	AppUtilPINVOKE_Variant_type_m0575207999A99C1679D7E9ABF70BF42B2DBA13AA,
	AppUtilPINVOKE_Variant_is_string_m0F75C01B34DB5651C00B60D0F477BF7BDD22DF3F,
	AppUtilPINVOKE_Variant_is_fundamental_type_m8B68412DA67C92C82E313F5F7CE9CFF9C70F40FC,
	AppUtilPINVOKE_Variant_AsString_m14A8B144EA7FAF0396478ACF233863E9CFB982AA,
	AppUtilPINVOKE_Variant_blob_size_m848887376DB001FD4066EFB4DAC5F13D3A7E4712,
	AppUtilPINVOKE_Variant_vector__SWIG_0_m2EEE0E67CF5880090850B48A7D84B644AC1849AA,
	AppUtilPINVOKE_Variant_map__SWIG_0_mE008492E043F7AE7554961B5382C1D6BFCCCEDD9,
	AppUtilPINVOKE_Variant_int64_value_m7215A286D3C0BDD480ED52735607F63039BDB947,
	AppUtilPINVOKE_Variant_double_value_m3F37AD68D935C0EE9CED1063E55D2D47AE89FE7C,
	AppUtilPINVOKE_Variant_bool_value_m9A7C00507E31A2EAA81D670B132BFE78D23ACDFB,
	AppUtilPINVOKE_Variant_string_value_mCFC1313C97830F6EC780F3A3E37A2C8E898E45EB,
	AppUtilPINVOKE_Variant_untyped_mutable_blob_data_mA898DA666F9663143AED17659F774773F2D6AC18,
	SWIGExceptionHelper__cctor_m2E9064FCCB3E37EAE10ED204AF7A72C0F1F78F8B,
	SWIGExceptionHelper__ctor_m4DB6794D8CB5F1A9740C37B0C257B69982C013B9,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m37C58735D4F8200BA3896F46FD46006F467A604D,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m20CAE98AF0AFEDCADBBEAA94148843C8C671F8D5,
	SWIGExceptionHelper_SetPendingApplicationException_m7FE3B7ADC198F4ED5A180BC5ECD18CC371444591,
	SWIGExceptionHelper_SetPendingArithmeticException_m5DA562871B81FA3E688FD12D78E82882F5ADC315,
	SWIGExceptionHelper_SetPendingDivideByZeroException_m7AE515E72B8E23D18919432B5B7BF0F06CCD18E7,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mCD203C03B85ADB38206622594E5DEECA14C1CA7E,
	SWIGExceptionHelper_SetPendingInvalidCastException_m64057305E28A3122C79BFF5A8C441D72B04C6E5B,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m4CE89FA918E3D9CA7C6391147792F8226CF6BA07,
	SWIGExceptionHelper_SetPendingIOException_mA50448F1AA4CA664C39B8AB78EF912F18E0DDF50,
	SWIGExceptionHelper_SetPendingNullReferenceException_m0CE8D326228371436AB3BBCE9AA7464619030A35,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mB4209DD263A50C83F1E9CE39A85ADDAE18F51759,
	SWIGExceptionHelper_SetPendingOverflowException_m366DEFCF657EFE4CBABD2ADCD7D09BD6144E25B0,
	SWIGExceptionHelper_SetPendingSystemException_mE317DA81F256CF3BD75CAC264E25961D7A536191,
	SWIGExceptionHelper_SetPendingArgumentException_m82CC529F5355DF173784D29CDB197BC3AAA353BC,
	SWIGExceptionHelper_SetPendingArgumentNullException_m1A239C193A01B3E73BD763718FB528ED933847A0,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1F079CDB1AC454648BEFF38716F88AFE6FA8F926,
	ExceptionDelegate__ctor_m49AB94CEC8E6544CE0D7B1E2300735728EE336D8,
	ExceptionDelegate_Invoke_mE04E9A1D96F5AE159E3D7878E87706B91A149B25,
	ExceptionArgumentDelegate__ctor_m45E4EFAE5F14FFEC5843A00ABEF4D0E1F0854629,
	ExceptionArgumentDelegate_Invoke_m46BEB8B17B239BFB498C54B89EB06352BD1948F8,
	SWIGPendingException_get_Pending_mFF0B94BEAD2C48E3CD3BB7EFB5EB23A0B6CF55EE,
	SWIGPendingException_Set_m7EB577A3129E190D2FBAF8CB9C9CAB7F87DA2642,
	SWIGPendingException_Retrieve_m5AF2B72B0C255B87F0044137B357F55F50101D51,
	SWIGStringHelper__cctor_mAEBC8A14986139AE4856DA02EC5F3748CACE1971,
	SWIGStringHelper__ctor_mF5EBABDC102D937A919B6A6CCA3690E2244ECE85,
	SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m6DC4FCA4611C8A5521BE0E65F5CCE26D000E4DD1,
	SWIGStringHelper_CreateString_m929C4B0922610C0571E685F27F79757BF669C77F,
	SWIGStringDelegate__ctor_m4D5B167B33345B58192AD3B50D1F8901A18F4F4D,
	SWIGStringDelegate_Invoke_m9831BD87E0EBFECFF48B4CA3FFEB118280C2ABCB,
	AppUtil_PollCallbacks_mB25BC1FD5126974F80860A05910301AAF7589BFC,
	AppUtil_AppEnableLogCallback_m9FE8159D116019E4E918F4B7CEC39687DD64B2EA,
	AppUtil_SetEnabledAllAppCallbacks_m0C41A4271764464915ACA461AE8A309AEDAFA6AC,
	AppUtil_SetEnabledAppCallbackByName_m59C6CB27D92D72FD0841031C207D86C2F684F366,
	AppUtil_GetEnabledAppCallbackByName_m979A86ABDBC2257B697ABDE5015AA76DE5D04F70,
	AppUtil_SetLogFunction_m1C535B70AA8069AC3CE81CB05882946F841DACCB,
	Variant__ctor_mCBCDC0D6E1290E1A8412B59A4A8956BC32DFA163,
	Variant_getCPtr_m02745459FA3350FFDC29D01587C8D797BFC10067,
	Variant_Finalize_mAB5A4CC29AC40AF0FD7FFF427E671CA7796E9E0C,
	Variant_Dispose_m68085851C3EE3DDDB413CAE4B1B4E237D0A87FBE,
	Variant_blob_as_bytes_mCA950A447F7928E4E3A303D09C0003EFB2A491EF,
	Variant_type_m03A0A0BFC610CF0047576B5CB15DC7E58E00D0B0,
	Variant_is_string_m2CD01E3123A174A575C240FD125F3E43CEAB73A8,
	Variant_is_fundamental_type_mB173361527A94BAEEBBF8747805DD81AD1BD9E2C,
	Variant_AsString_m88B77BD34C54D8FF8596675C201FAA43945EA758,
	Variant_blob_size_mCF0A2FFB1862051AAFE295694E62A87F03228563,
	Variant_vector_m605D7667ABCF391DD582F236746338AFEDD03262,
	Variant_map_mB7447AAFBC3421A6EEAE48E5A6FA13F08FA66597,
	Variant_int64_value_m11AE869002EB87F8958F3AF7122C33C466557C55,
	Variant_double_value_m66B4F265644B4F4B4DA1C5CDC23D220C62E07DAA,
	Variant_bool_value_m2AC1B3119BC6AD8B1BE18DA36B721B0CCBEE9CAC,
	Variant_string_value_mFC641893647B7A1023754C2A28C8FC3DCB39B0C7,
	Variant_untyped_mutable_blob_data_m69D26F368BADF4C3D26A201EFED448CE0A16169E,
	VariantList__ctor_m61AA2F0090993B8A446B5F5F4605044D353F34DF,
	VariantList_Finalize_mE6E6EEDD2892B26B954A226E29E88147937C31DD,
	VariantList_Dispose_mCCC4F2FC0519A530EEAE62AD612EA962E14092C6,
	VariantList_get_Item_m6DB080671B53261F20EC0BEFE80E72CB0F1945C2,
	VariantList_get_Count_m912730C365BD33F01CC1665B3D099BA5D8B98F7D,
	VariantList_System_Collections_Generic_IEnumerableU3CFirebase_VariantU3E_GetEnumerator_m3E9FA3584B4B641FA48272FA89902A3E4C4B103A,
	VariantList_System_Collections_IEnumerable_GetEnumerator_mCF9510C458E396BA0926E077FB9C6CC9C2BF451C,
	VariantList_GetEnumerator_m7F02DAF1A3C06A5CE8F316DC8ABB81E39F209AAD,
	VariantList_size_m328AD0C3BE60BA20EC49B39067F51317241D58B1,
	VariantList_getitem_mC418034502AA2DDC2B866F7175BA912D506A0DF3,
	VariantListEnumerator__ctor_m55CB33FB76E96E989600F557E45AFFBF440C04C7,
	VariantListEnumerator_get_Current_mB5DE10FA7F3EF3D40024B568B035DDA9188000DD,
	VariantListEnumerator_System_Collections_IEnumerator_get_Current_m305D2B35CB662715F6268DEA743BE4BD18E352A5,
	VariantListEnumerator_MoveNext_mB3356E9DA1734FE027E0F0F63CA8D0859C864117,
	VariantListEnumerator_Dispose_mA490806D774FDDEA003913DD68EA5290E6B1B3ED,
	VariantVariantMap__ctor_m41C3F31D743DEF93EBA676BE557F1A70E4BE7334,
	VariantVariantMap_Finalize_mDAF681083D55595180D8A687C342C8B75265DDB2,
	VariantVariantMap_Dispose_m2EF5C536EF12EF2311C8107618F38C6DEC115D17,
	VariantVariantMap_get_Item_mEE387DB235EC361518878AB36DAD8E9D508375FB,
	VariantVariantMap_set_Item_m3400A43959480801333DE6EF2C6678B07B0992BA,
	VariantVariantMap_TryGetValue_mCEE65FA7D0E6BF754F3D250D2830FA38FA39786B,
	VariantVariantMap_get_Count_m2BEE44655BF6BD9D696D0D59C23BC6BBFB033A98,
	VariantVariantMap_get_IsReadOnly_m27A6D9FE6FDA1F97EE831FF81A567FC2F61EEB0A,
	VariantVariantMap_get_Keys_m2C2F2F500CDE16BCD958453ECD0DD537C18D511C,
	VariantVariantMap_Add_m290B443C10658813255555A1DD6A13A2E4260ED1,
	VariantVariantMap_Remove_m38A98E00BA4B24B7B9A73B5A93F83D4A73565E83,
	VariantVariantMap_Contains_m92DA6A5CCE3AB281CE9E729CD7FECBD2790BD602,
	VariantVariantMap_CopyTo_mF75ECD5C29F30F9F482126DBEA0E99D5A26436BE,
	VariantVariantMap_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CFirebase_VariantU2CFirebase_VariantU3EU3E_GetEnumerator_m801B90F2D5B346799F4D77E27865CEA05B94BE9E,
	VariantVariantMap_System_Collections_IEnumerable_GetEnumerator_mA754DFF662C1032AA3379FDA3056FB9581AEF8DE,
	VariantVariantMap_GetEnumerator_m360952FBCF0F2F6AE30CA92612D50F9043894567,
	VariantVariantMap_size_m2770CDA09C92D8766E4A857F1A73E2A52565B1FD,
	VariantVariantMap_Clear_m5BDCCEC4D9C6FD388B5AFE22808BA04693BEDBFC,
	VariantVariantMap_getitem_mB7316DB5AA86AF857DF7CFD03949E8A8290C6C1A,
	VariantVariantMap_setitem_mDDA297409628E8F3E024CE64A2FECD629E7579B8,
	VariantVariantMap_ContainsKey_mFC7BDCB3103730A4FA0D5B94107F71CCC85E3955,
	VariantVariantMap_Add_m0487DEEFE55FF79967F7A7583F7563408261BBF1,
	VariantVariantMap_Remove_m6916F106939189219FBC8DB48CFB407F0CC7885A,
	VariantVariantMap_create_iterator_begin_m131671A481692CB8BA53945C4D5731B034CDEE12,
	VariantVariantMap_get_next_key_m088DEF53A9746BEB15DD8B344C2AFB232B4A6DD3,
	VariantVariantMap_destroy_iterator_m754B78CB27395071ABE1E283241CCFEECA0ABD38,
	VariantVariantMapEnumerator__ctor_m19EBFEA767C0CD94BAAC0028E8878CC804AD68E2,
	VariantVariantMapEnumerator_get_Current_m2DDA678E632DBC094DBD1C613CB572AECCC6FC2A,
	VariantVariantMapEnumerator_System_Collections_IEnumerator_get_Current_mA7F4DDCBABA52036A1BCC6524BAC28E9CE10C1E0,
	VariantVariantMapEnumerator_MoveNext_mBD4ECB27F072218FD3DE27476AA880BB530A0F06,
	VariantVariantMapEnumerator_Dispose_m991451F9BDD4DC411399D475FFDEEACC3A7A4BC3,
	FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C,
	FirebaseApp__cctor_m91B5E844644438D93858FE54C4DF15D53358F31B,
	FirebaseApp_getCPtr_m54B5DAC73BA43E79771E0181BEFD846BBE06C84F,
	FirebaseApp_Finalize_mF8DA91BE30AF031A390E068301053AEF3D6B5A98,
	FirebaseApp_Dispose_mC1965A7AE8BAB834DB652BF0BACF377F3D45192B,
	FirebaseApp_TranslateDllNotFoundException_m8A53BF93797E69E0A396E5D387C8BE2FAC5A887E,
	FirebaseApp_get_DefaultInstance_m2387909BEFA7CA8F51D87B62700EAE8DA6FC13A0,
	FirebaseApp_GetInstance_m9BAC597B32771401771C8915446DA531E7B66EC5,
	FirebaseApp_Create_mB737A2508FF5A06F35D01D4A8CD4AEF1F8944512,
	FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E,
	FirebaseApp_get_LogLevel_m64B54EED8CF1B5F8EA074612CF09E58026D23603,
	FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85,
	FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289,
	FirebaseApp_AddReference_m562BA6DFE00568AC30B15C36D8BB848F14EDED95,
	FirebaseApp_RemoveReference_m3C28724EDB5D9F20A2A4924E517A8FF79C7E3425,
	FirebaseApp_ThrowIfNull_mEBB4A7F4A0E30B8F6969C68C340AF30D44491B20,
	FirebaseApp_InitializeAppUtilCallbacks_m69A50FD352AE820F31C0DBA793A462BC774F4B20,
	FirebaseApp_OnAllAppsDestroyed_m19CF36FB1A2439786994BBB55F1E405B7B43CAAB,
	FirebaseApp_InitializeCrashlyticsIfPresent_m543A1327364F796F96120C0CD4D7805B412529B6,
	FirebaseApp_CreateAndTrack_m3EFC2B1BFE4FF3BE069B04DE63E93A257CD65B3E,
	FirebaseApp_ThrowIfCheckDependenciesRunning_mCC374EEFD25964DB6A38DA14EC987792DE119D9B,
	FirebaseApp_IsCheckDependenciesRunning_m14D0D3293F91F52A79382CB3B8B2C93EBE3FDB59,
	FirebaseApp_get_NameInternal_m493D9AEC87709D1197A1997C7560AFEBB107FBCE,
	FirebaseApp_CreateInternal_m63EB3F64189DA8E6C5B2E1C95B11D63EF7F7BC4B,
	FirebaseApp_ReleaseReferenceInternal_mA281FCA13BCF136D9D9B4311C40925B99614D63D,
	FirebaseApp_RegisterLibraryInternal_mA76889CEB6554AF46D5A371236028E7BC32F13C4,
	FirebaseApp_AppSetDefaultConfigPath_m9712BA055777DA0F98524BD85A6C97FBFC5C6192,
	FirebaseApp_get_DefaultName_mE170961E3E149AB409453866F8FBEDE07E9C3714,
	FirebaseApp_U3CCreateU3Em__0_mA4818C0300E040537AA50D14D45568B60135FD59,
	FirebaseApp_U3CCreateAndTrackU3Em__1_mAFA14C14DA9E90AE9A5F0C646F75AAD04D616A11,
	EnableModuleParams__ctor_m448B394AF46BBC2CE9C3301F732850625F6B37EF,
	EnableModuleParams_get_CppModuleName_mB91981F21F3F94D82CD64DD7BD810741CBB04E3A,
	EnableModuleParams_set_CppModuleName_mF1C3FE3BBE44DEDB23AF2879630075AEAC7106DF,
	EnableModuleParams_get_CSharpClassName_m04AD392AA82FCE1E6636F812672C77F294AC16EC,
	EnableModuleParams_set_CSharpClassName_m9152635BDD8F608352C12F3447962C10F7DF4F43,
	EnableModuleParams_get_AlwaysEnable_mC44F8EA7A9EDCD493C6B8E04E3B3CF00D09FDEA6,
	EnableModuleParams_set_AlwaysEnable_m3F7638041BDA0CC3669AD7119C68ABD2B6F7C482,
	CreateDelegate__ctor_m966C39812E422F82DD3AACF101F012749B1F9E12,
	CreateDelegate_Invoke_m3C05F10053C0FD938376079571835049ADDD6186,
	FutureBase__ctor_m98C8AE4F030730C1CEE7E0B4A1816C623F2B9BE0,
	FutureBase_Finalize_m9CD99D25C0199A337732E16288ABCE051A4D5CB7,
	FutureBase_Dispose_m32193D02DE4608C6C3EDF42F3D0495707DA4D15E,
	FutureBase_status_mC75FD35438B176F95462D3A5D7D9194629211902,
	FutureBase_error_m47E3B5E0A43B4C19510A77B3658EE5D7D10B6030,
	FutureBase_error_message_m5CC18319253B1ECC3C8AC675B213A08B1755D527,
	FirebaseAppUtils__ctor_m77E9C2ADF611B1553A685AC953C5508DFD636CD4,
	FirebaseAppUtils_get_Instance_m844818D4028B3E8866E21AD1CB6E559038CEEC41,
	FirebaseAppUtils_TranslateDllNotFoundException_m8D9620D2F9B093C4DBF14AD9803923F0763955B8,
	FirebaseAppUtils_PollCallbacks_m94AC1FCAA3602F030E6AA26C1FD6CB03E0F7155C,
	FirebaseAppUtils_GetLogLevel_m420F7E6140E65C5494538339E1322E33F3661105,
	FirebaseAppUtils__cctor_m5444BBFA10C503F9659FF92D02A028B94DBB2204,
	VersionInfo_get_SdkVersion_mC32BFBE632414898F8525BD5AED76B512BA0E266,
	VariantExtension_ToObject_m48E049BEBDB87869AC67AE16FB2F31E7981CA594,
	VariantExtension_ToObjectInternal_m8AA939C2150F64216049D0FE9C713732AA4DBCF0,
	VariantExtension_ToStringVariantMap_m616B0FA2BD08664FBC67A9A74BFC66A7BBA04646,
	MonoPInvokeCallbackAttribute__ctor_m4AE84268E5E69C1E4E1E8CD7AF145EF3C73DDA02,
	LogUtil__cctor_m65D0A76AA61474FFF64D462091D3620818923C9E,
	LogUtil__ctor_mFE64F3E0CAE4C8D317093D419552825F2187F3EA,
	LogUtil_InitializeLogging_mC8B6DCC4B1E24F42B676EA58E1AD2EBCDF2967CE,
	LogUtil_ConvertLogLevel_mE58CCE065A1D6EBEDDDDA2CDE76AFEA71E474216,
	LogUtil_LogMessage_mA96CEACFEBC0F9B08D7F282A4E55685F6E803A49,
	LogUtil_LogMessageFromCallback_m3EA336850B4BE115C393BA3AD71981D1AA654307,
	LogUtil_Finalize_mA58D6095B47CD414CEED5AB924C2D53F34FF9D55,
	LogUtil_Dispose_m69B36B965145091F6023543E577B1D882AAD3F31,
	LogUtil_Dispose_m97EA8C366043F8F98301F73F488901880DA431CB,
	LogUtil_U3CLogUtilU3Em__0_mBF4E9852385CD398896D319A5ECB080F71E3324A,
	LogMessageDelegate__ctor_mB6AACCCEAE43E818C4B0DFCF6388FF4CC7200F10,
	LogMessageDelegate_Invoke_m93848481738EC2A03FD8F5600C132464290BDAC8,
	InitializationException__ctor_mC48C74EE90B137CDEA82068C2E1695D81974C5BF,
	InitializationException__ctor_m1384021A3E1B7B0E372257380559D926BD6200BF,
	InitializationException_set_InitResult_m94032AD57F63718F6F20625FDB98958766C9D764,
	FirebaseException__ctor_m18D67DA955D2B4EA2BC58BCE0E96AC0A177DD70F,
	FirebaseException_set_ErrorCode_m65B2880424E85063D56405A009DAA13E3B106465,
	ErrorMessages_get_DependencyNotFoundErrorMessage_mA71EBFCD6E5CC0C61BD0E3624738175EADBCC0F7,
	ErrorMessages_get_DllNotFoundExceptionErrorMessage_m0B273BB2A0E048AACEA44918DBBBBACB38B579F3,
	ErrorMessages__cctor_m15AA44253303AB0779074729761A927C52A9DD82,
};
static const int32_t s_InvokerIndices[221] = 
{
	5107,
	5003,
	4809,
	4809,
	4868,
	4868,
	5077,
	5003,
	5075,
	4645,
	5008,
	5082,
	5107,
	5000,
	5000,
	4638,
	4736,
	5008,
	4963,
	5003,
	4470,
	4208,
	4330,
	4208,
	4330,
	4836,
	4472,
	4621,
	5003,
	4963,
	4471,
	5003,
	5003,
	4809,
	4731,
	4731,
	4836,
	4963,
	4836,
	4836,
	4825,
	4774,
	4731,
	4868,
	4836,
	5107,
	3316,
	3386,
	4254,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	5008,
	4645,
	4645,
	4645,
	1455,
	2688,
	1455,
	1458,
	5062,
	5008,
	5082,
	5107,
	3316,
	5008,
	4872,
	1455,
	2362,
	5107,
	5000,
	5000,
	4638,
	4736,
	5008,
	1442,
	4789,
	3316,
	3316,
	3228,
	3208,
	3168,
	3168,
	3228,
	3304,
	3228,
	3228,
	3209,
	3184,
	3168,
	3228,
	3210,
	1442,
	3316,
	3316,
	2359,
	3208,
	3228,
	3228,
	3228,
	3304,
	2359,
	2688,
	3228,
	3228,
	3168,
	3316,
	1442,
	3316,
	3316,
	2362,
	1458,
	880,
	3208,
	3168,
	3228,
	2546,
	1723,
	1723,
	1453,
	3228,
	3228,
	3228,
	3304,
	3316,
	2362,
	1458,
	1880,
	1458,
	1880,
	3210,
	2361,
	2672,
	2688,
	3095,
	3228,
	3168,
	3316,
	1442,
	5107,
	4789,
	3316,
	3316,
	5008,
	5082,
	4872,
	5082,
	3228,
	5075,
	2688,
	2688,
	3316,
	3316,
	3316,
	5107,
	5107,
	5062,
	4506,
	5107,
	5062,
	3228,
	5082,
	5008,
	4645,
	5008,
	5082,
	5082,
	5062,
	785,
	3228,
	2688,
	3228,
	2688,
	3168,
	2627,
	1455,
	3228,
	1442,
	3316,
	3316,
	3208,
	3208,
	3228,
	3316,
	5082,
	2688,
	3316,
	3208,
	5107,
	5082,
	4502,
	4502,
	4502,
	2688,
	5107,
	3316,
	5107,
	4811,
	4625,
	4625,
	3316,
	3316,
	2627,
	1458,
	1455,
	1335,
	1335,
	753,
	2670,
	1335,
	2670,
	5082,
	5082,
	5107,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[16] = 
{
	{ 0x06000032, 9,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m7FE3B7ADC198F4ED5A180BC5ECD18CC371444591_RuntimeMethod_var, 0 },
	{ 0x06000033, 13,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m5DA562871B81FA3E688FD12D78E82882F5ADC315_RuntimeMethod_var, 0 },
	{ 0x06000034, 14,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_m7AE515E72B8E23D18919432B5B7BF0F06CCD18E7_RuntimeMethod_var, 0 },
	{ 0x06000035, 16,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mCD203C03B85ADB38206622594E5DEECA14C1CA7E_RuntimeMethod_var, 0 },
	{ 0x06000036, 17,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m64057305E28A3122C79BFF5A8C441D72B04C6E5B_RuntimeMethod_var, 0 },
	{ 0x06000037, 18,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m4CE89FA918E3D9CA7C6391147792F8226CF6BA07_RuntimeMethod_var, 0 },
	{ 0x06000038, 15,  (void**)&SWIGExceptionHelper_SetPendingIOException_mA50448F1AA4CA664C39B8AB78EF912F18E0DDF50_RuntimeMethod_var, 0 },
	{ 0x06000039, 19,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m0CE8D326228371436AB3BBCE9AA7464619030A35_RuntimeMethod_var, 0 },
	{ 0x0600003A, 20,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mB4209DD263A50C83F1E9CE39A85ADDAE18F51759_RuntimeMethod_var, 0 },
	{ 0x0600003B, 21,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m366DEFCF657EFE4CBABD2ADCD7D09BD6144E25B0_RuntimeMethod_var, 0 },
	{ 0x0600003C, 22,  (void**)&SWIGExceptionHelper_SetPendingSystemException_mE317DA81F256CF3BD75CAC264E25961D7A536191_RuntimeMethod_var, 0 },
	{ 0x0600003D, 10,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m82CC529F5355DF173784D29CDB197BC3AAA353BC_RuntimeMethod_var, 0 },
	{ 0x0600003E, 11,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_m1A239C193A01B3E73BD763718FB528ED933847A0_RuntimeMethod_var, 0 },
	{ 0x0600003F, 12,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1F079CDB1AC454648BEFF38716F88AFE6FA8F926_RuntimeMethod_var, 0 },
	{ 0x0600004A, 23,  (void**)&SWIGStringHelper_CreateString_m929C4B0922610C0571E685F27F79757BF669C77F_RuntimeMethod_var, 0 },
	{ 0x060000CF, 7,  (void**)&LogUtil_LogMessageFromCallback_m3EA336850B4BE115C393BA3AD71981D1AA654307_RuntimeMethod_var, 0 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_App_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_App_CodeGenModule = 
{
	"Firebase.App.dll",
	221,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	16,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
