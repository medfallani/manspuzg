using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sergiodouglas : MonoBehaviour
{
    private static sergiodouglas _instance = null;
    public static sergiodouglas Instance
    {
        get { return _instance; }
    }

    public AudioClip[] suara;
    public AudioSource[] source;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        for (int i = 0; i < source.Length; i++)
        {
            source[i] = gameObject.AddComponent<AudioSource>();
            source[i].clip = suara[i];
        }
    }

    public void sabrinatodd(int i)
    {
        source[i].Play();
    }


}
