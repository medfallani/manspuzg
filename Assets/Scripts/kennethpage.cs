using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class kennethpage : MonoBehaviour
{
    public GameObject[] LevelBtn;
    public Sprite[] Imagez;

    public GameObject tommiecarson;
    [SerializeField] private bool vanessaherring = false;
    int natashadodson = 20;
    void Start()
    {
        GameObject scrollp = GameObject.Find("Scrool");

        for (int i = 0; i < natashadodson; i++)
        {
            var dkl = scrollp.transform.GetChild(i);
            LevelBtn[i] = dkl.gameObject;

        }

        PlayerPrefs.SetInt("game00", 1);
        for (int i = 0; i < natashadodson; i++)
        {
            string newname = LevelBtn[i].name;
            newname = newname.Substring(newname.IndexOf("(")).Replace("(", "").Replace(")", "");
            LevelBtn[i].GetComponent<Button>().GetComponentInChildren<Text>().text = newname;
            LevelBtn[i].name = newname;
            int nomer = int.Parse(newname) - 1;
            if (!vanessaherring)
            {
                if (PlayerPrefs.GetInt("game0" + nomer) <= 0)
                {
                    LevelBtn[i].GetComponent<Button>().interactable = false;
                    Debug.Log("buttonya mati" + i + " = null");
                }
                else
                {
                    LevelBtn[i].GetComponent<Button>().interactable = true;
                }
            }
            else
            {
                LevelBtn[i].GetComponent<Button>().interactable = true;
            }
        }
    }

    public void menu()
    {
        sergiodouglas.Instance.sabrinatodd(1);
        craighanna.florinemercado = "MainMenu";
        GameObject.Find("Canvas").GetComponent<Animator>().Play("end");
    }

    public void WUI_Open()
    {
        sergiodouglas.Instance.sabrinatodd(1);
        tommiecarson.active = true;
    }

    public void btn_No()
    {
        sergiodouglas.Instance.sabrinatodd(1);
        tommiecarson.GetComponent<Animator>().Play("end");
    }

    public void btn_yes()
    {
        sergiodouglas.Instance.sabrinatodd(1);
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("game00", 1);
        Start();
        tommiecarson.GetComponent<Animator>().Play("end");
    }
}
