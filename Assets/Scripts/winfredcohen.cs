using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class winfredcohen : MonoBehaviour
{
    private string masanford;
    public Texture[] BGImage;
    public GameObject robertaepps;

    private static winfredcohen _instance = null;
    public static winfredcohen Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    void Update()
    {
        masanford = SceneManager.GetActiveScene().name;
        if (masanford == "Game")
        {
            robertaepps.GetComponent<RawImage>().texture = BGImage[Data.LevelNow];
        }
    }
}
